﻿namespace CodeRestaurant
{
    partial class DividirCuenta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DividirCuenta));
            this.ProductosdataGridView = new System.Windows.Forms.DataGridView();
            this.ProductoId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Articulo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Precio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SubTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NuevaCuentadataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label8 = new System.Windows.Forms.Label();
            this.Mesalabel = new System.Windows.Forms.Label();
            this.Finalizarbutton = new System.Windows.Forms.Button();
            this.Cancelarbutton = new System.Windows.Forms.Button();
            this.Enviarbutton = new System.Windows.Forms.Button();
            this.Reversarbutton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.NCuentaTotallabel = new System.Windows.Forms.Label();
            this.MesaTotallabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ProductosdataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NuevaCuentadataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // ProductosdataGridView
            // 
            this.ProductosdataGridView.AllowUserToAddRows = false;
            this.ProductosdataGridView.AllowUserToDeleteRows = false;
            this.ProductosdataGridView.AllowUserToResizeColumns = false;
            this.ProductosdataGridView.AllowUserToResizeRows = false;
            this.ProductosdataGridView.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ProductosdataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.ProductosdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ProductosdataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProductoId,
            this.Articulo,
            this.Precio,
            this.Cantidad,
            this.SubTotal});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ProductosdataGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.ProductosdataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.ProductosdataGridView.Location = new System.Drawing.Point(12, 65);
            this.ProductosdataGridView.MultiSelect = false;
            this.ProductosdataGridView.Name = "ProductosdataGridView";
            this.ProductosdataGridView.ReadOnly = true;
            this.ProductosdataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ProductosdataGridView.Size = new System.Drawing.Size(401, 432);
            this.ProductosdataGridView.TabIndex = 2;
            // 
            // ProductoId
            // 
            this.ProductoId.HeaderText = "ProductoId";
            this.ProductoId.Name = "ProductoId";
            this.ProductoId.ReadOnly = true;
            this.ProductoId.Visible = false;
            // 
            // Articulo
            // 
            this.Articulo.HeaderText = "Articulo";
            this.Articulo.Name = "Articulo";
            this.Articulo.ReadOnly = true;
            this.Articulo.Width = 170;
            // 
            // Precio
            // 
            this.Precio.HeaderText = "Precio";
            this.Precio.Name = "Precio";
            this.Precio.ReadOnly = true;
            this.Precio.Width = 60;
            // 
            // Cantidad
            // 
            this.Cantidad.HeaderText = "Cantidad";
            this.Cantidad.Name = "Cantidad";
            this.Cantidad.ReadOnly = true;
            this.Cantidad.Width = 85;
            // 
            // SubTotal
            // 
            this.SubTotal.HeaderText = "SubTotal";
            this.SubTotal.Name = "SubTotal";
            this.SubTotal.ReadOnly = true;
            this.SubTotal.Width = 80;
            // 
            // NuevaCuentadataGridView
            // 
            this.NuevaCuentadataGridView.AllowUserToAddRows = false;
            this.NuevaCuentadataGridView.AllowUserToDeleteRows = false;
            this.NuevaCuentadataGridView.AllowUserToResizeColumns = false;
            this.NuevaCuentadataGridView.AllowUserToResizeRows = false;
            this.NuevaCuentadataGridView.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.NuevaCuentadataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.NuevaCuentadataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.NuevaCuentadataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.NuevaCuentadataGridView.DefaultCellStyle = dataGridViewCellStyle4;
            this.NuevaCuentadataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.NuevaCuentadataGridView.Location = new System.Drawing.Point(534, 65);
            this.NuevaCuentadataGridView.MultiSelect = false;
            this.NuevaCuentadataGridView.Name = "NuevaCuentadataGridView";
            this.NuevaCuentadataGridView.ReadOnly = true;
            this.NuevaCuentadataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.NuevaCuentadataGridView.Size = new System.Drawing.Size(401, 432);
            this.NuevaCuentadataGridView.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "ProductoId";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Articulo";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 170;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Precio";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 60;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Cantidad";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 85;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "SubTotal";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 80;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(646, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(164, 29);
            this.label8.TabIndex = 28;
            this.label8.Text = "Nueva Cuenta";
            // 
            // Mesalabel
            // 
            this.Mesalabel.AutoSize = true;
            this.Mesalabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Mesalabel.Location = new System.Drawing.Point(148, 9);
            this.Mesalabel.Name = "Mesalabel";
            this.Mesalabel.Size = new System.Drawing.Size(72, 29);
            this.Mesalabel.TabIndex = 29;
            this.Mesalabel.Text = "Mesa";
            // 
            // Finalizarbutton
            // 
            this.Finalizarbutton.BackColor = System.Drawing.Color.Red;
            this.Finalizarbutton.FlatAppearance.BorderSize = 0;
            this.Finalizarbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Finalizarbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Finalizarbutton.ForeColor = System.Drawing.Color.White;
            this.Finalizarbutton.Image = ((System.Drawing.Image)(resources.GetObject("Finalizarbutton.Image")));
            this.Finalizarbutton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Finalizarbutton.Location = new System.Drawing.Point(534, 508);
            this.Finalizarbutton.Name = "Finalizarbutton";
            this.Finalizarbutton.Size = new System.Drawing.Size(256, 48);
            this.Finalizarbutton.TabIndex = 30;
            this.Finalizarbutton.Text = "Finalizar Nueva Cuenta";
            this.Finalizarbutton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Finalizarbutton.UseVisualStyleBackColor = false;
            this.Finalizarbutton.Click += new System.EventHandler(this.Finalizarbutton_Click);
            // 
            // Cancelarbutton
            // 
            this.Cancelarbutton.BackColor = System.Drawing.SystemColors.Control;
            this.Cancelarbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cancelarbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cancelarbutton.Location = new System.Drawing.Point(796, 507);
            this.Cancelarbutton.Name = "Cancelarbutton";
            this.Cancelarbutton.Size = new System.Drawing.Size(139, 48);
            this.Cancelarbutton.TabIndex = 31;
            this.Cancelarbutton.Text = "Cancelar";
            this.Cancelarbutton.UseVisualStyleBackColor = false;
            this.Cancelarbutton.Click += new System.EventHandler(this.Cancelarbutton_Click);
            // 
            // Enviarbutton
            // 
            this.Enviarbutton.BackColor = System.Drawing.SystemColors.Control;
            this.Enviarbutton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Enviarbutton.BackgroundImage")));
            this.Enviarbutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Enviarbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Enviarbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Enviarbutton.Location = new System.Drawing.Point(436, 180);
            this.Enviarbutton.Name = "Enviarbutton";
            this.Enviarbutton.Size = new System.Drawing.Size(74, 39);
            this.Enviarbutton.TabIndex = 32;
            this.Enviarbutton.UseVisualStyleBackColor = false;
            this.Enviarbutton.Click += new System.EventHandler(this.Enviarbutton_Click);
            // 
            // Reversarbutton
            // 
            this.Reversarbutton.BackColor = System.Drawing.SystemColors.Control;
            this.Reversarbutton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Reversarbutton.BackgroundImage")));
            this.Reversarbutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Reversarbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Reversarbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Reversarbutton.Location = new System.Drawing.Point(436, 269);
            this.Reversarbutton.Name = "Reversarbutton";
            this.Reversarbutton.Size = new System.Drawing.Size(74, 39);
            this.Reversarbutton.TabIndex = 33;
            this.Reversarbutton.UseVisualStyleBackColor = false;
            this.Reversarbutton.Click += new System.EventHandler(this.Reversarbutton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(671, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 24);
            this.label1.TabIndex = 34;
            this.label1.Text = "Total:";
            // 
            // NCuentaTotallabel
            // 
            this.NCuentaTotallabel.AutoSize = true;
            this.NCuentaTotallabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NCuentaTotallabel.Location = new System.Drawing.Point(733, 38);
            this.NCuentaTotallabel.Name = "NCuentaTotallabel";
            this.NCuentaTotallabel.Size = new System.Drawing.Size(45, 24);
            this.NCuentaTotallabel.TabIndex = 35;
            this.NCuentaTotallabel.Text = "0.00";
            // 
            // MesaTotallabel
            // 
            this.MesaTotallabel.AutoSize = true;
            this.MesaTotallabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MesaTotallabel.Location = new System.Drawing.Point(190, 38);
            this.MesaTotallabel.Name = "MesaTotallabel";
            this.MesaTotallabel.Size = new System.Drawing.Size(45, 24);
            this.MesaTotallabel.TabIndex = 37;
            this.MesaTotallabel.Text = "0.00";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(128, 38);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 24);
            this.label4.TabIndex = 36;
            this.label4.Text = "Total:";
            // 
            // DividirCuenta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(953, 568);
            this.Controls.Add(this.MesaTotallabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.NCuentaTotallabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Reversarbutton);
            this.Controls.Add(this.Enviarbutton);
            this.Controls.Add(this.Cancelarbutton);
            this.Controls.Add(this.Finalizarbutton);
            this.Controls.Add(this.Mesalabel);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.NuevaCuentadataGridView);
            this.Controls.Add(this.ProductosdataGridView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "DividirCuenta";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DividirCuenta";
            ((System.ComponentModel.ISupportInitialize)(this.ProductosdataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NuevaCuentadataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.DataGridView ProductosdataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductoId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Articulo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Precio;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn SubTotal;
        public System.Windows.Forms.DataGridView NuevaCuentadataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label Mesalabel;
        private System.Windows.Forms.Button Finalizarbutton;
        private System.Windows.Forms.Button Cancelarbutton;
        private System.Windows.Forms.Button Enviarbutton;
        private System.Windows.Forms.Button Reversarbutton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label NCuentaTotallabel;
        private System.Windows.Forms.Label MesaTotallabel;
        private System.Windows.Forms.Label label4;
    }
}