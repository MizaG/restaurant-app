﻿namespace CodeRestaurant
{
    partial class RegistroVentas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegistroVentas));
            this.EfectivoradioButton = new System.Windows.Forms.RadioButton();
            this.TarjetaradioButton = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Exactobutton = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.AutorizaciontextBox = new System.Windows.Forms.TextBox();
            this.ValorRecibidotextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Cambiolabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.BancocomboBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.OperadorcomboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TarjetamaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Totallabel = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.Cancelarbutton = new System.Windows.Forms.Button();
            this.Guardarbutton = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.PorcentajetextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.MontotextBox = new System.Windows.Forms.TextBox();
            this.DescuentocheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.CortesiacheckBox = new System.Windows.Forms.CheckBox();
            this.ComentariotextBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.ClientetextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.PorcientoLeycheckBox = new System.Windows.Forms.CheckBox();
            this.PorcientoLeytextBox = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // EfectivoradioButton
            // 
            this.EfectivoradioButton.AutoSize = true;
            this.EfectivoradioButton.Checked = true;
            this.EfectivoradioButton.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EfectivoradioButton.Location = new System.Drawing.Point(85, 19);
            this.EfectivoradioButton.Name = "EfectivoradioButton";
            this.EfectivoradioButton.Size = new System.Drawing.Size(101, 27);
            this.EfectivoradioButton.TabIndex = 0;
            this.EfectivoradioButton.TabStop = true;
            this.EfectivoradioButton.Text = "Efectivo";
            this.EfectivoradioButton.UseVisualStyleBackColor = true;
            this.EfectivoradioButton.Click += new System.EventHandler(this.EfectivoradioButton_Click);
            // 
            // TarjetaradioButton
            // 
            this.TarjetaradioButton.AutoSize = true;
            this.TarjetaradioButton.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TarjetaradioButton.Location = new System.Drawing.Point(299, 19);
            this.TarjetaradioButton.Name = "TarjetaradioButton";
            this.TarjetaradioButton.Size = new System.Drawing.Size(265, 27);
            this.TarjetaradioButton.TabIndex = 1;
            this.TarjetaradioButton.Text = "Tarjeta de Credito/Debito";
            this.TarjetaradioButton.UseVisualStyleBackColor = true;
            this.TarjetaradioButton.Click += new System.EventHandler(this.TarjetaradioButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Exactobutton);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.AutorizaciontextBox);
            this.groupBox1.Controls.Add(this.ValorRecibidotextBox);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.Cambiolabel);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.BancocomboBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.OperadorcomboBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.TarjetamaskedTextBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.TarjetaradioButton);
            this.groupBox1.Controls.Add(this.EfectivoradioButton);
            this.groupBox1.Location = new System.Drawing.Point(12, 145);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(585, 223);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // Exactobutton
            // 
            this.Exactobutton.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.Exactobutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Exactobutton.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Exactobutton.ForeColor = System.Drawing.Color.White;
            this.Exactobutton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Exactobutton.Location = new System.Drawing.Point(63, 126);
            this.Exactobutton.Name = "Exactobutton";
            this.Exactobutton.Size = new System.Drawing.Size(145, 37);
            this.Exactobutton.TabIndex = 23;
            this.Exactobutton.Text = "Exacto";
            this.Exactobutton.UseVisualStyleBackColor = false;
            this.Exactobutton.Click += new System.EventHandler(this.Exactobutton_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(263, 186);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(109, 21);
            this.label10.TabIndex = 13;
            this.label10.Text = "Autorizacion";
            // 
            // AutorizaciontextBox
            // 
            this.AutorizaciontextBox.Enabled = false;
            this.AutorizaciontextBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AutorizaciontextBox.Location = new System.Drawing.Point(378, 183);
            this.AutorizaciontextBox.Name = "AutorizaciontextBox";
            this.AutorizaciontextBox.Size = new System.Drawing.Size(186, 27);
            this.AutorizaciontextBox.TabIndex = 12;
            // 
            // ValorRecibidotextBox
            // 
            this.ValorRecibidotextBox.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ValorRecibidotextBox.Location = new System.Drawing.Point(63, 90);
            this.ValorRecibidotextBox.Name = "ValorRecibidotextBox";
            this.ValorRecibidotextBox.Size = new System.Drawing.Size(145, 31);
            this.ValorRecibidotextBox.TabIndex = 1;
            this.ValorRecibidotextBox.TextChanged += new System.EventHandler(this.ValorRecibidotextBox_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(59, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(149, 22);
            this.label6.TabIndex = 10;
            this.label6.Text = "Valor Recibido:";
            // 
            // Cambiolabel
            // 
            this.Cambiolabel.AutoSize = true;
            this.Cambiolabel.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cambiolabel.ForeColor = System.Drawing.Color.Red;
            this.Cambiolabel.Location = new System.Drawing.Point(150, 170);
            this.Cambiolabel.Name = "Cambiolabel";
            this.Cambiolabel.Size = new System.Drawing.Size(68, 32);
            this.Cambiolabel.TabIndex = 9;
            this.Cambiolabel.Text = "0.00";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(28, 170);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(128, 32);
            this.label4.TabIndex = 8;
            this.label4.Text = "Cambio:";
            // 
            // BancocomboBox
            // 
            this.BancocomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.BancocomboBox.Enabled = false;
            this.BancocomboBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BancocomboBox.FormattingEnabled = true;
            this.BancocomboBox.Location = new System.Drawing.Point(378, 148);
            this.BancocomboBox.Name = "BancocomboBox";
            this.BancocomboBox.Size = new System.Drawing.Size(186, 29);
            this.BancocomboBox.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(312, 151);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 21);
            this.label3.TabIndex = 6;
            this.label3.Text = "Banco";
            // 
            // OperadorcomboBox
            // 
            this.OperadorcomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.OperadorcomboBox.Enabled = false;
            this.OperadorcomboBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OperadorcomboBox.FormattingEnabled = true;
            this.OperadorcomboBox.Location = new System.Drawing.Point(378, 113);
            this.OperadorcomboBox.Name = "OperadorcomboBox";
            this.OperadorcomboBox.Size = new System.Drawing.Size(186, 29);
            this.OperadorcomboBox.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(285, 116);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 21);
            this.label2.TabIndex = 4;
            this.label2.Text = "Operador";
            // 
            // TarjetamaskedTextBox
            // 
            this.TarjetamaskedTextBox.Enabled = false;
            this.TarjetamaskedTextBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TarjetamaskedTextBox.Location = new System.Drawing.Point(378, 80);
            this.TarjetamaskedTextBox.Mask = "0000-0000-0000-0000";
            this.TarjetamaskedTextBox.Name = "TarjetamaskedTextBox";
            this.TarjetamaskedTextBox.Size = new System.Drawing.Size(186, 27);
            this.TarjetamaskedTextBox.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(277, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 21);
            this.label1.TabIndex = 2;
            this.label1.Text = "No. Tarjeta";
            // 
            // Totallabel
            // 
            this.Totallabel.AutoSize = true;
            this.Totallabel.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Totallabel.Location = new System.Drawing.Point(302, 30);
            this.Totallabel.Name = "Totallabel";
            this.Totallabel.Size = new System.Drawing.Size(68, 32);
            this.Totallabel.TabIndex = 11;
            this.Totallabel.Text = "0.00";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(230, 30);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 32);
            this.label8.TabIndex = 10;
            this.label8.Text = "Total:";
            // 
            // Cancelarbutton
            // 
            this.Cancelarbutton.BackColor = System.Drawing.SystemColors.Control;
            this.Cancelarbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cancelarbutton.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cancelarbutton.Location = new System.Drawing.Point(450, 439);
            this.Cancelarbutton.Name = "Cancelarbutton";
            this.Cancelarbutton.Size = new System.Drawing.Size(147, 50);
            this.Cancelarbutton.TabIndex = 18;
            this.Cancelarbutton.Text = "Cancelar";
            this.Cancelarbutton.UseVisualStyleBackColor = false;
            this.Cancelarbutton.Click += new System.EventHandler(this.Cancelarbutton_Click);
            // 
            // Guardarbutton
            // 
            this.Guardarbutton.BackColor = System.Drawing.Color.Gray;
            this.Guardarbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Guardarbutton.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Guardarbutton.ForeColor = System.Drawing.Color.White;
            this.Guardarbutton.Image = ((System.Drawing.Image)(resources.GetObject("Guardarbutton.Image")));
            this.Guardarbutton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Guardarbutton.Location = new System.Drawing.Point(297, 439);
            this.Guardarbutton.Name = "Guardarbutton";
            this.Guardarbutton.Size = new System.Drawing.Size(147, 50);
            this.Guardarbutton.TabIndex = 17;
            this.Guardarbutton.Text = "Guardar";
            this.Guardarbutton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Guardarbutton.UseVisualStyleBackColor = false;
            this.Guardarbutton.Click += new System.EventHandler(this.Guardarbutton_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.PorcentajetextBox);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.MontotextBox);
            this.groupBox2.Controls.Add(this.DescuentocheckBox);
            this.groupBox2.Location = new System.Drawing.Point(12, 104);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(462, 44);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(333, 13);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(25, 22);
            this.label9.TabIndex = 13;
            this.label9.Text = "%";
            // 
            // PorcentajetextBox
            // 
            this.PorcentajetextBox.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PorcentajetextBox.Location = new System.Drawing.Point(362, 10);
            this.PorcentajetextBox.Name = "PorcentajetextBox";
            this.PorcentajetextBox.Size = new System.Drawing.Size(88, 31);
            this.PorcentajetextBox.TabIndex = 14;
            this.PorcentajetextBox.Text = "0";
            this.PorcentajetextBox.TextChanged += new System.EventHandler(this.PorcentajetextBox_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(151, 13);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 22);
            this.label7.TabIndex = 12;
            this.label7.Text = "Monto";
            // 
            // MontotextBox
            // 
            this.MontotextBox.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MontotextBox.Location = new System.Drawing.Point(223, 10);
            this.MontotextBox.Name = "MontotextBox";
            this.MontotextBox.Size = new System.Drawing.Size(88, 31);
            this.MontotextBox.TabIndex = 12;
            this.MontotextBox.Text = "0";
            this.MontotextBox.TextChanged += new System.EventHandler(this.MontotextBox_TextChanged);
            // 
            // DescuentocheckBox
            // 
            this.DescuentocheckBox.AutoSize = true;
            this.DescuentocheckBox.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DescuentocheckBox.Location = new System.Drawing.Point(6, 12);
            this.DescuentocheckBox.Name = "DescuentocheckBox";
            this.DescuentocheckBox.Size = new System.Drawing.Size(129, 26);
            this.DescuentocheckBox.TabIndex = 0;
            this.DescuentocheckBox.Text = "Descuento";
            this.DescuentocheckBox.UseVisualStyleBackColor = true;
            this.DescuentocheckBox.CheckedChanged += new System.EventHandler(this.DescuentocheckBox_CheckedChanged);
            this.DescuentocheckBox.Click += new System.EventHandler(this.DescuentocheckBox_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.CortesiacheckBox);
            this.groupBox3.Location = new System.Drawing.Point(480, 104);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(117, 44);
            this.groupBox3.TabIndex = 20;
            this.groupBox3.TabStop = false;
            // 
            // CortesiacheckBox
            // 
            this.CortesiacheckBox.AutoSize = true;
            this.CortesiacheckBox.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CortesiacheckBox.Location = new System.Drawing.Point(6, 12);
            this.CortesiacheckBox.Name = "CortesiacheckBox";
            this.CortesiacheckBox.Size = new System.Drawing.Size(104, 26);
            this.CortesiacheckBox.TabIndex = 1;
            this.CortesiacheckBox.Text = "Cortesia";
            this.CortesiacheckBox.UseVisualStyleBackColor = true;
            this.CortesiacheckBox.CheckedChanged += new System.EventHandler(this.CortesiacheckBox_CheckedChanged);
            this.CortesiacheckBox.Click += new System.EventHandler(this.CortesiacheckBox_Click);
            // 
            // ComentariotextBox
            // 
            this.ComentariotextBox.Location = new System.Drawing.Point(12, 408);
            this.ComentariotextBox.MaxLength = 150;
            this.ComentariotextBox.Multiline = true;
            this.ComentariotextBox.Name = "ComentariotextBox";
            this.ComentariotextBox.Size = new System.Drawing.Size(277, 83);
            this.ComentariotextBox.TabIndex = 21;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(12, 384);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(108, 21);
            this.label11.TabIndex = 22;
            this.label11.Text = "Comentario:";
            // 
            // ClientetextBox
            // 
            this.ClientetextBox.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClientetextBox.Location = new System.Drawing.Point(94, 76);
            this.ClientetextBox.Name = "ClientetextBox";
            this.ClientetextBox.Size = new System.Drawing.Size(503, 31);
            this.ClientetextBox.TabIndex = 23;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(14, 79);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 22);
            this.label5.TabIndex = 24;
            this.label5.Text = "Cliente";
            // 
            // PorcientoLeycheckBox
            // 
            this.PorcientoLeycheckBox.AutoSize = true;
            this.PorcientoLeycheckBox.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PorcientoLeycheckBox.Location = new System.Drawing.Point(400, 371);
            this.PorcientoLeycheckBox.Name = "PorcientoLeycheckBox";
            this.PorcientoLeycheckBox.Size = new System.Drawing.Size(103, 26);
            this.PorcientoLeycheckBox.TabIndex = 25;
            this.PorcientoLeycheckBox.Text = "10% Ley";
            this.PorcientoLeycheckBox.UseVisualStyleBackColor = true;
            this.PorcientoLeycheckBox.Click += new System.EventHandler(this.PorcientoLeycheckBox_Click);
            // 
            // PorcientoLeytextBox
            // 
            this.PorcientoLeytextBox.Enabled = false;
            this.PorcientoLeytextBox.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PorcientoLeytextBox.Location = new System.Drawing.Point(509, 369);
            this.PorcientoLeytextBox.Name = "PorcientoLeytextBox";
            this.PorcientoLeytextBox.Size = new System.Drawing.Size(88, 31);
            this.PorcientoLeytextBox.TabIndex = 26;
            this.PorcientoLeytextBox.Text = "0.00";
            // 
            // RegistroVentas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(609, 503);
            this.Controls.Add(this.PorcientoLeytextBox);
            this.Controls.Add(this.PorcientoLeycheckBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ClientetextBox);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.ComentariotextBox);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.Cancelarbutton);
            this.Controls.Add(this.Guardarbutton);
            this.Controls.Add(this.Totallabel);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RegistroVentas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RegistroVentas";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton EfectivoradioButton;
        private System.Windows.Forms.RadioButton TarjetaradioButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.MaskedTextBox TarjetamaskedTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox BancocomboBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox OperadorcomboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox ValorRecibidotextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label Cambiolabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label Totallabel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox AutorizaciontextBox;
        private System.Windows.Forms.Button Cancelarbutton;
        private System.Windows.Forms.Button Guardarbutton;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox PorcentajetextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox MontotextBox;
        private System.Windows.Forms.CheckBox DescuentocheckBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox CortesiacheckBox;
        private System.Windows.Forms.TextBox ComentariotextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button Exactobutton;
        private System.Windows.Forms.TextBox ClientetextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox PorcientoLeycheckBox;
        private System.Windows.Forms.TextBox PorcientoLeytextBox;
    }
}