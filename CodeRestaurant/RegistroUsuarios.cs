﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BLL;

namespace CodeRestaurant
{
    public partial class RegistroUsuarios : Form
    {
        private Usuarios usuarios;
        public static int UsuarioId;
        public RegistroUsuarios()
        {
            InitializeComponent();
            usuarios = new Usuarios();
            TipoUsuariocomboBox.DataSource = Enum.GetValues(typeof(TipoUsuario));
            BuscarUsuario();
        }

        private void BuscarUsuario()
        {
            if (usuarios.Buscar(UsuarioId) && UsuarioId > 0)
            {
                NombretextBox.Text = usuarios.Nombre;
                ContrasenatextBox.Text = usuarios.Contrasena;
                TipoUsuariocomboBox.SelectedIndex = usuarios.TipoUsuario;
            }
        }

        public void Limpiar()
        {
            NombretextBox.Clear();
            ContrasenatextBox.Clear();
            TipoUsuariocomboBox.SelectedIndex = 0;
        }

        public bool LlenarDatos()
        {
            bool retorno = true;
            usuarios.Nombre = NombretextBox.Text;
            usuarios.Contrasena = Seguridad.Encriptar(ContrasenatextBox.Text);
            usuarios.FechaInicio = DateTime.Now.ToString();
            usuarios.TipoUsuario = (int)TipoUsuariocomboBox.SelectedValue;
            return retorno;
        }

        private void Guardarbutton_Click(object sender, EventArgs e)
        {
            if (UsuarioId <= 0)
            {
                if (LlenarDatos())
                {
                    if (usuarios.Insertar())
                    {
                        MessageBox.Show("Guardado Correctamente", "Confirmar", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Limpiar();
                    }
                    else
                    {
                        MessageBox.Show("Error Al Guardar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Faltan Datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                if (LlenarDatos())
                {
                    usuarios.IdUsuario = UsuarioId;
                    if (UsuarioId != 1)
                    {
                        if (usuarios.Editar())
                        {
                            MessageBox.Show("Guardado Correctamente", "Confirmar", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            Limpiar();
                        }
                        else
                        {
                            MessageBox.Show("Error Al Guardar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    
                }
                else
                {
                    MessageBox.Show("Faltan Datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void Cancelarbutton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
