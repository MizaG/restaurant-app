﻿using BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CodeRestaurant
{
    public partial class DividirCuenta : Form
    {
        public static int MesaId;
        private Preventa preventa;
        private Mesas mesa;
        public double TotalProd;
        public double TotalNCuenta;
        private Ventas ventas;
        private Configuraciones configuraciones;
        public static int TipoVenta;
        public static int Cambio;
        public static int ValorRecibido;
        public static string Comentario;
        private CrearTicket ticket;
        private Usuarios usuarios;

        public DividirCuenta()
        {
            InitializeComponent();
            preventa = new Preventa();
            mesa = new Mesas();
            ventas = new Ventas();
            configuraciones = new Configuraciones();
            ticket = new CrearTicket();
            usuarios = new Usuarios();
            BuscarPreventa();
            ProductosdataGridView.RowHeadersVisible = false;
            NuevaCuentadataGridView.RowHeadersVisible = false;
            ActTotales();
        }

        private void BuscarPreventa()
        {
            if (preventa.Buscar(MesaId))
            {
                foreach (var item in preventa.producto)
                {
                    ProductosdataGridView.Rows.Add(item.ProductoId, item.Nombre, item.Precio, item.Cantidad, item.Importe);
                }
            }
        }

        private void Enviarbutton_Click(object sender, EventArgs e)
        {
            try
            {
                if (ProductosdataGridView.CurrentRow.Index >= 0)
                {
                    foreach (DataGridViewRow row in ProductosdataGridView.SelectedRows)
                    {
                        bool Encontrado = false;
                        for (int x = 0; x < NuevaCuentadataGridView.Rows.Count; x++)
                        {
                            if (row.Cells[0].Value == NuevaCuentadataGridView.Rows[x].Cells[0].Value)
                            {
                                Encontrado = true;
                                row.Cells[3].Value = (Convert.ToInt32(row.Cells[3].Value) - 1).ToString();
                                NuevaCuentadataGridView.Rows[x].Cells[3].Value = (Convert.ToInt32(NuevaCuentadataGridView.Rows[x].Cells[3].Value) + 1).ToString();
                                ActualizarSubTotales();
                            }
                        }

                        if (Encontrado == false)
                        {
                            row.Cells[3].Value = (Convert.ToInt32(row.Cells[3].Value) - 1).ToString();
                            NuevaCuentadataGridView.Rows.Add(row.Cells[0].Value, row.Cells[1].Value, row.Cells[2].Value, 1.ToString(), row.Cells[4].Value);
                            ActualizarSubTotales();
                        }

                    }

                    if (Convert.ToInt32(ProductosdataGridView.Rows[ProductosdataGridView.CurrentRow.Index].Cells[3].Value) == 0)
                        ProductosdataGridView.Rows.RemoveAt(ProductosdataGridView.CurrentRow.Index);
                }
                ActTotales();
            }
            catch (System.NullReferenceException)
            {

            }
            
        }

        public void ActualizarSubTotales()
        {
            foreach(DataGridViewRow row in ProductosdataGridView.Rows)
            {
                row.Cells[4].Value = (Convert.ToInt32(row.Cells[2].Value) * Convert.ToInt32(row.Cells[3].Value)).ToString();
            }

            foreach (DataGridViewRow row in NuevaCuentadataGridView.Rows)
            {
                row.Cells[4].Value = (Convert.ToInt32(row.Cells[2].Value) * Convert.ToInt32(row.Cells[3].Value)).ToString();
            }
        }

        private void Reversarbutton_Click(object sender, EventArgs e)
        {
            try
            {
                if (NuevaCuentadataGridView.CurrentRow.Index >= 0)
                {
                    foreach (DataGridViewRow row in NuevaCuentadataGridView.SelectedRows)
                    {
                        bool Encontrado = false;
                        for (int x = 0; x < ProductosdataGridView.Rows.Count; x++)
                        {
                            if (row.Cells[0].Value == ProductosdataGridView.Rows[x].Cells[0].Value)
                            {
                                Encontrado = true;
                                row.Cells[3].Value = (Convert.ToInt32(row.Cells[3].Value) - 1).ToString();
                                ProductosdataGridView.Rows[x].Cells[3].Value = (Convert.ToInt32(ProductosdataGridView.Rows[x].Cells[3].Value) + 1).ToString();
                                ActualizarSubTotales();
                            }
                        }

                        if (Encontrado == false)
                        {
                            row.Cells[3].Value = (Convert.ToInt32(row.Cells[3].Value) - 1).ToString();
                            ProductosdataGridView.Rows.Add(row.Cells[0].Value, row.Cells[1].Value, row.Cells[2].Value, 1.ToString(), row.Cells[4].Value);
                            ActualizarSubTotales();
                        }

                    }

                    if (Convert.ToInt32(NuevaCuentadataGridView.Rows[NuevaCuentadataGridView.CurrentRow.Index].Cells[3].Value) == 0)
                        NuevaCuentadataGridView.Rows.RemoveAt(NuevaCuentadataGridView.CurrentRow.Index);
                }
                ActTotales();
            }
            catch (System.NullReferenceException)
            {

            }
            
        }

        private void Cancelarbutton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void ActTotales()
        {
            TotalProd = 0;

            foreach (DataGridViewRow row in ProductosdataGridView.Rows)
            {
                TotalProd += Convert.ToDouble(row.Cells[4].Value);
            }
            MesaTotallabel.Text = TotalProd.ToString("n2");

            TotalNCuenta = 0;

            foreach (DataGridViewRow row in NuevaCuentadataGridView.Rows)
            {
                TotalNCuenta += Convert.ToDouble(row.Cells[4].Value);
            }
            NCuentaTotallabel.Text = TotalNCuenta.ToString("n2");
        }

        public bool LlenarDatosVenta()
        {
            bool retorno = true;

            ventas.UsuarioId = LoginForm.UsuarioId;
            ventas.MesaId = MesaId;
            ventas.MeseroId = 1;
            ventas.TarjetaId = RegistroVentas.TarjetaId;
            ventas.NombreCliente = "Cliente Corriente";
            ventas.Fecha = DateTime.Now.ToString();
            ventas.NCF = configuraciones.NCF;
            ventas.TipoVenta = TipoVenta;
            ventas.ValorRecibido = ValorRecibido;
            ventas.Cambio = Cambio;
            ventas.Comentario = Comentario;
            ventas.TotalVenta = RegistroVentas.montoFinal;


            ventas.LimpiarList();
            for (int i = 0; i < ProductosdataGridView.RowCount; i++)
            {
                ventas.AgregarProductos(Seguridad.ValidarIdEntero(ProductosdataGridView.Rows[i].Cells[0].Value.ToString()), Seguridad.ValidarIdEntero(ProductosdataGridView.Rows[i].Cells[3].Value.ToString()), Seguridad.ValidarDouble(ProductosdataGridView.Rows[i].Cells[4].Value.ToString()));
                //inventario.Buscar((int)VentasdataGridView.Rows[i].Cells[0].Value);
                //inventario.RestarCantidad((inventario.Cantidad - (int)VentasdataGridView.Rows[i].Cells[3].Value), (int)VentasdataGridView.Rows[i].Cells[0].Value);
            }
            return retorno;
        }

        public bool LlenarDatos()
        {
            bool retorno = true;

            preventa.UsuarioId = LoginForm.UsuarioId;
            preventa.Fecha = DateTime.Now.ToString();
            preventa.MesaId = MesaId;
            preventa.NombreCliente = "Cliente Corriente";
            preventa.Total = Seguridad.ValidarFloat(MesaTotallabel.Text);

            preventa.LimpiarList();
            for (int i = 0; i < ProductosdataGridView.RowCount; i++)
            {
                preventa.AgregarProductos(Seguridad.ValidarIdEntero(ProductosdataGridView.Rows[i].Cells[0].Value.ToString()), Seguridad.ValidarIdEntero(ProductosdataGridView.Rows[i].Cells[3].Value.ToString()), Seguridad.ValidarDouble((Seguridad.ValidarDouble(ProductosdataGridView.Rows[i].Cells[4].Value.ToString()) * 0.18).ToString()), Seguridad.ValidarDouble(ProductosdataGridView.Rows[i].Cells[4].Value.ToString()));
                //inventario.Buscar((int)VentasdataGridView.Rows[i].Cells[0].Value);
                //inventario.RestarCantidad((inventario.Cantidad - (int)VentasdataGridView.Rows[i].Cells[3].Value), (int)VentasdataGridView.Rows[i].Cells[0].Value);
            }
            return retorno;
        }

        private void Finalizarbutton_Click(object sender, EventArgs e)
        {
            //mesa.MesaId = MesaId;
            RegistroVentas.monto = Seguridad.ValidarDouble(NCuentaTotallabel.Text);
            RegistroVentas rVenta = new RegistroVentas();
            rVenta.ShowDialog();
            if (RegistroVentas.Sigo && LlenarDatosVenta())
            {
                    if (ventas.Insertar())
                    {
                        DialogResult result;
                        result = MessageBox.Show("Quieres Imprimir?", "Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (result == DialogResult.Yes)
                        {
                            Imprimir();

                            
                        }
                    }
                    else
                    {
                        MessageBox.Show("Error al Guardar la Venta");
                    }

                if (LlenarDatos())
                {
                    if (preventa.Editar())
                    {
                        mesa.MesaId = MesaId;

                    }
                    else
                    {
                        MessageBox.Show("Error al Modificar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }

            this.Close();
        }

        private void Imprimir()
        {
            ticket.TextoCentro("TAIRIL");
            ticket.TextoCentro("Bar and Lounge");
            ticket.TextoCentro("C/Principal, Bomba de Cenovi R.D");
            ticket.TextoCentro("(829) 423-6196");
            ticket.LineasIgual();
            ticket.TextoCentro("FACTURA");
            ticket.LineasIgual();
            ticket.TextoIzquierda("RNC: " + configuraciones.RNC);
            ticket.TextoIzquierda("Factura NO: " + NumeroFactura(ventas.MaximoId()));
            ticket.TextoIzquierda("Fecha: " + DateTime.Now.ToString("dd/M/yyyy"));
            ticket.TextoIzquierda("Hora: " + DateTime.Now.ToString("hh:mm:ss"));
            ticket.TextoIzquierda("NCF: " + configuraciones.NCF);
            if (MesaId < 25)
            {
                ticket.TextoIzquierda("Mesa: " + MesaId);
            }

            ticket.LineasBaja();
            ticket.EncabezadoVenta();//nombre del articulo, canti, precio
            ticket.LineasGuion();
            for (int i = 0; i < ProductosdataGridView.RowCount; i++)
            {
                ticket.AgregarProducto(ProductosdataGridView.Rows[i].Cells[1].Value.ToString(), Seguridad.ValidarIdEntero(ProductosdataGridView.Rows[i].Cells[3].Value.ToString()), Seguridad.ValidarIdEntero(ProductosdataGridView.Rows[i].Cells[2].Value.ToString()));
            }
            ticket.LineasIgual();
            ticket.AgregarTotales("Subtotal", Seguridad.ValidarDouble(NCuentaTotallabel.Text) - (Seguridad.ValidarDouble(NCuentaTotallabel.Text) * 0.18));
            ticket.AgregarTotales("Total ITBIS", (Seguridad.ValidarDouble(NCuentaTotallabel.Text) * 0.18));
            ticket.AgregarTotales("Descuento", (Seguridad.ValidarDouble(NCuentaTotallabel.Text) - RegistroVentas.montoFinal));
            ticket.TextoDerecha("------------");
            ticket.AgregarTotales("Total General", RegistroVentas.montoFinal);//para agregar los totales efectivo, cambio, etc..
            ticket.LineasIgual();
            ticket.TextoCentro("");
            ticket.TextoCentro("");
            ticket.TextoIzquierda("______________             _____________");
            ticket.TextoIzquierda("Despachado por             Recibido Por");
            ticket.TextoCentro("Gracias por Preferirnos");
            ticket.TextoCentro("Le Atendio: " + usuarios.Nombre);
            //MessageBox.Show(ticket.GetLine().ToString());
            ticket.TextoCentro("");
            ticket.TextoCentro("");
            ticket.TextoCentro("");
            ticket.TextoCentro("");
            //ticket.CortaTicket();
            ticket.ImprimirTicket("Microsoft XPS Document Writer");
            //nombre impresora-Microsoft XPS Document Writer-EPSON-Microsoft Print to PDF
            //Limpiar();
        }

        private string NumeroFactura(int id)
        {
            string cadena = "", numero = id + "";
            if (numero.Length == 1)
            {
                cadena = "000" + numero;
            }
            else if (numero.Length == 2)
            {
                cadena = "00" + numero;
            }
            else if (numero.Length == 3)
            {
                cadena = "0" + numero;
            }
            else
            {
                cadena = numero;
            }
            return cadena;
        }
    }
}
