﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BLL;

namespace CodeRestaurant
{
    public partial class NotaForm : Form
    {
        public static bool Check;
        public NotaForm()
        {
            InitializeComponent();
            Check = false;
            if (PrecuentaFormcs.Nota.Length > 0)
            {
                NotatextBox.Text = PrecuentaFormcs.Nota;
            }
        }

        private void Guardarbutton_Click(object sender, EventArgs e)
        {
            if(NotatextBox.Text != "")
            {
                PrecuentaFormcs.Nota = NotatextBox.Text;
                Check = true;
                this.Close();
            }
            
        }

        private void Cancelarbutton_Click(object sender, EventArgs e)
        {
            this.Close();
            Check = false;
        }
    }
}
