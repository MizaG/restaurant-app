﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CodeRestaurant
{
    public partial class VentanaBloqueo : Form
    {
        public VentanaBloqueo()
        {
            InitializeComponent();
        }

        private void VentanaBloqueo_Load(object sender, EventArgs e)
        {
            PasstextBox.Focus();
        }

        private void Salirbutton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void PasstextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == Convert.ToChar(Keys.Enter))
            {
                if(PasstextBox.Text == LoginForm.Pass)
                {
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Contraseña incorrecta!");
                    PasstextBox.Text = "";
                }
            }
        }
    }
}
