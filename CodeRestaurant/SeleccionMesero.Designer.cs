﻿namespace CodeRestaurant
{
    partial class SeleccionMesero
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MeserosdataGridView = new System.Windows.Forms.DataGridView();
            this.Cancelarbutton = new System.Windows.Forms.Button();
            this.Seleccionarbutton = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.MeserosdataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // MeserosdataGridView
            // 
            this.MeserosdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.MeserosdataGridView.Location = new System.Drawing.Point(12, 74);
            this.MeserosdataGridView.Name = "MeserosdataGridView";
            this.MeserosdataGridView.Size = new System.Drawing.Size(300, 150);
            this.MeserosdataGridView.TabIndex = 0;
            // 
            // Cancelarbutton
            // 
            this.Cancelarbutton.BackColor = System.Drawing.SystemColors.Control;
            this.Cancelarbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cancelarbutton.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cancelarbutton.Location = new System.Drawing.Point(165, 243);
            this.Cancelarbutton.Name = "Cancelarbutton";
            this.Cancelarbutton.Size = new System.Drawing.Size(147, 50);
            this.Cancelarbutton.TabIndex = 26;
            this.Cancelarbutton.Text = "Cancelar";
            this.Cancelarbutton.UseVisualStyleBackColor = false;
            // 
            // Seleccionarbutton
            // 
            this.Seleccionarbutton.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.Seleccionarbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Seleccionarbutton.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Seleccionarbutton.ForeColor = System.Drawing.Color.White;
            this.Seleccionarbutton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Seleccionarbutton.Location = new System.Drawing.Point(12, 243);
            this.Seleccionarbutton.Name = "Seleccionarbutton";
            this.Seleccionarbutton.Size = new System.Drawing.Size(147, 50);
            this.Seleccionarbutton.TabIndex = 25;
            this.Seleccionarbutton.Text = "Seleccionar";
            this.Seleccionarbutton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Seleccionarbutton.UseVisualStyleBackColor = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(35, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(250, 30);
            this.label8.TabIndex = 27;
            this.label8.Text = "Seleccionar Mesero";
            // 
            // SeleccionMesero
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(324, 305);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.Cancelarbutton);
            this.Controls.Add(this.Seleccionarbutton);
            this.Controls.Add(this.MeserosdataGridView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "SeleccionMesero";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SeleccionMesero";
            ((System.ComponentModel.ISupportInitialize)(this.MeserosdataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView MeserosdataGridView;
        private System.Windows.Forms.Button Cancelarbutton;
        private System.Windows.Forms.Button Seleccionarbutton;
        private System.Windows.Forms.Label label8;
    }
}