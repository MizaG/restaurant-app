﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BLL;

namespace CodeRestaurant
{
    public partial class VentanaCuadre : Form
    {
        private Ventas ventas;
        private Cajas cajas;
        private double TotalDetalle;
        private double TotalCuadre;
        private double TotalCredito;
        private CrearTicket ticket;

        public VentanaCuadre()
        {
            InitializeComponent();
            ventas = new Ventas();
            cajas = new Cajas();
            ticket = new CrearTicket();
        }

        private void CargarTotales()
        {
            cajas.Buscar(0);
            BalanceIniciallabel.Text = "RD $" + cajas.Balance.ToString("N2");
            TotalDetalle = cajas.Balance + Seguridad.ValidarDouble(ventas.TotalVentas(DateTime.Today.ToString("yyyy-MM-dd"),LoginForm.UsuarioId).Rows[0]["Total"].ToString());
            TotalCredito = Seguridad.ValidarDouble(ventas.TotalCredito(DateTime.Today.ToString("yyyy-MM-dd"), LoginForm.UsuarioId).Rows[0]["Total"].ToString());
            TotalDetallelabel.Text = "RD $" + TotalDetalle.ToString("N2");
            VentasTarjetaslabel.Text = "RD $" + TotalCredito.ToString("N2");
            double efectivo = Seguridad.ValidarDouble(ventas.TotalDebito(DateTime.Today.ToString("yyyy-MM-dd"), LoginForm.UsuarioId).Rows[0]["Total"].ToString());
            VentasEfectivolabel.Text = "RD $" + efectivo.ToString("N2");
            if (VentasTarjetaslabel.Text.Length == 4) { VentasTarjetaslabel.Text = "RD $0"; }
            if (VentasEfectivolabel.Text.Length == 4) { VentasEfectivolabel.Text = "RD $0"; }
            if (TotalDetallelabel.Text.Length == 4) { TotalDetallelabel.Text = "RD $0"; }
            Totaltimer.Start();
        }

        private void Cancelarbutton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void VentanaCuadre_Load(object sender, EventArgs e)
        {
            CargarTotales();
        }

        private void Cant1textBox_TextChanged(object sender, EventArgs e)
        {
            SubT1textBox.Text = (Seguridad.ValidarIdEntero(Cant1textBox.Text) * 1).ToString();
        }

        private void Cant5textBox_TextChanged(object sender, EventArgs e)
        {
            SubT5textBox.Text = (Seguridad.ValidarIdEntero(Cant5textBox.Text) * 5).ToString();
        }

        private void Cant10textBox_TextChanged(object sender, EventArgs e)
        {
            SubT10textBox.Text = (Seguridad.ValidarIdEntero(Cant10textBox.Text) * 10).ToString();
        }

        private void Cant25textBox_TextChanged(object sender, EventArgs e)
        {
            SubT25textBox.Text = (Seguridad.ValidarIdEntero(Cant25textBox.Text) * 25).ToString();
        }

        private void Cant50textBox_TextChanged(object sender, EventArgs e)
        {
            SubT50textBox.Text = (Seguridad.ValidarIdEntero(Cant50textBox.Text) * 50).ToString();
        }

        private void Cant100textBox_TextChanged(object sender, EventArgs e)
        {
            SubT100textBox.Text = (Seguridad.ValidarIdEntero(Cant100textBox.Text) * 100).ToString();
        }

        private void Cant200textBox_TextChanged(object sender, EventArgs e)
        {
            SubT200textBox.Text = (Seguridad.ValidarIdEntero(Cant200textBox.Text) * 200).ToString();
        }

        private void Cant500textBox_TextChanged(object sender, EventArgs e)
        {
            SubT500textBox.Text = (Seguridad.ValidarIdEntero(Cant500textBox.Text) * 500).ToString();
        }

        private void Cant1000textBox_TextChanged(object sender, EventArgs e)
        {
            SubT1000textBox.Text = (Seguridad.ValidarIdEntero(Cant1000textBox.Text) * 1000).ToString();
        }

        private void Cant2000textBox_TextChanged(object sender, EventArgs e)
        {
            SubT2000textBox.Text = (Seguridad.ValidarIdEntero(Cant2000textBox.Text) * 2000).ToString();
        }

        private void Totaltimer_Tick(object sender, EventArgs e)
        {
            TotalCuadre = Seguridad.ValidarIdEntero(SubT1textBox.Text) + Seguridad.ValidarIdEntero(SubT5textBox.Text) + Seguridad.ValidarIdEntero(SubT10textBox.Text) +
                Seguridad.ValidarIdEntero(SubT25textBox.Text) + Seguridad.ValidarIdEntero(SubT50textBox.Text) + Seguridad.ValidarIdEntero(SubT100textBox.Text) +
                Seguridad.ValidarIdEntero(SubT200textBox.Text) + Seguridad.ValidarIdEntero(SubT500textBox.Text) + Seguridad.ValidarIdEntero(SubT1000textBox.Text) +
                Seguridad.ValidarIdEntero(SubT2000textBox.Text);

            TotalCuadrelabel.Text = "RD $" + TotalCuadre.ToString("N2");
            Diferencialabel.Text = "RD $" + (TotalCuadre - (TotalDetalle - TotalCredito)).ToString("N2");
        }

        private void CerrarCajabutton_Click(object sender, EventArgs e)
        {
            DialogResult result;
            result = MessageBox.Show("Quieres Cerrar Caja?", "Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                if (!cajas.CerrarCaja())
                {
                    MessageBox.Show("Error al Cerrar la Caja","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                }
                else
                {
                    ImprimirCuadre();
                    this.Close();
                }
            }
            
        }

        private void ImprimirCuadre()
        {
            ticket.TextoCentro("TAIRIL");
            ticket.TextoCentro("Bar and Lounge");
            ticket.TextoCentro("C/Principal, Bomba de Cenovi R.D");
            ticket.TextoCentro("(829) 423-6196");
            ticket.TextoCentro(" ");
            ticket.TextoCentro("Usuario: " + LoginForm.NombreUsuario);
            ticket.TextoCentro(" ");
            ticket.TextoCentro("Cuadre de Caja");
            ticket.TextoIzquierda("Fecha: " + DateTime.Now.ToString("dd/M/yyyy"));
            ticket.TextoIzquierda("Hora: " + DateTime.Now.ToString("hh:mm:ss"));
            ticket.LineasIgual();
            ticket.AgregarTotales("Facturas -->", Seguridad.ValidarDouble(ventas.TotalDebito(DateTime.Today.ToString("yyyy-MM-dd"), LoginForm.UsuarioId).Rows[0]["Total"].ToString()));
            ticket.LineasIgual();
            ticket.AgregarTotales("APERTURA -->", cajas.Balance);
            ticket.AgregarTotales("Total Tarjeta -->", TotalCredito);
            ticket.LineasIgual();
            ticket.AgregarTotales("Total Efectivo Sistema-->", Seguridad.ValidarDouble(ventas.TotalDebito(DateTime.Today.ToString("yyyy-MM-dd"), LoginForm.UsuarioId).Rows[0]["Total"].ToString()) +  cajas.Balance);
            ticket.AgregarTotales("Total Efectivo Caja -->", TotalCuadre);
            ticket.TextoDerecha("---------");
            ticket.AgregarTotales("Diferencia -->", (TotalCuadre - (TotalDetalle - TotalCredito)));
            ticket.TextoCentro("");
            ticket.TextoCentro("");
            ticket.TextoCentro("");
            ticket.TextoCentro("");
            ticket.CortaTicket();
            ticket.ImprimirTicket("Microsoft XPS Document Writer");
            //nombre impresora-Microsoft XPS Document Writer-Microsoft Print to PDF-Generic / Text Only - Star TSP100 Cutter (TSP143)            
        }
    }
}
