﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BLL;

namespace CodeRestaurant
{
    public partial class RegistroMeseros : Form
    {
        private Meseros mesero;
        public static int MeseroId;

        public RegistroMeseros()
        {
            InitializeComponent();
            mesero = new Meseros();
            BuscarMesero();
        }

        private void BuscarMesero()
        {
            if (mesero.Buscar(MeseroId) && MeseroId > 0)
            {
                NombretextBox.Text = mesero.Nombre;
            }
        }

        private bool LlenarDatos()
        {
            bool retorno = true;
            mesero.Nombre = NombretextBox.Text;
            return retorno;
        }

        private void Guardarbutton_Click(object sender, EventArgs e)
        {
            if(NombretextBox.Text != "")
            {
                if (MeseroId <= 0)
                {
                    if (LlenarDatos())
                    {
                        if (mesero.Insertar())
                        {
                            MessageBox.Show("Guardado Correctamente", "Confirmar", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            NombretextBox.Clear();
                        }
                        else
                        {
                            MessageBox.Show("Error Al Guardar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Faltan Datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    if (LlenarDatos())
                    {
                        mesero.MeseroId = MeseroId;
                        if (mesero.Editar())
                        {
                            MessageBox.Show("Modificado Correctamente", "Confirmar", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            MeseroId = 0;
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("Error Al Modificar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Faltan Datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            
        }

        private void Cancelarbutton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
