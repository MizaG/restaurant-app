﻿using BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CodeRestaurant
{
    public partial class LoginForm : Form
    {
        int intentos = 0;
        Usuarios usuario = new Usuarios();
        public static string NombreUsuario;
        public static int UsuarioId;
        public static string Pass;

        public LoginForm()
        {
            InitializeComponent();
            UsuariotextBox.Text = "Francis";
            ContrasenatextBox.Text = "fc1010";
        }

        private void VerificarCaja(RegistroCaja caja)
        {
            if (caja.Estado())
            {
                caja.AbrirFormMain();
            }
            else
            {
                caja.Show();
                this.Hide();
            }
        }

        public void IniciarSesion()
        {
            if (UsuariotextBox.Text.Length > 0 && ContrasenatextBox.Text.Length > 0)
            {

                usuario.Nombre = UsuariotextBox.Text;
                usuario.Contrasena = Seguridad.Encriptar(ContrasenatextBox.Text);
                if (usuario.InicioSesion() && usuario.UpdateFecha())
                {
                    this.Visible = false;
                    if (usuario.TipoUsuario == 1)
                    {
                        MainForm main = new MainForm();
                        main.UsuarioarchivoToolStripMenuItem.Visible = false;
                        main.inventarioToolStripMenuItem.Visible = false;
                        main.configuracionesToolStripMenuItem.Visible = false;
                        main.backupToolStripMenuItem.Visible = false;
                        main.productosToolStripMenuItem.Visible = false;
                        main.proveedoresToolStripMenuItem.Visible = false;
                        main.meserosToolStripMenuItem.Visible = false;
                    }
                    RegistroCaja caja = new RegistroCaja();
                    UsuarioId = usuario.IdUsuario;                    
                    NombreUsuario = usuario.Nombre;
                    VerificarCaja(caja);
                }
                else
                {
                    intentos++;
                    if (intentos >= 3)
                        this.Close();
                    MessageBox.Show("Usuario Incorrecto \n" + intentos + " Intentos Incorrectos.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Faltan Campos.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Iniciarbutton_Click(object sender, EventArgs e)
        {
            IniciarSesion();
            Pass = ContrasenatextBox.Text;
        }

        private void Cancelarbutton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ContrasenatextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                IniciarSesion();
            }
        }
    }
}
