﻿using BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CodeRestaurant
{
    public partial class RegistroVentas : Form
    {
        public static bool Sigo = false;
        public static double monto;
        public static double montoFinal;
        public static double cambio;
        private Tarjetas tarjeta;
        public static int TarjetaId;
        double PorcientoLey;
        public bool PorcientoLeyAplicado = false;
        public bool ValidacionPorciento = true;
        public static bool ValidacionAdm = false;

        public RegistroVentas()
        {
            InitializeComponent();
            tarjeta = new Tarjetas();
            CargarForm();
        }

        public void CargarForm()
        {
            ValorRecibidotextBox.Focus();
            BancocomboBox.DataSource = Enum.GetValues(typeof(Bancos));
            OperadorcomboBox.DataSource = Enum.GetValues(typeof(Operadores)); ;
            Totallabel.Text = monto.ToString("N2");
            montoFinal = monto;
        }

        public bool LlenarDatos()
        {
            bool retorno = true;
            if (TarjetaradioButton.Checked)
            {
                tarjeta.Banco = Seguridad.ValidarIdEntero(BancocomboBox.SelectedValue.ToString());
                tarjeta.Operador = Seguridad.ValidarIdEntero(OperadorcomboBox.SelectedValue.ToString());
                if (TarjetamaskedTextBox.Text.Length > 8)
                {
                    tarjeta.NoTarjeta = TarjetamaskedTextBox.Text;
                }
                else
                {
                    retorno = false;
                }
                if (AutorizaciontextBox.Text.Length == 6)
                {
                    tarjeta.Autorizacion = AutorizaciontextBox.Text;
                }
                else
                {
                    retorno = false;
                }
            }
            else
            {
                retorno = false;
            }

            return retorno;
        }

        public void CalcularMonto()
        {

            if (Seguridad.ValidarIdEntero(PorcentajetextBox.Text) > 0)
            {
                Totallabel.Text = (monto - ((monto * Seguridad.ValidarDouble(PorcentajetextBox.Text)) / 100)).ToString("N2");
                montoFinal = Seguridad.ValidarDouble(Totallabel.Text);
            }
            else if (Seguridad.ValidarIdEntero(MontotextBox.Text) > 0)
            {
                Totallabel.Text = (monto - Seguridad.ValidarDouble(MontotextBox.Text)).ToString("N2");
                montoFinal = Seguridad.ValidarDouble(Totallabel.Text);
            }
        }

        public void LlenarDatosVenta()
        {
            PrecuentaFormcs.Comentario = ComentariotextBox.Text;
            PrecuentaFormcs.Cambio = Seguridad.ValidarIdEntero(Cambiolabel.Text);
            PrecuentaFormcs.ValorRecibido = Seguridad.ValidarIdEntero(ValorRecibidotextBox.Text);
            PrecuentaFormcs.NombreCliente = ClientetextBox.Text;
            if (EfectivoradioButton.Checked)
            {
                PrecuentaFormcs.TipoVenta = 1;
            }
            else
            {
                PrecuentaFormcs.TipoVenta = 0;
            }
            if (CortesiacheckBox.Checked)
            {
                PrecuentaFormcs.TipoVenta = 2;
            }
        }

        private void TarjetaradioButton_Click(object sender, EventArgs e)
        {
            CalcularMonto();
            ValorRecibidotextBox.Enabled = false;
            Exactobutton.Enabled = false;
            TarjetamaskedTextBox.Enabled = true;
            OperadorcomboBox.Enabled = true;
            BancocomboBox.Enabled = true;
            AutorizaciontextBox.Enabled = true;
        }

        private void EfectivoradioButton_Click(object sender, EventArgs e)
        {
            ValorRecibidotextBox.Enabled = true;
            Exactobutton.Enabled = true;
            TarjetamaskedTextBox.Enabled = false;
            OperadorcomboBox.Enabled = false;
            BancocomboBox.Enabled = false;
            AutorizaciontextBox.Enabled = false;
        }

        private void Guardarbutton_Click(object sender, EventArgs e)
        {
            LlenarDatosVenta();
            if (TarjetaradioButton.Checked)
            {
                if (LlenarDatos())
                {
                    if (tarjeta.Insertar())
                    {
                        TarjetaId = tarjeta.MaximoId();
                        this.Close();
                        Sigo = true;
                    }
                    else
                    {
                        MessageBox.Show("Error Al Guardar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Verifique los datos de la tarjeta", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else if (EfectivoradioButton.Checked && ValorRecibidotextBox.Text.Length > 0 && Seguridad.ValidarDouble(ValorRecibidotextBox.Text) >= montoFinal)
            {
                this.Close();
                Sigo = true;
            }
            else if (CortesiacheckBox.Checked)
            {
                this.Close();
                Sigo = true;
            }
            else
            {
                MessageBox.Show("Verifique los datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void Cancelarbutton_Click(object sender, EventArgs e)
        {
            this.Close();
            Sigo = false;
        }

        private void Exactobutton_Click(object sender, EventArgs e)
        {
            if (montoFinal > 0)
            {
                ValorRecibidotextBox.Text = montoFinal.ToString();
            }
            else
            {
                ValorRecibidotextBox.Text = monto.ToString();
                montoFinal = monto;
            }
        }

        private void MontotextBox_TextChanged(object sender, EventArgs e)
        {
            if (DescuentocheckBox.Checked)
            {
                CalcularMonto();
            }
            else
            {
                DialogResult result;
                result = MessageBox.Show("Activar Descuento?", "", MessageBoxButtons.YesNo);

                if (result == DialogResult.Yes)
                {
                    DescuentocheckBox.Checked = true;
                }
            }

        }

        private void PorcentajetextBox_TextChanged(object sender, EventArgs e)
        {
            if (DescuentocheckBox.Checked)
            {
                CalcularMonto();
            }
            else
            {
                DialogResult result;

                // Displays the MessageBox.

                result = MessageBox.Show("Activar Descuento?", "", MessageBoxButtons.YesNo);

                if (result == DialogResult.Yes)
                {
                    DescuentocheckBox.Checked = true;
                }
            }
            if (MontotextBox.Text.Length == 0)
            {
                MontotextBox.Text = "0";
                CalcularMonto();
            }
        }

        private void ValorRecibidotextBox_TextChanged(object sender, EventArgs e)
        {
            if (montoFinal > 0)
            {
                Cambiolabel.Text = (Seguridad.ValidarIdEntero(ValorRecibidotextBox.Text) - montoFinal).ToString();
            }
            else
            {
                Cambiolabel.Text = (Seguridad.ValidarIdEntero(ValorRecibidotextBox.Text) - monto).ToString();
            }
            if (ValorRecibidotextBox.Text.Length == 0)
            {
                Cambiolabel.Text = "0";
            }
            cambio = Seguridad.ValidarDouble(Cambiolabel.Text);
        }

        private void CortesiacheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (CortesiacheckBox.Checked)
            {
                Totallabel.Text = "0.00";
                montoFinal = 0;
            }
            else
            {
                Totallabel.Text = monto.ToString("N2");
                montoFinal = monto;
            }
        }

        private void PorcientoLeycheckBox_Click(object sender, EventArgs e)
        {
            if (PorcientoLeycheckBox.Checked)
            {
                if (MessageBox.Show("Esta seguro que desea aplicar el 10% de Ley?", "10% Ley", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    PorcientoLey = Convert.ToDouble(Totallabel.Text) * 0.10;
                    PorcientoLeytextBox.Text = PorcientoLey.ToString("N2");
                    Totallabel.Text = (Convert.ToDouble(Totallabel.Text) + PorcientoLey).ToString("N2");
                    montoFinal = Seguridad.ValidarDouble(Totallabel.Text);
                }
                else
                {
                    PorcientoLeycheckBox.Checked = false;
                }
            }
            else if (!PorcientoLeycheckBox.Checked)
            {
                Totallabel.Text = (Convert.ToDouble(Totallabel.Text) - PorcientoLey).ToString("N2");
                PorcientoLeytextBox.Text = 0.ToString("N2");
                montoFinal = Seguridad.ValidarDouble(Totallabel.Text);
            }
        }

        private void DescuentocheckBox_Click(object sender, EventArgs e)
        {
            if (DescuentocheckBox.Checked)
            {
                ClaveAdm claveAdm = new ClaveAdm();
                claveAdm.ShowDialog();
                if (ValidacionAdm)
                {
                    DescuentocheckBox.Checked = true;
                    ValidacionAdm = false;
                }
                else
                {
                    DescuentocheckBox.Checked = false;
                }
            }
        }

        private void CortesiacheckBox_Click(object sender, EventArgs e)
        {
            if (CortesiacheckBox.Checked)
            {
                ClaveAdm claveAdm = new ClaveAdm();
                claveAdm.ShowDialog();
                if (ValidacionAdm)
                {
                    CortesiacheckBox.Checked = true;
                    ValidacionAdm = false;
                }
                else
                {
                    CortesiacheckBox.Checked = false;
                }
            }
        }

        private void DescuentocheckBox_CheckedChanged(object sender, EventArgs e)
        {
            CalcularMonto();
            if (!DescuentocheckBox.Checked)
            {
                Totallabel.Text = monto.ToString("N2");
                montoFinal = monto;
            }
        }
    }
}
