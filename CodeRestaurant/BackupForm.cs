﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;

namespace CodeRestaurant
{
    public partial class BackupForm : Form
    {
        Backup backup;
        Configuraciones configuracion;
        public BackupForm()
        {
            InitializeComponent();
            backup = new Backup();
            configuracion = new Configuraciones();
            configuracion.Buscar(1);
        }

        private void Backupbutton_Click(object sender, EventArgs e)
        {
            backup.Nombre = "-Backup-CodeRestaurant_db.bak";
            backup.Fecha = DateTime.Today.Day +"-"+ DateTime.Today.Month +"-"+ DateTime.Today.Year + "-"+ DateTime.Now.Hour +"-"+DateTime.Now.Minute+"-"+DateTime.Now.Second;
            backup.Direccion = configuracion.RutaBackup;
            
            if (backup.Crear())
            {
                MessageBox.Show("Creado Correctamente", "Correcto", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
            {
                MessageBox.Show("Error al Crear Backup", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
