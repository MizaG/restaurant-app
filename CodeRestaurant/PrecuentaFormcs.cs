﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BLL;

namespace CodeRestaurant
{
    public partial class PrecuentaFormcs : Form
    {
        private bool esIgual = false;
        private Preventa preventa;
        private Ventas ventas;
        private Productos productos;
        private Mesas mesa;
        private CrearTicket ticket;
        private Usuarios usuarios;
        private Configuraciones configuraciones;
        public static int MesaId;
        public static string Nota = "";
        //Ventana de Corbo
        public static int Cambio;
        public static int ValorRecibido;
        public static string Comentario;
        public static int TipoVenta;
        public double Total;
        public static bool EliminarArticulo = false;
        public static string NombreCliente="";

        public PrecuentaFormcs()
        {
            InitializeComponent();
            preventa = new Preventa();
            ventas = new Ventas();
            productos = new Productos();
            mesa = new Mesas();
            ticket = new CrearTicket();
            configuraciones = new Configuraciones();
            usuarios = new Usuarios();
            ListarComidas();
            ListarEnsaladas();
            ListarBebidas();
            ListarHelados();
            ListarTragos();
            CarcgarConfiguracion();
            ProductosdataGridView.RowHeadersVisible = false;
            CargarMeseros();
            BuscarPreventa();            
        }

        public void CargarMeseros()
        {
            Meseros meseros = new Meseros();
            DataTable dt = meseros.Listado(" * ", " Activo = 0 ", "");
            Dictionary<string, string> ListaMeseros = new Dictionary<string, string>();

            //ListaMeseros.Add("0", "Seleccionar Mesero");//crear seleccionar mesero

            //foreach (DataRow row in dt.Rows)
            //{
            //    ListaMeseros.Add(row["MeseroId"].ToString(), row["Nombre"].ToString());
            //}

            MeserocomboBox.DataSource = meseros.Listado(" * ", " Activo = 0 ", "");//new BindingSource(ListaMeseros, null);
            MeserocomboBox.DisplayMember = "Nombre";//Value
            MeserocomboBox.ValueMember = "MeseroId";//Key
        }

        private void CarcgarConfiguracion()
        {
            configuraciones.Buscar(1);
            usuarios.Buscar(LoginForm.UsuarioId);
        }

        private void BuscarPreventa()
        {
            if (preventa.Buscar(MesaId))
            {
                MontoTotallabel.Text = preventa.Total.ToString();
                MeserocomboBox.SelectedValue = preventa.MeseroId;

                if (preventa.Nota.Length > 0)
                {
                    Nota = preventa.Nota;
                    Notabutton.BackColor = Color.OrangeRed;
                }
                foreach (var item in preventa.producto)
                {
                    bool Encontrado = false;
                    foreach(DataGridViewRow row in ProductosdataGridView.Rows)
                    {
                        if(item.ProductoId == Seguridad.ValidarIdEntero(row.Cells[0].Value.ToString()))
                        {
                            row.Cells[3].Value = (Seguridad.ValidarIdEntero(row.Cells[3].Value.ToString()) + 1).ToString();

                            Encontrado = true;
                            break;
                        }
                        
                    }
                    if (!Encontrado)
                    {
                        ProductosdataGridView.Rows.Add(item.ProductoId, item.Nombre, item.Precio, item.Cantidad, item.Importe);
                    }
                    
                }
            }
        }

        private void handlerComun_Click(object sender, EventArgs e)
        {
            esIgual = false;

            if (ProductosdataGridView.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in ProductosdataGridView.Rows)
                {
                    if (Seguridad.ValidarIdEntero(row.Cells[0].Value.ToString()) == Seguridad.ValidarIdEntero(((Button)sender).Tag.ToString()))
                    {
                        row.Cells[3].Value = Convert.ToInt32(row.Cells[3].Value) + 1;
                        row.Cells[4].Value = Convert.ToInt32(row.Cells[3].Value) * Convert.ToInt32(row.Cells[2].Value);
                        esIgual = true;
                        ActualizarTotal();
                        break;
                    }

                }

                if (esIgual == false)
                {
                    Productos p = new Productos();
                    DataGridViewRow fila = new DataGridViewRow();
                    fila.CreateCells(ProductosdataGridView);
                    fila.Cells[0].Value = ((Button)sender).Tag;

                    if (p.Buscar(Convert.ToUInt16(((Button)sender).Tag)))
                    {
                        fila.Cells[1].Value = p.Nombre;
                        fila.Cells[2].Value = p.Precio.ToString();
                        fila.Cells[3].Value = 1;
                        fila.Cells[4].Value = p.Precio.ToString();

                        ProductosdataGridView.RowHeadersVisible = false;
                        ProductosdataGridView.Rows.Add(fila);

                        ActualizarTotal();
                    }
                }

            }
            else
            {
                Productos producto = new Productos();
                DataGridViewRow filas = new DataGridViewRow();
                filas.CreateCells(ProductosdataGridView);
                filas.Cells[0].Value = ((Button)sender).Tag;

                if (producto.Buscar(Convert.ToUInt16(((Button)sender).Tag)))
                {
                    filas.Cells[1].Value = producto.Nombre;
                    filas.Cells[2].Value = producto.Precio.ToString();
                }

                filas.Cells[3].Value = 1;
                filas.Cells[4].Value = producto.Precio.ToString();

                ProductosdataGridView.RowHeadersVisible = false;
                ProductosdataGridView.Rows.Add(filas);

                ActualizarTotal();
            }
        }

        public void ActualizarTotal()
        {
            Total = 0;

            foreach (DataGridViewRow row in ProductosdataGridView.Rows)
            {
                Total += Convert.ToDouble(row.Cells[4].Value);
            }

            MontoTotallabel.Text = Total.ToString("N2");
        }

        public void ListarComidas()
        {
            Productos p = new Productos();
            DataTable dt = p.Listado(" p.ProductoId,p.Nombre,p.Imagen ", " p.ClaseProductoId != 3 and tp.TipoProductosId = 1", "");

            foreach (DataRow row in dt.Rows)
            {
                Button temp = new Button();

                //Propiedades
                temp.Height = 150;
                temp.Width = 130;
                temp.Name = row["Nombre"].ToString() + "Button";
                temp.Text = row["Nombre"].ToString();
                

                if (row["Imagen"].ToString() != "")
                {
                    temp.Image = new Bitmap(Image.FromFile(row["Imagen"].ToString()), 130, 115);//Image.FromFile(row["Imagen"].ToString());
                    temp.TextAlign = ContentAlignment.BottomCenter;
                }
                else
                {
                    temp.TextAlign = ContentAlignment.MiddleCenter;
                }

                temp.ImageAlign = ContentAlignment.TopCenter;
                temp.FlatStyle = FlatStyle.Flat;
                temp.BackColor = Color.LightGray;
                //Igualar el Tag del boton al Id del producto en la DB.
                temp.Tag = row["ProductoId"].ToString();

                //Asignamos el evento al boton generado
                temp.Click += new EventHandler(handlerComun_Click);

                //Agregamos el boton al formulario
                ComidasflowLayoutPanel.Controls.Add(temp);
            }
        }

        public void ListarEnsaladas()
        {
            Productos p = new Productos();
            DataTable dt = p.Listado(" p.ProductoId,p.Nombre,p.Imagen ", " p.ClaseProductoId != 3 and tp.TipoProductosId = 2 ", "");

            foreach (DataRow row in dt.Rows)
            {
                Button temp = new Button();

                //Propiedades
                temp.Height = 150;
                temp.Width = 130;
                temp.Name = row["Nombre"].ToString() + "Button";
                temp.Text = row["Nombre"].ToString();

                if (row["Imagen"].ToString() != "")
                {
                    temp.Image = new Bitmap(Image.FromFile(row["Imagen"].ToString()), 130, 115);//Image.FromFile(row["Imagen"].ToString());
                    temp.TextAlign = ContentAlignment.BottomCenter;
                }
                else
                {
                    temp.TextAlign = ContentAlignment.MiddleCenter;
                }

                temp.ImageAlign = ContentAlignment.TopCenter;
                temp.FlatStyle = FlatStyle.Flat;
                temp.BackColor = Color.LightGray;
                //Igualar el Tag del boton al Id del producto en la DB.
                temp.Tag = row["ProductoId"].ToString();

                //Asignamos el evento al boton generado
                temp.Click += new EventHandler(handlerComun_Click);

                //Agregamos el boton al formulario
                EnsaladasflowLayoutPanel.Controls.Add(temp);
            }
        }

        public void ListarBebidas()
        {
            Productos p = new Productos();
            DataTable dt = p.Listado(" p.ProductoId,p.Nombre,p.Imagen ", " p.ClaseProductoId != 3 and tp.TipoProductosId = 3 ", "");

            foreach (DataRow row in dt.Rows)
            {
                Button temp = new Button();

                //Propiedades
                temp.Height = 150;
                temp.Width = 130;
                temp.Name = row["Nombre"].ToString() + "Button";
                temp.Text = row["Nombre"].ToString();
                if (row["Imagen"].ToString() != "")
                {
                    temp.Image = new Bitmap(Image.FromFile(row["Imagen"].ToString()), 130, 115);//Image.FromFile(row["Imagen"].ToString());
                    temp.TextAlign = ContentAlignment.BottomCenter;
                }
                else
                {
                    temp.TextAlign = ContentAlignment.MiddleCenter;
                }

                temp.ImageAlign = ContentAlignment.TopCenter;
                temp.FlatStyle = FlatStyle.Flat;
                temp.BackColor = Color.LightGray;
                //Igualar el Tag del boton al Id del producto en la DB.
                temp.Tag = row["ProductoId"].ToString();

                //Asignamos el evento al boton generado
                temp.Click += new EventHandler(handlerComun_Click);

                //Agregamos el boton al formulario
                BebidasflowLayoutPanel.Controls.Add(temp);
            }
        }

        public void ListarHelados()
        {
            Productos p = new Productos();
            DataTable dt = p.Listado(" p.ProductoId,p.Nombre,p.Imagen ", " p.ClaseProductoId != 3 and tp.TipoProductosId = 4 ", "");

            foreach (DataRow row in dt.Rows)
            {
                Button temp = new Button();

                //Propiedades
                temp.Height = 150;
                temp.Width = 130;
                temp.Name = row["Nombre"].ToString() + "Button";
                temp.Text = row["Nombre"].ToString();
                if (row["Imagen"].ToString() != "")
                {
                    temp.Image = new Bitmap(Image.FromFile(row["Imagen"].ToString()), 130, 115);//Image.FromFile(row["Imagen"].ToString());
                    temp.TextAlign = ContentAlignment.BottomCenter;
                }
                else
                {
                    temp.TextAlign = ContentAlignment.MiddleCenter;
                }

                temp.ImageAlign = ContentAlignment.TopCenter;
                temp.FlatStyle = FlatStyle.Flat;
                temp.BackColor = Color.LightGray;
                //Igualar el Tag del boton al Id del producto en la DB.
                temp.Tag = row["ProductoId"].ToString();

                //Asignamos el evento al boton generado
                temp.Click += new EventHandler(handlerComun_Click);

                //Agregamos el boton al formulario
                HeladosflowLayoutPanel.Controls.Add(temp);
            }
        }

        public void ListarTragos()
        {
            Productos p = new Productos();
            DataTable dt = p.Listado(" p.ProductoId,p.Nombre,p.Imagen ", " p.ClaseProductoId != 3 and tp.TipoProductosId = 5 ", "");

            foreach (DataRow row in dt.Rows)
            {
                Button temp = new Button();

                //Propiedades
                temp.Height = 150;
                temp.Width = 130;
                temp.Name = row["Nombre"].ToString() + "Button";
                temp.Text = row["Nombre"].ToString();
                if (row["Imagen"].ToString() != "")
                {
                    temp.Image = new Bitmap(Image.FromFile(row["Imagen"].ToString()), 130, 115);//Image.FromFile(row["Imagen"].ToString());
                    temp.TextAlign = ContentAlignment.BottomCenter;
                }
                else
                {
                    temp.TextAlign = ContentAlignment.MiddleCenter;
                }

                temp.ImageAlign = ContentAlignment.TopCenter;
                temp.FlatStyle = FlatStyle.Flat;
                temp.BackColor = Color.LightGray;
                //Igualar el Tag del boton al Id del producto en la DB.
                temp.Tag = row["ProductoId"].ToString();

                //Asignamos el evento al boton generado
                temp.Click += new EventHandler(handlerComun_Click);

                //Agregamos el boton al formulario
                TragosflowLayoutPanel.Controls.Add(temp);
            }
        }

        public bool LlenarDatosVenta()
        {
            bool retorno = true;

            ventas.UsuarioId = LoginForm.UsuarioId;           
            ventas.MesaId = MesaId;
            ventas.MeseroId = Seguridad.ValidarIdEntero(MeserocomboBox.SelectedValue.ToString());
            ventas.TarjetaId = RegistroVentas.TarjetaId;
            if (NombreCliente.Length <= 0)
                ventas.NombreCliente = "Cliente Corriente";
            else
                ventas.NombreCliente = NombreCliente;
            ventas.Fecha = DateTime.Now.ToString();
            ventas.NCF = configuraciones.NCF;
            ventas.TipoVenta = TipoVenta;
            ventas.ValorRecibido = ValorRecibido;
            ventas.Cambio = Cambio;
            ventas.Comentario = Comentario;
            ventas.TotalVenta = RegistroVentas.montoFinal;
            

            ventas.LimpiarList();
            for (int i = 0; i < ProductosdataGridView.RowCount; i++)
            {
                ventas.AgregarProductos(Seguridad.ValidarIdEntero(ProductosdataGridView.Rows[i].Cells[0].Value.ToString()), Seguridad.ValidarIdEntero(ProductosdataGridView.Rows[i].Cells[3].Value.ToString()), Seguridad.ValidarDouble(ProductosdataGridView.Rows[i].Cells[4].Value.ToString()));
                //inventario.Buscar((int)VentasdataGridView.Rows[i].Cells[0].Value);
                //inventario.RestarCantidad((inventario.Cantidad - (int)VentasdataGridView.Rows[i].Cells[3].Value), (int)VentasdataGridView.Rows[i].Cells[0].Value);
            }
            return retorno;
        }

        public bool LlenarDatos()
        {
            bool retorno = true;

            preventa.UsuarioId = LoginForm.UsuarioId;
            preventa.Fecha = DateTime.Now.ToString();
            preventa.MesaId = MesaId;
            preventa.MeseroId = Seguridad.ValidarIdEntero(MeserocomboBox.SelectedValue.ToString());
            preventa.NombreCliente = "Cliente Corriente";
            preventa.Total = Seguridad.ValidarFloat(MontoTotallabel.Text);
            preventa.Nota = Nota;

            preventa.LimpiarList();
            for (int i = 0; i < ProductosdataGridView.RowCount; i++)
            {
                preventa.AgregarProductos(Seguridad.ValidarIdEntero(ProductosdataGridView.Rows[i].Cells[0].Value.ToString()), Seguridad.ValidarIdEntero(ProductosdataGridView.Rows[i].Cells[3].Value.ToString()), Seguridad.ValidarDouble((Seguridad.ValidarDouble(ProductosdataGridView.Rows[i].Cells[4].Value.ToString()) * 0.18).ToString()), Seguridad.ValidarDouble(ProductosdataGridView.Rows[i].Cells[4].Value.ToString()));
                //inventario.Buscar((int)VentasdataGridView.Rows[i].Cells[0].Value);
                //inventario.RestarCantidad((inventario.Cantidad - (int)VentasdataGridView.Rows[i].Cells[3].Value), (int)VentasdataGridView.Rows[i].Cells[0].Value);
            }
            return retorno;
        }

        private void buttonAtras_Click(object sender, EventArgs e)
        {
            if (ProductosdataGridView.RowCount > 0)
            {
                if (preventa.PreventaId <= 0)//lo que tenemos que hacer aqui es editar la preventa, en lugar de no evaluar los cambios nuevos
                {
                    if (LlenarDatos())
                    {
                        if (preventa.Insertar())
                        {
                            mesa.MesaId = MesaId;
                            if (mesa.Eliminar())
                            {
                                this.Close();
                                Nota = "";
                            }

                        }
                        else
                        {
                            MessageBox.Show("Error al Guardar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                else
                {
                    if (LlenarDatos())
                    {
                        if (preventa.Editar())
                        {
                            mesa.MesaId = MesaId;
                            Nota = "";
                            this.Close();

                        }
                        else
                        {
                            MessageBox.Show("Error al Modificar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
            else
            {
                if (preventa.Buscar(MesaId))
                {
                    mesa.MesaId = MesaId;
                    if (preventa.Eliminar() && mesa.Liberar())
                    {
                        this.Close();
                    }
                }
                else
                {
                    this.Close();
                }
               
            }
        }

        private void Notabutton_Click(object sender, EventArgs e)
        {
            //NotaForm.Nota = 
            NotaForm nf = new NotaForm();
            nf.ShowDialog();

            if (NotaForm.Check)
            {
                Notabutton.BackColor = Color.OrangeRed;
            }
        }

        private void Eliminarbutton_Click(object sender, EventArgs e)
        {
            try
            {
                if (ProductosdataGridView.CurrentRow.Index >= 0)
                {
                    ClaveAdm claveAdm = new ClaveAdm();
                    claveAdm.ShowDialog();
                    if (EliminarArticulo)
                    {
                        ProductosdataGridView.Rows.RemoveAt(ProductosdataGridView.CurrentRow.Index);
                        EliminarArticulo = false;
                    }
                    
                }
                ActualizarTotal();
            }
            catch (Exception)
            {

            }

        }

        private void Duplicarbutton_Click(object sender, EventArgs e)
        {
            try
            {
                if (ProductosdataGridView.CurrentRow.Index >= 0)
                {
                    ProductosdataGridView.CurrentRow.Cells[3].Value = (Convert.ToInt32(ProductosdataGridView.CurrentRow.Cells[3].Value) + 1).ToString();
                    ProductosdataGridView.CurrentRow.Cells[4].Value = (Convert.ToInt32(ProductosdataGridView.CurrentRow.Cells[3].Value) * Seguridad.ValidarDouble(ProductosdataGridView.CurrentRow.Cells[2].Value.ToString())).ToString();
                }
                ActualizarTotal();
            }
            catch (Exception)
            {

            }

        }

        private void Unirbutton_Click(object sender, EventArgs e)
        {
            UnirCuenta.MesaId = MesaId;
            UnirCuenta uc = new UnirCuenta();
            uc.ShowDialog();
            ProductosdataGridView.Rows.Clear();
            BuscarPreventa();
            ActualizarTotal();
        }

        private string NumeroFactura(int id)
        {
            string cadena = "", numero = id + "";
            if (numero.Length == 1)
            {
                cadena = "000" + numero;
            }
            else if (numero.Length == 2)
            {
                cadena = "00" + numero;
            }
            else if (numero.Length == 3)
            {
                cadena = "0" + numero;
            }
            else
            {
                cadena = numero;
            }
            return cadena;
        }

        private void Imprimir()
        {
            string condicion = "";
            ticket.TextoCentro("TAIRIL");
            ticket.TextoCentro("Bar and Lounge");
            ticket.TextoCentro("C/Principal, Bomba de Cenovi R.D");
            ticket.TextoCentro("(829) 423-6196");
            ticket.LineasIgual();
            ticket.TextoCentro("FACTURA");
            ticket.LineasIgual();
            ticket.TextoIzquierda("RNC: "+ configuraciones.RNC);
            ticket.TextoIzquierda("Factura NO: " + NumeroFactura(ventas.MaximoId()));
            ticket.TextoIzquierda("Fecha: " + DateTime.Now.ToString("dd/M/yyyy"));
            ticket.TextoIzquierda("Hora: " + DateTime.Now.ToString("hh:mm:ss"));
            ticket.TextoIzquierda("NCF: "+ configuraciones.NCF);

            if (TipoVenta == 1)
                condicion = "Contado";
            else if (TipoVenta == 0)
                condicion = "Tarjeta";
            else if (TipoVenta == 2)
            {
                condicion = "Cortesia";
                MontoTotallabel.Text = "0";
            }

            ticket.TextoIzquierda("Condicion: " + condicion);
            if (MesaId < 25)
            {
                ticket.TextoIzquierda("Mesa: " + MesaId);
            }
            if(ventas.NombreCliente != "Cliente Corriente")
                ticket.TextoIzquierda("Cliente: "+ventas.NombreCliente);
            ticket.LineasBaja();
            ticket.EncabezadoVenta();//nombre del articulo, canti, precio
            ticket.LineasGuion();
            for (int i = 0; i < ProductosdataGridView.RowCount; i++)
            {
                ticket.AgregarProducto(ProductosdataGridView.Rows[i].Cells[1].Value.ToString(), Seguridad.ValidarIdEntero(ProductosdataGridView.Rows[i].Cells[3].Value.ToString()), Seguridad.ValidarIdEntero(ProductosdataGridView.Rows[i].Cells[2].Value.ToString()));
            }
            ticket.LineasIgual();
            ticket.AgregarTotales("Subtotal", Seguridad.ValidarDouble(MontoTotallabel.Text) - (Seguridad.ValidarDouble(MontoTotallabel.Text) * 0.18));
            ticket.AgregarTotales("Total ITBIS", (Seguridad.ValidarDouble(MontoTotallabel.Text) * 0.18));
            ticket.AgregarTotales("Descuento", (Seguridad.ValidarDouble(MontoTotallabel.Text) - RegistroVentas.montoFinal));
            ticket.TextoDerecha("------------");
            ticket.AgregarTotales("Total General", RegistroVentas.montoFinal);//para agregar los totales efectivo, cambio, etc..
            ticket.LineasIgual();
            ticket.TextoCentro("");
            ticket.TextoCentro("");
            ticket.TextoIzquierda("______________             _____________");
            ticket.TextoIzquierda("Despachado por             Recibido Por");
            ticket.TextoCentro("Gracias por Preferirnos");
            ticket.TextoCentro("Le Atendio: " + usuarios.Nombre);
            //MessageBox.Show(ticket.GetLine().ToString());
            ticket.TextoCentro("");
            ticket.TextoCentro("");
            ticket.TextoCentro("");
            ticket.TextoCentro("");
            //ticket.CortaTicket();
            ticket.ImprimirTicket("Microsoft XPS Document Writer");
            //nombre impresora-Microsoft XPS Document Writer-EPSON-Microsoft Print to PDF
            //Limpiar();
        }

        private void ImprimirPreventa()
        {
            ticket.TextoCentro("TAIRIL");
            ticket.TextoCentro("Bar and Lounge");
            ticket.TextoCentro("C/Principal, Bomba de Cenovi R.D");
            ticket.TextoCentro("(829) 423-6196");
            ticket.LineasIgual();
            ticket.TextoCentro("FACTURA");
            ticket.LineasIgual();
            ticket.TextoIzquierda("RNC: " + configuraciones.RNC);
            ticket.TextoIzquierda("Factura NO: " + NumeroFactura(ventas.MaximoId()));
            ticket.TextoIzquierda("Fecha: " + DateTime.Now.ToString("dd/M/yyyy"));
            ticket.TextoIzquierda("Hora: " + DateTime.Now.ToString("hh:mm:ss"));
            ticket.TextoIzquierda("NCF: " + configuraciones.NCF);
            if (MesaId < 25)
            {
                ticket.TextoIzquierda("Mesa: " + MesaId);
            }

            ticket.LineasBaja();
            ticket.EncabezadoVenta();//nombre del articulo, canti, precio
            ticket.LineasGuion();
            for (int i = 0; i < ProductosdataGridView.RowCount; i++)
            {
                ticket.AgregarProducto(ProductosdataGridView.Rows[i].Cells[1].Value.ToString(), Seguridad.ValidarIdEntero(ProductosdataGridView.Rows[i].Cells[3].Value.ToString()), Seguridad.ValidarIdEntero(ProductosdataGridView.Rows[i].Cells[2].Value.ToString()));
            }
            ticket.LineasIgual();
            ticket.AgregarTotales("Subtotal", Seguridad.ValidarDouble(MontoTotallabel.Text) - (Seguridad.ValidarDouble(MontoTotallabel.Text) * 0.18));
            ticket.AgregarTotales("Total ITBIS", (Seguridad.ValidarDouble(MontoTotallabel.Text) * 0.18));
            //ticket.AgregarTotales("Descuento", (Seguridad.ValidarDouble(MontoTotallabel.Text) - RegistroVentas.montoFinal));
            ticket.TextoDerecha("------------");
            ticket.AgregarTotales("Total General", Seguridad.ValidarDouble(MontoTotallabel.Text));//para agregar los totales efectivo, cambio, etc..
            //ticket.LineasIgual();
            //ticket.TextoCentro("");
            //ticket.TextoCentro("");
            //ticket.TextoIzquierda("______________             _____________");
            //ticket.TextoIzquierda("Despachado por             Recibido Por");
            //ticket.TextoCentro("Gracias por Preferirnos");
            //ticket.TextoCentro("Le Atendio: " + usuarios.Nombre);
            //MessageBox.Show(ticket.GetLine().ToString());
            ticket.TextoCentro("");
            ticket.TextoCentro("");
            ticket.TextoCentro("");
            ticket.TextoCentro("");
            //ticket.CortaTicket();
            ticket.ImprimirTicket("Microsoft XPS Document Writer");
            //nombre impresora-Microsoft XPS Document Writer-EPSON-Microsoft Print to PDF
            //Limpiar();
        }


        private void button6_Click(object sender, EventArgs e)
        {
            //boton imprimir preventa   
            ImprimirPreventa();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            //Boton de finalizar venta
            mesa.MesaId = MesaId;
            RegistroVentas.monto = Seguridad.ValidarDouble(MontoTotallabel.Text);
            RegistroVentas rVenta = new RegistroVentas();
            rVenta.ShowDialog();
            if (RegistroVentas.Sigo && LlenarDatosVenta())
            {
                if (preventa.Eliminar() && mesa.Liberar())
                {
                    if (ventas.Insertar())
                    {
                        RestarInventario();
                        DialogResult result;
                        result = MessageBox.Show("Quieres Imprimir?", "Confirmar" ,MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (result == DialogResult.Yes)
                        {
                            Imprimir();
                        }
                        this.Close();
                        RegistroVentas.montoFinal = 0;
                    }
                    else
                    {
                        MessageBox.Show("Error al Guardar la Venta");
                    }
                    
                }
            }
            
        }

        private void Dividirbutton_Click_1(object sender, EventArgs e)
        {
            if (ProductosdataGridView.RowCount > 0)
            {
                if (preventa.PreventaId <= 0)//lo que tenemos que hacer aqui es editar la preventa, en lugar de no evaluar los cambios nuevos
                {
                    if (LlenarDatos())
                    {
                        if (preventa.Insertar())
                        {
                            mesa.MesaId = MesaId;
                            Nota = "";
                        }
                        else
                        {
                            MessageBox.Show("Error al Guardar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                else
                {
                    if (LlenarDatos())
                    {
                        if (preventa.Editar())
                        {
                            mesa.MesaId = MesaId;
                            Nota = "";

                        }
                        else
                        {
                            MessageBox.Show("Error al Modificar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }

            DividirCuenta.MesaId = MesaId;
            DividirCuenta dc = new DividirCuenta();
            dc.ShowDialog();

            ProductosdataGridView.Rows.Clear();
            ProductosdataGridView.Refresh();

            BuscarPreventa();
        }

        public void RestarInventario()
        {
            Inventarios inv = new Inventarios();

            foreach(DataGridViewRow row in ProductosdataGridView.Rows)
            {
                if (productos.Buscar(Convert.ToInt32(row.Cells[0].Value)))
                {
                    if(productos.ClaseProductoId == 1)
                    {
                        if (inv.Buscar(Convert.ToInt32(row.Cells[0].Value)))
                        {
                            inv.RestarCantidad(inv.Cantidad - Convert.ToInt32(row.Cells[3].Value), Convert.ToInt32(row.Cells[0].Value));
                        }
                        
                    }
                    else if(productos.ClaseProductoId == 2)
                    {
                        DataTable dtIng;
                        dtIng = productos.BuscarIngredientes("ProductoSecundarioId, Cantidad", " ProductoId = " + Convert.ToInt32(row.Cells[0].Value));
                        
                        foreach(DataRow fila in dtIng.Rows)
                        {
                            if (inv.Buscar(Convert.ToInt32(fila["ProductoSecundarioId"].ToString())))
                            {
                                for (int i = 0; i < Seguridad.ValidarIdEntero(row.Cells[3].Value.ToString()); i++)
                                {
                                    inv.Buscar(Convert.ToInt32(fila["ProductoSecundarioId"].ToString()));
                                    inv.RestarCantidad(inv.Cantidad - Convert.ToInt32(fila["Cantidad"]), Convert.ToInt32(fila["ProductoSecundarioId"]));
                                }
                                
                            }
                        }
                    }
                }
            }
        }
    }
}
