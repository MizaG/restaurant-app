﻿namespace CodeRestaurant
{
    partial class BackupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BackupForm));
            this.Backupbutton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Backupbutton
            // 
            this.Backupbutton.Image = ((System.Drawing.Image)(resources.GetObject("Backupbutton.Image")));
            this.Backupbutton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Backupbutton.Location = new System.Drawing.Point(80, 69);
            this.Backupbutton.Name = "Backupbutton";
            this.Backupbutton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Backupbutton.Size = new System.Drawing.Size(107, 50);
            this.Backupbutton.TabIndex = 0;
            this.Backupbutton.Text = "Backup";
            this.Backupbutton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Backupbutton.UseVisualStyleBackColor = true;
            this.Backupbutton.Click += new System.EventHandler(this.Backupbutton_Click);
            // 
            // BackupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(275, 211);
            this.Controls.Add(this.Backupbutton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "BackupForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Backup";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Backupbutton;
    }
}