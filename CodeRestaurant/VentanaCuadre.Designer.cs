﻿namespace CodeRestaurant
{
    partial class VentanaCuadre
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TotalDetallelabel = new System.Windows.Forms.Label();
            this.VentasEfectivolabel = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.VentasTarjetaslabel = new System.Windows.Forms.Label();
            this.BalanceIniciallabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.TotalCuadrelabel = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.SubT2000textBox = new System.Windows.Forms.TextBox();
            this.SubT1000textBox = new System.Windows.Forms.TextBox();
            this.SubT500textBox = new System.Windows.Forms.TextBox();
            this.SubT200textBox = new System.Windows.Forms.TextBox();
            this.SubT100textBox = new System.Windows.Forms.TextBox();
            this.SubT50textBox = new System.Windows.Forms.TextBox();
            this.SubT25textBox = new System.Windows.Forms.TextBox();
            this.SubT10textBox = new System.Windows.Forms.TextBox();
            this.SubT5textBox = new System.Windows.Forms.TextBox();
            this.SubT1textBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Cant2000textBox = new System.Windows.Forms.TextBox();
            this.Cant1000textBox = new System.Windows.Forms.TextBox();
            this.Cant500textBox = new System.Windows.Forms.TextBox();
            this.Cant200textBox = new System.Windows.Forms.TextBox();
            this.Cant100textBox = new System.Windows.Forms.TextBox();
            this.Cant50textBox = new System.Windows.Forms.TextBox();
            this.Cant25textBox = new System.Windows.Forms.TextBox();
            this.Cant10textBox = new System.Windows.Forms.TextBox();
            this.Cant5textBox = new System.Windows.Forms.TextBox();
            this.Cant1textBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Cancelarbutton = new System.Windows.Forms.Button();
            this.CerrarCajabutton = new System.Windows.Forms.Button();
            this.Totaltimer = new System.Windows.Forms.Timer(this.components);
            this.Diferencialabel = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TotalDetallelabel);
            this.groupBox1.Controls.Add(this.VentasEfectivolabel);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.VentasTarjetaslabel);
            this.groupBox1.Controls.Add(this.BalanceIniciallabel);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(12, 40);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(521, 595);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // TotalDetallelabel
            // 
            this.TotalDetallelabel.AutoSize = true;
            this.TotalDetallelabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TotalDetallelabel.ForeColor = System.Drawing.Color.Red;
            this.TotalDetallelabel.Location = new System.Drawing.Point(295, 339);
            this.TotalDetallelabel.Name = "TotalDetallelabel";
            this.TotalDetallelabel.Size = new System.Drawing.Size(144, 31);
            this.TotalDetallelabel.TabIndex = 26;
            this.TotalDetallelabel.Text = "RD$ 00.00";
            // 
            // VentasEfectivolabel
            // 
            this.VentasEfectivolabel.AutoSize = true;
            this.VentasEfectivolabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VentasEfectivolabel.Location = new System.Drawing.Point(295, 238);
            this.VentasEfectivolabel.Name = "VentasEfectivolabel";
            this.VentasEfectivolabel.Size = new System.Drawing.Size(114, 25);
            this.VentasEfectivolabel.TabIndex = 25;
            this.VentasEfectivolabel.Text = "RD$ 00.00";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(189, 16);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(80, 29);
            this.label22.TabIndex = 19;
            this.label22.Text = "Cierre";
            // 
            // VentasTarjetaslabel
            // 
            this.VentasTarjetaslabel.AutoSize = true;
            this.VentasTarjetaslabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VentasTarjetaslabel.Location = new System.Drawing.Point(295, 190);
            this.VentasTarjetaslabel.Name = "VentasTarjetaslabel";
            this.VentasTarjetaslabel.Size = new System.Drawing.Size(114, 25);
            this.VentasTarjetaslabel.TabIndex = 24;
            this.VentasTarjetaslabel.Text = "RD$ 00.00";
            // 
            // BalanceIniciallabel
            // 
            this.BalanceIniciallabel.AutoSize = true;
            this.BalanceIniciallabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BalanceIniciallabel.Location = new System.Drawing.Point(295, 142);
            this.BalanceIniciallabel.Name = "BalanceIniciallabel";
            this.BalanceIniciallabel.Size = new System.Drawing.Size(114, 25);
            this.BalanceIniciallabel.TabIndex = 23;
            this.BalanceIniciallabel.Text = "RD$ 00.00";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 339);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 31);
            this.label5.TabIndex = 22;
            this.label5.Text = "Total:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.DarkGreen;
            this.label3.Location = new System.Drawing.Point(6, 238);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(222, 25);
            this.label3.TabIndex = 21;
            this.label3.Text = "Total Ventas Efectivo:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label2.Location = new System.Drawing.Point(6, 190);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(223, 25);
            this.label2.TabIndex = 20;
            this.label2.Text = "Total Ventas Tarjetas:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.OrangeRed;
            this.label4.Location = new System.Drawing.Point(6, 142);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(157, 25);
            this.label4.TabIndex = 19;
            this.label4.Text = "Balance Inicial:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(450, 8);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(168, 29);
            this.label8.TabIndex = 18;
            this.label8.Text = "Detalle Ventas";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.TotalCuadrelabel);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.SubT2000textBox);
            this.groupBox2.Controls.Add(this.SubT1000textBox);
            this.groupBox2.Controls.Add(this.SubT500textBox);
            this.groupBox2.Controls.Add(this.SubT200textBox);
            this.groupBox2.Controls.Add(this.SubT100textBox);
            this.groupBox2.Controls.Add(this.SubT50textBox);
            this.groupBox2.Controls.Add(this.SubT25textBox);
            this.groupBox2.Controls.Add(this.SubT10textBox);
            this.groupBox2.Controls.Add(this.SubT5textBox);
            this.groupBox2.Controls.Add(this.SubT1textBox);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.Cant2000textBox);
            this.groupBox2.Controls.Add(this.Cant1000textBox);
            this.groupBox2.Controls.Add(this.Cant500textBox);
            this.groupBox2.Controls.Add(this.Cant200textBox);
            this.groupBox2.Controls.Add(this.Cant100textBox);
            this.groupBox2.Controls.Add(this.Cant50textBox);
            this.groupBox2.Controls.Add(this.Cant25textBox);
            this.groupBox2.Controls.Add(this.Cant10textBox);
            this.groupBox2.Controls.Add(this.Cant5textBox);
            this.groupBox2.Controls.Add(this.Cant1textBox);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(539, 40);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(521, 595);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // TotalCuadrelabel
            // 
            this.TotalCuadrelabel.AutoSize = true;
            this.TotalCuadrelabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TotalCuadrelabel.ForeColor = System.Drawing.Color.Red;
            this.TotalCuadrelabel.Location = new System.Drawing.Point(253, 561);
            this.TotalCuadrelabel.Name = "TotalCuadrelabel";
            this.TotalCuadrelabel.Size = new System.Drawing.Size(144, 31);
            this.TotalCuadrelabel.TabIndex = 53;
            this.TotalCuadrelabel.Text = "RD$ 00.00";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(176, 561);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(83, 31);
            this.label21.TabIndex = 52;
            this.label21.Text = "Total:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(69, 498);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(60, 25);
            this.label19.TabIndex = 51;
            this.label19.Text = "2000";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(69, 456);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(60, 25);
            this.label18.TabIndex = 50;
            this.label18.Text = "1000";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(73, 414);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(48, 25);
            this.label17.TabIndex = 49;
            this.label17.Text = "500";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(73, 372);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(48, 25);
            this.label16.TabIndex = 48;
            this.label16.Text = "200";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(73, 330);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(48, 25);
            this.label15.TabIndex = 47;
            this.label15.Text = "100";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(76, 288);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(36, 25);
            this.label14.TabIndex = 46;
            this.label14.Text = "50";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(75, 246);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(36, 25);
            this.label13.TabIndex = 45;
            this.label13.Text = "25";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(73, 204);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(36, 25);
            this.label12.TabIndex = 44;
            this.label12.Text = "10";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(77, 162);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(24, 25);
            this.label11.TabIndex = 43;
            this.label11.Text = "5";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(77, 120);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(24, 25);
            this.label10.TabIndex = 42;
            this.label10.Text = "1";
            // 
            // SubT2000textBox
            // 
            this.SubT2000textBox.Enabled = false;
            this.SubT2000textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubT2000textBox.Location = new System.Drawing.Point(361, 495);
            this.SubT2000textBox.Name = "SubT2000textBox";
            this.SubT2000textBox.Size = new System.Drawing.Size(108, 31);
            this.SubT2000textBox.TabIndex = 41;
            // 
            // SubT1000textBox
            // 
            this.SubT1000textBox.Enabled = false;
            this.SubT1000textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubT1000textBox.Location = new System.Drawing.Point(361, 453);
            this.SubT1000textBox.Name = "SubT1000textBox";
            this.SubT1000textBox.Size = new System.Drawing.Size(108, 31);
            this.SubT1000textBox.TabIndex = 40;
            // 
            // SubT500textBox
            // 
            this.SubT500textBox.Enabled = false;
            this.SubT500textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubT500textBox.Location = new System.Drawing.Point(361, 411);
            this.SubT500textBox.Name = "SubT500textBox";
            this.SubT500textBox.Size = new System.Drawing.Size(108, 31);
            this.SubT500textBox.TabIndex = 39;
            // 
            // SubT200textBox
            // 
            this.SubT200textBox.Enabled = false;
            this.SubT200textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubT200textBox.Location = new System.Drawing.Point(361, 369);
            this.SubT200textBox.Name = "SubT200textBox";
            this.SubT200textBox.Size = new System.Drawing.Size(108, 31);
            this.SubT200textBox.TabIndex = 38;
            // 
            // SubT100textBox
            // 
            this.SubT100textBox.Enabled = false;
            this.SubT100textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubT100textBox.Location = new System.Drawing.Point(361, 327);
            this.SubT100textBox.Name = "SubT100textBox";
            this.SubT100textBox.Size = new System.Drawing.Size(108, 31);
            this.SubT100textBox.TabIndex = 37;
            // 
            // SubT50textBox
            // 
            this.SubT50textBox.Enabled = false;
            this.SubT50textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubT50textBox.Location = new System.Drawing.Point(361, 285);
            this.SubT50textBox.Name = "SubT50textBox";
            this.SubT50textBox.Size = new System.Drawing.Size(108, 31);
            this.SubT50textBox.TabIndex = 36;
            // 
            // SubT25textBox
            // 
            this.SubT25textBox.Enabled = false;
            this.SubT25textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubT25textBox.Location = new System.Drawing.Point(361, 243);
            this.SubT25textBox.Name = "SubT25textBox";
            this.SubT25textBox.Size = new System.Drawing.Size(108, 31);
            this.SubT25textBox.TabIndex = 35;
            // 
            // SubT10textBox
            // 
            this.SubT10textBox.Enabled = false;
            this.SubT10textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubT10textBox.Location = new System.Drawing.Point(361, 201);
            this.SubT10textBox.Name = "SubT10textBox";
            this.SubT10textBox.Size = new System.Drawing.Size(108, 31);
            this.SubT10textBox.TabIndex = 34;
            // 
            // SubT5textBox
            // 
            this.SubT5textBox.Enabled = false;
            this.SubT5textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubT5textBox.Location = new System.Drawing.Point(361, 159);
            this.SubT5textBox.Name = "SubT5textBox";
            this.SubT5textBox.Size = new System.Drawing.Size(108, 31);
            this.SubT5textBox.TabIndex = 33;
            // 
            // SubT1textBox
            // 
            this.SubT1textBox.Enabled = false;
            this.SubT1textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubT1textBox.Location = new System.Drawing.Point(361, 117);
            this.SubT1textBox.Name = "SubT1textBox";
            this.SubT1textBox.Size = new System.Drawing.Size(108, 31);
            this.SubT1textBox.TabIndex = 32;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(361, 73);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(98, 25);
            this.label9.TabIndex = 31;
            this.label9.Text = "SubTotal";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(186, 73);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(98, 25);
            this.label7.TabIndex = 30;
            this.label7.Text = "Cantidad";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 73);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(155, 25);
            this.label6.TabIndex = 29;
            this.label6.Text = "Moneda/Billete";
            // 
            // Cant2000textBox
            // 
            this.Cant2000textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cant2000textBox.Location = new System.Drawing.Point(195, 495);
            this.Cant2000textBox.Name = "Cant2000textBox";
            this.Cant2000textBox.Size = new System.Drawing.Size(80, 31);
            this.Cant2000textBox.TabIndex = 28;
            this.Cant2000textBox.TextChanged += new System.EventHandler(this.Cant2000textBox_TextChanged);
            // 
            // Cant1000textBox
            // 
            this.Cant1000textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cant1000textBox.Location = new System.Drawing.Point(195, 453);
            this.Cant1000textBox.Name = "Cant1000textBox";
            this.Cant1000textBox.Size = new System.Drawing.Size(80, 31);
            this.Cant1000textBox.TabIndex = 27;
            this.Cant1000textBox.TextChanged += new System.EventHandler(this.Cant1000textBox_TextChanged);
            // 
            // Cant500textBox
            // 
            this.Cant500textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cant500textBox.Location = new System.Drawing.Point(195, 411);
            this.Cant500textBox.Name = "Cant500textBox";
            this.Cant500textBox.Size = new System.Drawing.Size(80, 31);
            this.Cant500textBox.TabIndex = 26;
            this.Cant500textBox.TextChanged += new System.EventHandler(this.Cant500textBox_TextChanged);
            // 
            // Cant200textBox
            // 
            this.Cant200textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cant200textBox.Location = new System.Drawing.Point(195, 369);
            this.Cant200textBox.Name = "Cant200textBox";
            this.Cant200textBox.Size = new System.Drawing.Size(80, 31);
            this.Cant200textBox.TabIndex = 25;
            this.Cant200textBox.TextChanged += new System.EventHandler(this.Cant200textBox_TextChanged);
            // 
            // Cant100textBox
            // 
            this.Cant100textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cant100textBox.Location = new System.Drawing.Point(195, 327);
            this.Cant100textBox.Name = "Cant100textBox";
            this.Cant100textBox.Size = new System.Drawing.Size(80, 31);
            this.Cant100textBox.TabIndex = 24;
            this.Cant100textBox.TextChanged += new System.EventHandler(this.Cant100textBox_TextChanged);
            // 
            // Cant50textBox
            // 
            this.Cant50textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cant50textBox.Location = new System.Drawing.Point(195, 285);
            this.Cant50textBox.Name = "Cant50textBox";
            this.Cant50textBox.Size = new System.Drawing.Size(80, 31);
            this.Cant50textBox.TabIndex = 23;
            this.Cant50textBox.TextChanged += new System.EventHandler(this.Cant50textBox_TextChanged);
            // 
            // Cant25textBox
            // 
            this.Cant25textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cant25textBox.Location = new System.Drawing.Point(195, 243);
            this.Cant25textBox.Name = "Cant25textBox";
            this.Cant25textBox.Size = new System.Drawing.Size(80, 31);
            this.Cant25textBox.TabIndex = 22;
            this.Cant25textBox.TextChanged += new System.EventHandler(this.Cant25textBox_TextChanged);
            // 
            // Cant10textBox
            // 
            this.Cant10textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cant10textBox.Location = new System.Drawing.Point(195, 201);
            this.Cant10textBox.Name = "Cant10textBox";
            this.Cant10textBox.Size = new System.Drawing.Size(80, 31);
            this.Cant10textBox.TabIndex = 21;
            this.Cant10textBox.TextChanged += new System.EventHandler(this.Cant10textBox_TextChanged);
            // 
            // Cant5textBox
            // 
            this.Cant5textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cant5textBox.Location = new System.Drawing.Point(195, 159);
            this.Cant5textBox.Name = "Cant5textBox";
            this.Cant5textBox.Size = new System.Drawing.Size(80, 31);
            this.Cant5textBox.TabIndex = 20;
            this.Cant5textBox.TextChanged += new System.EventHandler(this.Cant5textBox_TextChanged);
            // 
            // Cant1textBox
            // 
            this.Cant1textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cant1textBox.Location = new System.Drawing.Point(195, 117);
            this.Cant1textBox.Name = "Cant1textBox";
            this.Cant1textBox.Size = new System.Drawing.Size(80, 31);
            this.Cant1textBox.TabIndex = 19;
            this.Cant1textBox.TextChanged += new System.EventHandler(this.Cant1textBox_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(207, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 29);
            this.label1.TabIndex = 18;
            this.label1.Text = "Cuadre";
            // 
            // Cancelarbutton
            // 
            this.Cancelarbutton.BackColor = System.Drawing.SystemColors.Control;
            this.Cancelarbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cancelarbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cancelarbutton.Location = new System.Drawing.Point(913, 641);
            this.Cancelarbutton.Name = "Cancelarbutton";
            this.Cancelarbutton.Size = new System.Drawing.Size(147, 50);
            this.Cancelarbutton.TabIndex = 18;
            this.Cancelarbutton.Text = "Cancelar";
            this.Cancelarbutton.UseVisualStyleBackColor = false;
            this.Cancelarbutton.Click += new System.EventHandler(this.Cancelarbutton_Click);
            // 
            // CerrarCajabutton
            // 
            this.CerrarCajabutton.BackColor = System.Drawing.Color.LimeGreen;
            this.CerrarCajabutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CerrarCajabutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CerrarCajabutton.ForeColor = System.Drawing.Color.White;
            this.CerrarCajabutton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.CerrarCajabutton.Location = new System.Drawing.Point(760, 641);
            this.CerrarCajabutton.Name = "CerrarCajabutton";
            this.CerrarCajabutton.Size = new System.Drawing.Size(147, 50);
            this.CerrarCajabutton.TabIndex = 17;
            this.CerrarCajabutton.Text = "Cerrar Caja";
            this.CerrarCajabutton.UseVisualStyleBackColor = false;
            this.CerrarCajabutton.Click += new System.EventHandler(this.CerrarCajabutton_Click);
            // 
            // Totaltimer
            // 
            this.Totaltimer.Interval = 500;
            this.Totaltimer.Tick += new System.EventHandler(this.Totaltimer_Tick);
            // 
            // Diferencialabel
            // 
            this.Diferencialabel.AutoSize = true;
            this.Diferencialabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Diferencialabel.ForeColor = System.Drawing.Color.Blue;
            this.Diferencialabel.Location = new System.Drawing.Point(516, 648);
            this.Diferencialabel.Name = "Diferencialabel";
            this.Diferencialabel.Size = new System.Drawing.Size(144, 31);
            this.Diferencialabel.TabIndex = 55;
            this.Diferencialabel.Text = "RD$ 00.00";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(388, 648);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(145, 31);
            this.label23.TabIndex = 54;
            this.label23.Text = "Diferencia:";
            // 
            // VentanaCuadre
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1071, 701);
            this.Controls.Add(this.Diferencialabel);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.Cancelarbutton);
            this.Controls.Add(this.CerrarCajabutton);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label8);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "VentanaCuadre";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VentanaCuadre";
            this.Load += new System.EventHandler(this.VentanaCuadre_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label TotalDetallelabel;
        private System.Windows.Forms.Label VentasEfectivolabel;
        private System.Windows.Forms.Label VentasTarjetaslabel;
        private System.Windows.Forms.Label BalanceIniciallabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label TotalCuadrelabel;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox SubT2000textBox;
        private System.Windows.Forms.TextBox SubT1000textBox;
        private System.Windows.Forms.TextBox SubT500textBox;
        private System.Windows.Forms.TextBox SubT200textBox;
        private System.Windows.Forms.TextBox SubT100textBox;
        private System.Windows.Forms.TextBox SubT50textBox;
        private System.Windows.Forms.TextBox SubT25textBox;
        private System.Windows.Forms.TextBox SubT10textBox;
        private System.Windows.Forms.TextBox SubT5textBox;
        private System.Windows.Forms.TextBox SubT1textBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox Cant2000textBox;
        private System.Windows.Forms.TextBox Cant1000textBox;
        private System.Windows.Forms.TextBox Cant500textBox;
        private System.Windows.Forms.TextBox Cant200textBox;
        private System.Windows.Forms.TextBox Cant100textBox;
        private System.Windows.Forms.TextBox Cant50textBox;
        private System.Windows.Forms.TextBox Cant25textBox;
        private System.Windows.Forms.TextBox Cant10textBox;
        private System.Windows.Forms.TextBox Cant5textBox;
        private System.Windows.Forms.TextBox Cant1textBox;
        private System.Windows.Forms.Button Cancelarbutton;
        private System.Windows.Forms.Button CerrarCajabutton;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Timer Totaltimer;
        private System.Windows.Forms.Label Diferencialabel;
        private System.Windows.Forms.Label label23;
    }
}