﻿namespace CodeRestaurant
{
    partial class RegistroProductos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegistroProductos));
            this.label1 = new System.Windows.Forms.Label();
            this.ImagenpictureBox = new System.Windows.Forms.PictureBox();
            this.TipoProductocomboBox = new System.Windows.Forms.ComboBox();
            this.Guardarbutton = new System.Windows.Forms.Button();
            this.ActivocheckBox = new System.Windows.Forms.CheckBox();
            this.NombretextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ProveedorcomboBox = new System.Windows.Forms.ComboBox();
            this.CategoriacomboBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.PreciotextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.CostotextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.Cancelarbutton = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.RegistrarProveedorbutton = new System.Windows.Forms.Button();
            this.ImagenopenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.label9 = new System.Windows.Forms.Label();
            this.IngredientesdataGridView = new System.Windows.Forms.DataGridView();
            this.IngredienteId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IngredientescomboBox = new System.Windows.Forms.ComboBox();
            this.Agregarbutton = new System.Windows.Forms.Button();
            this.CantidadtextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.IngredientesgroupBox = new System.Windows.Forms.GroupBox();
            this.ClaseProductocomboBox = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.UnidadMedidacomboBox = new System.Windows.Forms.ComboBox();
            this.Medidalabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ImagenpictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IngredientesdataGridView)).BeginInit();
            this.IngredientesgroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(549, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(176, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tipo de Producto";
            // 
            // ImagenpictureBox
            // 
            this.ImagenpictureBox.Image = ((System.Drawing.Image)(resources.GetObject("ImagenpictureBox.Image")));
            this.ImagenpictureBox.Location = new System.Drawing.Point(33, 86);
            this.ImagenpictureBox.Name = "ImagenpictureBox";
            this.ImagenpictureBox.Size = new System.Drawing.Size(137, 120);
            this.ImagenpictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ImagenpictureBox.TabIndex = 1;
            this.ImagenpictureBox.TabStop = false;
            this.ImagenpictureBox.Click += new System.EventHandler(this.ImagenpictureBox_Click);
            // 
            // TipoProductocomboBox
            // 
            this.TipoProductocomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TipoProductocomboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TipoProductocomboBox.FormattingEnabled = true;
            this.TipoProductocomboBox.Location = new System.Drawing.Point(553, 106);
            this.TipoProductocomboBox.Name = "TipoProductocomboBox";
            this.TipoProductocomboBox.Size = new System.Drawing.Size(295, 33);
            this.TipoProductocomboBox.TabIndex = 2;
            // 
            // Guardarbutton
            // 
            this.Guardarbutton.BackColor = System.Drawing.Color.Gray;
            this.Guardarbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Guardarbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Guardarbutton.ForeColor = System.Drawing.Color.White;
            this.Guardarbutton.Image = ((System.Drawing.Image)(resources.GetObject("Guardarbutton.Image")));
            this.Guardarbutton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Guardarbutton.Location = new System.Drawing.Point(587, 419);
            this.Guardarbutton.Name = "Guardarbutton";
            this.Guardarbutton.Size = new System.Drawing.Size(147, 50);
            this.Guardarbutton.TabIndex = 3;
            this.Guardarbutton.Text = "Guardar";
            this.Guardarbutton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Guardarbutton.UseVisualStyleBackColor = false;
            this.Guardarbutton.Click += new System.EventHandler(this.Guardarbutton_Click);
            // 
            // ActivocheckBox
            // 
            this.ActivocheckBox.AutoSize = true;
            this.ActivocheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ActivocheckBox.Location = new System.Drawing.Point(341, 248);
            this.ActivocheckBox.Name = "ActivocheckBox";
            this.ActivocheckBox.Size = new System.Drawing.Size(90, 29);
            this.ActivocheckBox.TabIndex = 4;
            this.ActivocheckBox.Text = "Activo";
            this.ActivocheckBox.UseVisualStyleBackColor = true;
            // 
            // NombretextBox
            // 
            this.NombretextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NombretextBox.Location = new System.Drawing.Point(193, 107);
            this.NombretextBox.Name = "NombretextBox";
            this.NombretextBox.Size = new System.Drawing.Size(295, 31);
            this.NombretextBox.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(549, 209);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 25);
            this.label2.TabIndex = 6;
            this.label2.Text = "Proveedor";
            // 
            // ProveedorcomboBox
            // 
            this.ProveedorcomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ProveedorcomboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProveedorcomboBox.FormattingEnabled = true;
            this.ProveedorcomboBox.Location = new System.Drawing.Point(553, 237);
            this.ProveedorcomboBox.Name = "ProveedorcomboBox";
            this.ProveedorcomboBox.Size = new System.Drawing.Size(295, 33);
            this.ProveedorcomboBox.TabIndex = 7;
            // 
            // CategoriacomboBox
            // 
            this.CategoriacomboBox.DisplayMember = "1";
            this.CategoriacomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CategoriacomboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CategoriacomboBox.FormattingEnabled = true;
            this.CategoriacomboBox.Items.AddRange(new object[] {
            "Prueba"});
            this.CategoriacomboBox.Location = new System.Drawing.Point(553, 174);
            this.CategoriacomboBox.Name = "CategoriacomboBox";
            this.CategoriacomboBox.Size = new System.Drawing.Size(295, 33);
            this.CategoriacomboBox.TabIndex = 9;
            this.CategoriacomboBox.ValueMember = "1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(549, 146);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 25);
            this.label3.TabIndex = 8;
            this.label3.Text = "Categoria";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(189, 80);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 25);
            this.label4.TabIndex = 10;
            this.label4.Text = "Nombre";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(189, 146);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 25);
            this.label5.TabIndex = 12;
            this.label5.Text = "Precio";
            // 
            // PreciotextBox
            // 
            this.PreciotextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PreciotextBox.Location = new System.Drawing.Point(193, 173);
            this.PreciotextBox.Name = "PreciotextBox";
            this.PreciotextBox.Size = new System.Drawing.Size(116, 31);
            this.PreciotextBox.TabIndex = 11;
            this.PreciotextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PreciotextBox_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(192, 209);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 25);
            this.label6.TabIndex = 14;
            this.label6.Text = "Costo";
            // 
            // CostotextBox
            // 
            this.CostotextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CostotextBox.Location = new System.Drawing.Point(193, 236);
            this.CostotextBox.Name = "CostotextBox";
            this.CostotextBox.Size = new System.Drawing.Size(116, 31);
            this.CostotextBox.TabIndex = 13;
            this.CostotextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CostotextBox_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(55, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 25);
            this.label7.TabIndex = 15;
            this.label7.Text = "Imagen";
            // 
            // Cancelarbutton
            // 
            this.Cancelarbutton.BackColor = System.Drawing.SystemColors.Control;
            this.Cancelarbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cancelarbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cancelarbutton.Location = new System.Drawing.Point(740, 419);
            this.Cancelarbutton.Name = "Cancelarbutton";
            this.Cancelarbutton.Size = new System.Drawing.Size(147, 50);
            this.Cancelarbutton.TabIndex = 16;
            this.Cancelarbutton.Text = "Cancelar";
            this.Cancelarbutton.UseVisualStyleBackColor = false;
            this.Cancelarbutton.Click += new System.EventHandler(this.Cancelarbutton_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(357, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(186, 29);
            this.label8.TabIndex = 17;
            this.label8.Text = "Nuevo Producto";
            // 
            // RegistrarProveedorbutton
            // 
            this.RegistrarProveedorbutton.BackColor = System.Drawing.SystemColors.Control;
            this.RegistrarProveedorbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RegistrarProveedorbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RegistrarProveedorbutton.Image = ((System.Drawing.Image)(resources.GetObject("RegistrarProveedorbutton.Image")));
            this.RegistrarProveedorbutton.Location = new System.Drawing.Point(854, 237);
            this.RegistrarProveedorbutton.Name = "RegistrarProveedorbutton";
            this.RegistrarProveedorbutton.Size = new System.Drawing.Size(42, 32);
            this.RegistrarProveedorbutton.TabIndex = 18;
            this.RegistrarProveedorbutton.UseVisualStyleBackColor = false;
            this.RegistrarProveedorbutton.Click += new System.EventHandler(this.RegistrarProveedorbutton_Click);
            // 
            // ImagenopenFileDialog
            // 
            this.ImagenopenFileDialog.FileName = "ImagenopenFileDialog";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(6, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(130, 25);
            this.label9.TabIndex = 19;
            this.label9.Text = "Ingredientes";
            // 
            // IngredientesdataGridView
            // 
            this.IngredientesdataGridView.AllowUserToAddRows = false;
            this.IngredientesdataGridView.AllowUserToDeleteRows = false;
            this.IngredientesdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.IngredientesdataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IngredienteId,
            this.Nombre,
            this.Cantidad});
            this.IngredientesdataGridView.Location = new System.Drawing.Point(11, 82);
            this.IngredientesdataGridView.Name = "IngredientesdataGridView";
            this.IngredientesdataGridView.ReadOnly = true;
            this.IngredientesdataGridView.Size = new System.Drawing.Size(365, 112);
            this.IngredientesdataGridView.TabIndex = 20;
            // 
            // IngredienteId
            // 
            this.IngredienteId.HeaderText = "Id";
            this.IngredienteId.Name = "IngredienteId";
            this.IngredienteId.ReadOnly = true;
            this.IngredienteId.Width = 37;
            // 
            // Nombre
            // 
            this.Nombre.HeaderText = "Nombre";
            this.Nombre.Name = "Nombre";
            this.Nombre.ReadOnly = true;
            this.Nombre.Width = 220;
            // 
            // Cantidad
            // 
            this.Cantidad.HeaderText = "Cantidad";
            this.Cantidad.Name = "Cantidad";
            this.Cantidad.ReadOnly = true;
            this.Cantidad.Width = 70;
            // 
            // IngredientescomboBox
            // 
            this.IngredientescomboBox.DisplayMember = "1";
            this.IngredientescomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IngredientescomboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IngredientescomboBox.FormattingEnabled = true;
            this.IngredientescomboBox.Location = new System.Drawing.Point(11, 44);
            this.IngredientescomboBox.Name = "IngredientescomboBox";
            this.IngredientescomboBox.Size = new System.Drawing.Size(227, 33);
            this.IngredientescomboBox.TabIndex = 22;
            this.IngredientescomboBox.ValueMember = "1";
            // 
            // Agregarbutton
            // 
            this.Agregarbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Agregarbutton.Location = new System.Drawing.Point(381, 44);
            this.Agregarbutton.Name = "Agregarbutton";
            this.Agregarbutton.Size = new System.Drawing.Size(89, 32);
            this.Agregarbutton.TabIndex = 23;
            this.Agregarbutton.Text = "Agregar";
            this.Agregarbutton.UseVisualStyleBackColor = true;
            this.Agregarbutton.Click += new System.EventHandler(this.Agregarbutton_Click);
            // 
            // CantidadtextBox
            // 
            this.CantidadtextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CantidadtextBox.Location = new System.Drawing.Point(312, 44);
            this.CantidadtextBox.Name = "CantidadtextBox";
            this.CantidadtextBox.Size = new System.Drawing.Size(64, 31);
            this.CantidadtextBox.TabIndex = 24;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(243, 47);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 25);
            this.label10.TabIndex = 25;
            this.label10.Text = "Cant.";
            // 
            // IngredientesgroupBox
            // 
            this.IngredientesgroupBox.Controls.Add(this.label9);
            this.IngredientesgroupBox.Controls.Add(this.label10);
            this.IngredientesgroupBox.Controls.Add(this.IngredientesdataGridView);
            this.IngredientesgroupBox.Controls.Add(this.CantidadtextBox);
            this.IngredientesgroupBox.Controls.Add(this.IngredientescomboBox);
            this.IngredientesgroupBox.Controls.Add(this.Agregarbutton);
            this.IngredientesgroupBox.Location = new System.Drawing.Point(12, 269);
            this.IngredientesgroupBox.Name = "IngredientesgroupBox";
            this.IngredientesgroupBox.Size = new System.Drawing.Size(476, 200);
            this.IngredientesgroupBox.TabIndex = 26;
            this.IngredientesgroupBox.TabStop = false;
            this.IngredientesgroupBox.Visible = false;
            // 
            // ClaseProductocomboBox
            // 
            this.ClaseProductocomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ClaseProductocomboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClaseProductocomboBox.FormattingEnabled = true;
            this.ClaseProductocomboBox.Location = new System.Drawing.Point(553, 301);
            this.ClaseProductocomboBox.Name = "ClaseProductocomboBox";
            this.ClaseProductocomboBox.Size = new System.Drawing.Size(295, 33);
            this.ClaseProductocomboBox.TabIndex = 28;
            this.ClaseProductocomboBox.SelectedIndexChanged += new System.EventHandler(this.ClaseProductocomboBox_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(549, 273);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(159, 25);
            this.label11.TabIndex = 27;
            this.label11.Text = "Clase Producto";
            // 
            // UnidadMedidacomboBox
            // 
            this.UnidadMedidacomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.UnidadMedidacomboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UnidadMedidacomboBox.FormattingEnabled = true;
            this.UnidadMedidacomboBox.Items.AddRange(new object[] {
            "Unidad",
            "Libra",
            "Onza",
            "CC",
            "Gramo",
            "Litro"});
            this.UnidadMedidacomboBox.Location = new System.Drawing.Point(553, 365);
            this.UnidadMedidacomboBox.Name = "UnidadMedidacomboBox";
            this.UnidadMedidacomboBox.Size = new System.Drawing.Size(295, 33);
            this.UnidadMedidacomboBox.TabIndex = 30;
            this.UnidadMedidacomboBox.Visible = false;
            // 
            // Medidalabel
            // 
            this.Medidalabel.AutoSize = true;
            this.Medidalabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Medidalabel.Location = new System.Drawing.Point(549, 337);
            this.Medidalabel.Name = "Medidalabel";
            this.Medidalabel.Size = new System.Drawing.Size(187, 25);
            this.Medidalabel.TabIndex = 29;
            this.Medidalabel.Text = "Unidad de Medida";
            this.Medidalabel.Visible = false;
            // 
            // RegistroProductos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(899, 481);
            this.Controls.Add(this.UnidadMedidacomboBox);
            this.Controls.Add(this.Medidalabel);
            this.Controls.Add(this.ClaseProductocomboBox);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.IngredientesgroupBox);
            this.Controls.Add(this.RegistrarProveedorbutton);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.Cancelarbutton);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.CostotextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.PreciotextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.CategoriacomboBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ProveedorcomboBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.NombretextBox);
            this.Controls.Add(this.ActivocheckBox);
            this.Controls.Add(this.Guardarbutton);
            this.Controls.Add(this.TipoProductocomboBox);
            this.Controls.Add(this.ImagenpictureBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "RegistroProductos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RegistroProductos";
            this.Load += new System.EventHandler(this.RegistroProductos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ImagenpictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IngredientesdataGridView)).EndInit();
            this.IngredientesgroupBox.ResumeLayout(false);
            this.IngredientesgroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox ImagenpictureBox;
        private System.Windows.Forms.ComboBox TipoProductocomboBox;
        private System.Windows.Forms.Button Guardarbutton;
        private System.Windows.Forms.CheckBox ActivocheckBox;
        private System.Windows.Forms.TextBox NombretextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox ProveedorcomboBox;
        private System.Windows.Forms.ComboBox CategoriacomboBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox PreciotextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox CostotextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button Cancelarbutton;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button RegistrarProveedorbutton;
        private System.Windows.Forms.OpenFileDialog ImagenopenFileDialog;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView IngredientesdataGridView;
        private System.Windows.Forms.ComboBox IngredientescomboBox;
        private System.Windows.Forms.Button Agregarbutton;
        private System.Windows.Forms.DataGridViewTextBoxColumn IngredienteId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad;
        private System.Windows.Forms.TextBox CantidadtextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox IngredientesgroupBox;
        private System.Windows.Forms.ComboBox ClaseProductocomboBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox UnidadMedidacomboBox;
        private System.Windows.Forms.Label Medidalabel;
    }
}