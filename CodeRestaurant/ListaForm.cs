﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BLL;

namespace CodeRestaurant
{
    public partial class ListaForm : Form
    {
        private Productos productos;
        public static int filtrar;
        Clientes clientes;
        public ListaForm()
        {
            productos = new Productos();
            clientes = new Clientes();
            InitializeComponent();
            
        }

        private void ListaForm_Load(object sender, EventArgs e)
        {
            BuscarporcomboBox.SelectedIndex = 0;
        }

        private void FiltrotextBox_TextChanged(object sender, EventArgs e)
        {
            if (filtrar == 0)
            {
                ItemsdataGridView.DataSource = clientes.Listado("ClienteId, Nombre, Direccion, Telefono", " Nombre LIKE '%"+FiltrotextBox.Text+"%'", "1");
            }
            else if (filtrar == 2)
            {
                ItemsdataGridView.DataSource = productos.Listado("p.ProductoId, p.Nombre, tp.Nombre as Tipo, p.Precio, p.Costo", " p.Nombre LIKE '%" + FiltrotextBox.Text + "%'", "");
            }
        }
    }
}
