﻿using BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CodeRestaurant
{
    public partial class RegistroCaja : Form
    {
        private Usuarios usuario;
        private Cajas caja;
        public RegistroCaja()
        {
            InitializeComponent();
            usuario = new Usuarios();
            caja = new Cajas();           
        }

        private void RegistroCaja_Load(object sender, EventArgs e)
        {
            CargarDatos();
        }

        public bool Estado()
        {
            caja.Buscar(0);
            if (caja.Estado == 1)
                return true;
            return false;
        }

        private void CargarDatos()
        {
            NombreUsuariotextBox.Text = LoginForm.NombreUsuario;
            caja.Buscar(0);            
            BalancetextBox.Text = caja.Balance.ToString();           
        }

        private bool LlenarDatos()
        {
            bool retorno = true;
            if (BalancetextBox.Text.Length > 0)
            {
                caja.Balance = Seguridad.ValidarDouble(BalancetextBox.Text);
                caja.UsuarioId = LoginForm.UsuarioId;
            }
            else
            {
                retorno = false;
            }
            return retorno;
        }

        public void AbrirFormMain()
        {
            this.Visible = false;
            MainForm main = new MainForm();

            main.Text = "CodeBar [" + LoginForm.NombreUsuario + "]";
            if (LoginForm.UsuarioId != 1)
            {
                if (usuario.Buscar(LoginForm.UsuarioId))
                {
                    if (usuario.TipoUsuario == 0)
                    {
                        main.backupToolStripMenuItem.Visible = false;
                        main.configuracionesToolStripMenuItem.Visible = false;
                    }
                    if (usuario.TipoUsuario == 1)
                    {
                        main.UsuarioarchivoToolStripMenuItem.Visible = false;
                        main.inventarioToolStripMenuItem.Visible = false;
                        main.configuracionesToolStripMenuItem.Visible = false;
                        main.backupToolStripMenuItem.Visible = false;
                        main.productosToolStripMenuItem.Visible = false;
                        main.proveedoresToolStripMenuItem.Visible = false;
                        main.meserosToolStripMenuItem.Visible = false;
                    }
                }
            }
            this.Hide();
            main.Show();            
        }

        private void AbrirCajabutton_Click(object sender, EventArgs e)
        {
            if(NombreUsuariotextBox.Text != "" || BalancetextBox.Text != "")
            {
                if (LlenarDatos())
                {
                    if (caja.Insertar())
                    {
                        AbrirFormMain();
                    }
                    else
                    {
                        MessageBox.Show("Error manin");
                    }
                }
                else
                {
                    MessageBox.Show("Error 23");
                }
            }
            
        }

        private void Cancelarbutton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void BalancetextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso 
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan 
                e.Handled = true;
            }
        }
    }
}
