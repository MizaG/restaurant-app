﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BLL;

namespace CodeRestaurant
{
    public partial class CantidadInventario : Form
    {
        private Inventarios inventario;
        public static int ProductoId;
        public static bool Condicion;

        public CantidadInventario()
        {
            InitializeComponent();
            inventario = new Inventarios();
        }

        public bool LlenarDatoInventario()
        {
            try
            {
                if (ProductoId > 0)
                {
                    inventario.ProductoId = ProductoId;
                    inventario.Cantidad = Seguridad.ValidarIdEntero(CantidadtextBox.Text);
                    inventario.Fecha = DateTime.Now.ToString("d");
                }
                else
                {
                    return false;
                }

            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        private void GuardarInventario()
        {
            try
            {
                if (!inventario.Buscar(ProductoId))
                {
                    if (LlenarDatoInventario())
                    {
                        if (inventario.Insertar())
                        {
                            MessageBox.Show("Guardado Correctamente", "Correcto", MessageBoxButtons.OK, MessageBoxIcon.Information);                            
                        }
                        else
                        {
                            MessageBox.Show("Error al Guardar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Faltan datos");
                    }
                }
                else
                {
                    int cantidad = Seguridad.ValidarIdEntero(CantidadtextBox.Text);

                    if (Seguridad.ValidarIdEntero(CantidadtextBox.Text) > 0)
                    {
                        if (Condicion)
                        {
                            CantidadtextBox.Text = (cantidad + inventario.Cantidad).ToString();
                        }
                        else
                        {
                            CantidadtextBox.Text = (inventario.Cantidad - cantidad).ToString();
                        }
                        
                        if (LlenarDatoInventario())
                        {
                            if (inventario.Editar())
                            {
                                MessageBox.Show("Guardado Correctamente", "Correcto", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else
                            {
                                MessageBox.Show("Error al Actualizar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Error al Actualizar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        CantidadtextBox.Text = inventario.Cantidad.ToString();
                        GuardarInventario();
                    }
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Error", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Guardarbutton_Click(object sender, EventArgs e)
        {
            if(CantidadtextBox.Text != "")
            {
                GuardarInventario();
                this.Close();
            }
            GuardarInventario();
            this.Close();
        }

        private void Cancelarbutton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CantidadtextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso 
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan 
                e.Handled = true;
            }
        }
    }
}
