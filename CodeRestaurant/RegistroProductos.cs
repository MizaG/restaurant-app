﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BLL;

namespace CodeRestaurant
{
    public partial class RegistroProductos : Form
    {
        private TiposProductos tipos;
        private Productos productos;
        private Proveedores proveedores;
        public static int ProductoId;

        public RegistroProductos()
        {
            InitializeComponent();
            tipos = new TiposProductos();
            productos = new Productos();
            proveedores = new Proveedores();
            LlenarComboBox();
            BuscarProducto();
            CargarClaseProd();
        }

        private void BuscarProducto()
        {
            if (productos.Buscar(ProductoId) && ProductoId > 0)
            {
                ImagenpictureBox.ImageLocation = productos.Imagen;
                NombretextBox.Text = productos.Nombre;
                PreciotextBox.Text = productos.Precio.ToString();
                CostotextBox.Text = productos.Costo.ToString();

                foreach (var item in productos.productos)
                {
                    IngredientesdataGridView.Rows.Add(item.ProductoSecundarioId, item.NombreIngrediente, item.Cantidad);
                }
            }
        }

        private void LlenarComboBox()
        {
            TipoProductocomboBox.DataSource = tipos.Listado(" * ", " 1=1 ", " TipoProductosId ");
            TipoProductocomboBox.DisplayMember = "Nombre";
            TipoProductocomboBox.ValueMember = "TipoProductosId";

            ProveedorcomboBox.DataSource = proveedores.Listado(" * ", " 1=1 ", " ProveedorId ");
            ProveedorcomboBox.DisplayMember = "NombreEmpresa";
            ProveedorcomboBox.ValueMember = "ProveedorId";

            IngredientescomboBox.DataSource = productos.Listado(" p.ProductoId,p.Nombre ", " ClaseProductoId = 3 ", "");
            IngredientescomboBox.DisplayMember = "Nombre";
            IngredientescomboBox.ValueMember = "ProductoId";
            CategoriacomboBox.SelectedIndex = 0;
        }

        public void Limpia()
        {
            //errorProvider.Clear();
            //ProductoIdtextBox.Clear();
            NombretextBox.Clear();
            PreciotextBox.Clear();
            CostotextBox.Clear();
            TipoProductocomboBox.SelectedIndex = 0;
            CategoriacomboBox.SelectedIndex = 0;
        }

        public bool LlenarDato()
        {
            bool retorno = true;
            //errorProvider.Clear();

            if (NombretextBox.Text.Length > 0)
            {
                productos.Nombre = NombretextBox.Text;
            }
            else
            {
                //errorProvider.SetError(NombretextBox, "Ingrese un Nombre");
                retorno = false;
            }
            if (ImagenpictureBox.ImageLocation != null)
            {
                productos.Imagen = ImagenpictureBox.ImageLocation.ToString();
            }
            else
            {
                //productos.Imagen = @"C:\Users\Francis Castillo\Pictures\FacturacionMF\11-11-2017-23-9-9.jpg";
                //ClienteerrorProvider.SetError(ClientepictureBox, "Ingrese Una Foto");
                //retorno = false;
            }
            if (Seguridad.ValidarIdEntero(PreciotextBox.Text) > 0)
            {
                productos.Precio = Seguridad.ValidarDouble(PreciotextBox.Text);
            }
            else
            {
                //errorProvider.SetError(PreciotextBox, "Ingrese un Precio");
                retorno = false;
            }

            productos.TipoProductosId = (int)TipoProductocomboBox.SelectedValue;
            productos.Categoria = CategoriacomboBox.Text;
            productos.ProveedorId = (int)ProveedorcomboBox.SelectedValue;
            productos.ClaseProductoId = (int)ClaseProductocomboBox.SelectedValue;

            if (Seguridad.ValidarIdEntero(CostotextBox.Text) > 0)
            {
                productos.Costo = Seguridad.ValidarDouble(CostotextBox.Text);
            }
            else
            {
                //errorProvider.SetError(CostotextBox, "Ingrese un Costo");
                retorno = false;
            }
            if (IngredientesdataGridView.RowCount > 0)
            {
                for (int i = 0; i < IngredientesdataGridView.RowCount; i++)
                {
                    productos.AgregarProductos(Seguridad.ValidarIdEntero(IngredientesdataGridView.Rows[i].Cells[0].Value.ToString()), Seguridad.ValidarIdEntero(IngredientesdataGridView.Rows[i].Cells[2].Value.ToString()));
                }
            }
            productos.Ingrediente = IngredientesdataGridView.RowCount > 0;
            return retorno;
        }

        private void Guardarbutton_Click(object sender, EventArgs e)
        {
            if (ProductoId <= 0)
            {
                if (LlenarDato())
                {
                    if (productos.Insertar())
                    {
                        MessageBox.Show("Guardado Correctamente", "Confirmar", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Limpia();
                    }
                    else
                    {
                        MessageBox.Show("Error Al Guardar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Faltan Datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                if (LlenarDato())
                {
                    productos.ProductoId = ProductoId;
                    if (productos.Editar())
                    {
                        MessageBox.Show("Modificado Correctamente", "Confirmar", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                        ProductoId = 0;
                    }
                    else
                    {
                        MessageBox.Show("Error Al Modificar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Faltan Datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void Cancelarbutton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void RegistroProductos_Load(object sender, EventArgs e)
        {

        }

        private void ImagenpictureBox_Click(object sender, EventArgs e)
        {
            ImagenopenFileDialog.ShowDialog();
            if (ImagenopenFileDialog.FileName != null)
            {
                ImagenpictureBox.ImageLocation = ImagenopenFileDialog.FileName;
            }
        }

        private void RegistrarProveedorbutton_Click(object sender, EventArgs e)
        {
            RegistroProveedores pro = new RegistroProveedores();
            pro.ShowDialog();
            LlenarComboBox();
        }

        private void Agregarbutton_Click(object sender, EventArgs e)
        {
            if(CantidadtextBox.Text != "" && Convert.ToInt32(CantidadtextBox.Text) < 0)
            {
                bool esIgual = false;

                if (IngredientesdataGridView.Rows.Count > 0)
                {
                    foreach (DataGridViewRow row in IngredientesdataGridView.Rows)
                    {
                        if (Seguridad.ValidarIdEntero(row.Cells[0].Value.ToString()) == Seguridad.ValidarIdEntero(IngredientescomboBox.SelectedValue.ToString()))
                        {
                            row.Cells[2].Value = CantidadtextBox.Text;
                            esIgual = true;
                            break;
                        }

                    }

                    if (esIgual == false)
                    {
                        DataGridViewRow fila = new DataGridViewRow();
                        fila.CreateCells(IngredientesdataGridView);
                        fila.Cells[0].Value = IngredientescomboBox.SelectedValue;
                        fila.Cells[1].Value = IngredientescomboBox.Text;
                        fila.Cells[2].Value = CantidadtextBox.Text;
                        IngredientesdataGridView.Rows.Add(fila);

                    }
                }
                else
                {
                    DataGridViewRow filas = new DataGridViewRow();
                    filas.CreateCells(IngredientesdataGridView);
                    filas.Cells[0].Value = IngredientescomboBox.SelectedValue;
                    filas.Cells[1].Value = IngredientescomboBox.Text;
                    filas.Cells[2].Value = CantidadtextBox.Text;

                    IngredientesdataGridView.Rows.Add(filas);
                }
            }
            else
            {
                MessageBox.Show("Digite una Cantidad correcta!");
            }
            
        }

        public void CargarClaseProd()
        {
            ClaseProductocomboBox.DataSource = productos.ListarClaseProd(" * ", " 1=1 ");
            ClaseProductocomboBox.DisplayMember = "DescripcionClase";
            ClaseProductocomboBox.ValueMember = "ClaseProductosId";
        }

        private void ClaseProductocomboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(ClaseProductocomboBox.Text == "Compuesto")
            {
                IngredientesgroupBox.Visible = true;
                UnidadMedidacomboBox.Visible = false;
                Medidalabel.Visible = false;
            }
            else if(ClaseProductocomboBox.Text == "Ingrediente")
            {
                Medidalabel.Visible = true;
                UnidadMedidacomboBox.Visible = true;
                IngredientesgroupBox.Visible = false;
            }
            else
            {
                Medidalabel.Visible = false;
                UnidadMedidacomboBox.Visible = false;
                IngredientesgroupBox.Visible = false;
            }
        }

        private void PreciotextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso 
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan 
                e.Handled = true;
            }
        }

        private void CostotextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso 
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan 
                e.Handled = true;
            }
        }
    }
}
