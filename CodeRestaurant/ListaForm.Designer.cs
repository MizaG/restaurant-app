﻿namespace CodeRestaurant
{
    partial class ListaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BuscarporcomboBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.FiltrotextBox = new System.Windows.Forms.TextBox();
            this.Nuevobutton = new System.Windows.Forms.Button();
            this.DataGridpanel = new System.Windows.Forms.Panel();
            this.ItemsdataGridView = new System.Windows.Forms.DataGridView();
            this.Listalabel = new System.Windows.Forms.Label();
            this.DataGridpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ItemsdataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // BuscarporcomboBox
            // 
            this.BuscarporcomboBox.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BuscarporcomboBox.FormattingEnabled = true;
            this.BuscarporcomboBox.Items.AddRange(new object[] {
            "Nombre",
            "Categoria",
            "ID"});
            this.BuscarporcomboBox.Location = new System.Drawing.Point(140, 66);
            this.BuscarporcomboBox.Name = "BuscarporcomboBox";
            this.BuscarporcomboBox.Size = new System.Drawing.Size(211, 32);
            this.BuscarporcomboBox.TabIndex = 24;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 24);
            this.label3.TabIndex = 23;
            this.label3.Text = "Buscar por:";
            // 
            // FiltrotextBox
            // 
            this.FiltrotextBox.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FiltrotextBox.Location = new System.Drawing.Point(357, 65);
            this.FiltrotextBox.Name = "FiltrotextBox";
            this.FiltrotextBox.Size = new System.Drawing.Size(473, 33);
            this.FiltrotextBox.TabIndex = 22;
            this.FiltrotextBox.TextChanged += new System.EventHandler(this.FiltrotextBox_TextChanged);
            // 
            // Nuevobutton
            // 
            this.Nuevobutton.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.Nuevobutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Nuevobutton.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Nuevobutton.ForeColor = System.Drawing.Color.White;
            this.Nuevobutton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Nuevobutton.Location = new System.Drawing.Point(836, 55);
            this.Nuevobutton.Name = "Nuevobutton";
            this.Nuevobutton.Size = new System.Drawing.Size(147, 50);
            this.Nuevobutton.TabIndex = 25;
            this.Nuevobutton.Text = "Nuevo";
            this.Nuevobutton.UseVisualStyleBackColor = false;
            // 
            // DataGridpanel
            // 
            this.DataGridpanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DataGridpanel.Controls.Add(this.ItemsdataGridView);
            this.DataGridpanel.Location = new System.Drawing.Point(12, 111);
            this.DataGridpanel.Name = "DataGridpanel";
            this.DataGridpanel.Size = new System.Drawing.Size(971, 365);
            this.DataGridpanel.TabIndex = 26;
            // 
            // ItemsdataGridView
            // 
            this.ItemsdataGridView.AllowUserToAddRows = false;
            this.ItemsdataGridView.AllowUserToDeleteRows = false;
            this.ItemsdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ItemsdataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ItemsdataGridView.Location = new System.Drawing.Point(0, 0);
            this.ItemsdataGridView.Name = "ItemsdataGridView";
            this.ItemsdataGridView.ReadOnly = true;
            this.ItemsdataGridView.Size = new System.Drawing.Size(971, 365);
            this.ItemsdataGridView.TabIndex = 0;
            // 
            // Listalabel
            // 
            this.Listalabel.AutoSize = true;
            this.Listalabel.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Listalabel.Location = new System.Drawing.Point(12, 9);
            this.Listalabel.Name = "Listalabel";
            this.Listalabel.Size = new System.Drawing.Size(64, 30);
            this.Listalabel.TabIndex = 36;
            this.Listalabel.Text = "Lista";
            // 
            // ListaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(991, 488);
            this.Controls.Add(this.Listalabel);
            this.Controls.Add(this.DataGridpanel);
            this.Controls.Add(this.Nuevobutton);
            this.Controls.Add(this.BuscarporcomboBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.FiltrotextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ListaForm";
            this.Text = "ListaForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ListaForm_Load);
            this.DataGridpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ItemsdataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox FiltrotextBox;
        private System.Windows.Forms.Panel DataGridpanel;
        private System.Windows.Forms.Label Listalabel;
        public System.Windows.Forms.ComboBox BuscarporcomboBox;
        public System.Windows.Forms.DataGridView ItemsdataGridView;
        public System.Windows.Forms.Button Nuevobutton;
    }
}