﻿using BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CodeRestaurant
{
    public partial class ConfiguracionesForm : Form
    {
        Configuraciones configuracion;
        public ConfiguracionesForm()
        {
            InitializeComponent();
            configuracion = new Configuraciones();
            CargarConfiguracion();
        }

        public void CargarConfiguracion()
        {
            configuracion.Buscar(1);
            NCFtextBox.Text = configuracion.NCF;
            RutaBackuptextBox.Text = configuracion.RutaBackup;
            RutaFototextBox.Text = configuracion.RutaFoto;
            RNCtextBox.Text = configuracion.RNC;
        }

        public bool LlenarCampos()
        {
            bool retorno = true;
            if (NCFtextBox.Text.Length > 0)
            {
                configuracion.NCF = NCFtextBox.Text;
            }
            else
            {
                errorProvider.SetError(NCFtextBox, "Ingrese una Ruta");
                retorno = false;
            }
            if (RutaBackuptextBox.Text.Length > 0)
            {
                configuracion.RutaBackup = RutaBackuptextBox.Text;
            }
            else
            {
                errorProvider.SetError(RutaBackuptextBox, "Ingrese una Ruta");
                retorno = false;
            }
            if (RutaFototextBox.Text.Length > 0)
            {
                configuracion.RutaFoto = RutaFototextBox.Text;
            }
            else
            {
                errorProvider.SetError(RutaFototextBox, "Ingrese una Ruta");
                retorno = false;
            }
            if (RNCtextBox.Text.Length > 0)
            {
                configuracion.RNC = RNCtextBox.Text;
            }
            else
            {
                errorProvider.SetError(RNCtextBox, "Ingrese el RNC");
                retorno = false;
            }

            return retorno;
        }

        private void Nuevobutton_Click_1(object sender, EventArgs e)
        {
            NCFtextBox.Clear();
            RutaBackuptextBox.Clear();
            RutaFototextBox.Clear();
            RNCtextBox.Clear();
            errorProvider.Clear();
        }

        private void Guardarbutton_Click_1(object sender, EventArgs e)
        {
            if (configuracion.NCF.Length > 0)
            {
                if (LlenarCampos())
                {
                    configuracion.ConfiguracionId = 1;
                    if (configuracion.Editar())
                    {
                        MessageBox.Show("Modificado Correctamente", "Correcto", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Error Al Modificado", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

            }
            else
            {
                if (LlenarCampos())
                {
                    if (configuracion.Insertar())
                    {
                        MessageBox.Show("Guardado Correctamente", "Correcto", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Error Al Guardar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Ingrese los Datos Correctamente", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
    }
}
