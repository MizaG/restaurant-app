﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BLL;

namespace CodeRestaurant
{
    public partial class UnirCuenta : Form
    {
        public static int MesaId;
        public Mesas mesa = new Mesas();

        public UnirCuenta()
        {
            InitializeComponent();
            CargarPreventas();
        }

        public void CargarPreventas()
        {
            Preventa pv = new Preventa();
            MesacomboBox.DataSource = pv.Listado("PreventaId, Concat( NombreCliente, ', Mesa',MesaId, ', Total: ', Total) as 'Cuenta'", " and MesaId not in ( " + MesaId + ")", "");
            MesacomboBox.DisplayMember = "Cuenta";
            MesacomboBox.ValueMember = "PreventaId";
        }

        private void Guardarbutton_Click(object sender, EventArgs e)
        {
            Preventa pv = new Preventa();
            if(pv.UnirPreventa(MesaId, (int)MesacomboBox.SelectedValue))
            {
                if (pv.BuscarMesaId((int)MesacomboBox.SelectedValue))
                {
                    mesa.MesaId = pv.MesaId;
                    mesa.Liberar();
                }
                pv.PreventaId = (int)MesacomboBox.SelectedValue;
                pv.Eliminar();
            }
            this.Close();
            MessageBox.Show("La cuenta ha sido unida satisfactoriamente!");
        }

        private void Cancelarbutton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
