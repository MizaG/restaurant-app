﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BLL;

namespace CodeRestaurant
{
    public partial class RegistroProveedores : Form
    {
        private Proveedores proveedores;
        private Ciudades ciudades;
        public static int ProveedorId;

        public RegistroProveedores()
        {
            proveedores = new Proveedores();
            ciudades = new Ciudades();
            InitializeComponent();
            LlenarComboBox();
            BuscarProveedores();
        }

        private void BuscarProveedores()
        {
            if (proveedores.Buscar(ProveedorId) && ProveedorId > 0)
            {
                NombreRepresentantetextBox.Text = proveedores.NombreRepresentante;
                NombreEmpresatextBox.Text = proveedores.NombreEmpresa;
                RNCtextBox.Text = proveedores.RNC;
                DirecciontextBox.Text = proveedores.Direccion;
                TelefonotextBox.Text = proveedores.Telefono;
                CelulartextBox.Text = proveedores.Celular;
                CorreotextBox.Text = proveedores.Correo;
            }
        }

        private void LlenarComboBox()
        {
            CiudadcomboBox.DataSource = ciudades.Listado(" * ", " 1=1 ", " CiudadId ");
            CiudadcomboBox.DisplayMember = "Nombre";
            CiudadcomboBox.ValueMember = "CiudadId";
        }

        public void Limpiar()
        {
            NombreEmpresatextBox.Clear();
            NombreRepresentantetextBox.Clear();
            RNCtextBox.Clear();
            TelefonotextBox.Clear();
            DirecciontextBox.Clear();
            CorreotextBox.Clear();
            CiudadcomboBox.SelectedIndex = 0;
            CelulartextBox.Clear();
        }

        private bool LlenarDatos()
        {
            bool retorno = true;
            proveedores.NombreEmpresa = NombreEmpresatextBox.Text;
            proveedores.NombreRepresentante = NombreRepresentantetextBox.Text;
            proveedores.RNC = RNCtextBox.Text;
            proveedores.Telefono = TelefonotextBox.Text;
            proveedores.Direccion = DirecciontextBox.Text;
            proveedores.Correo = CorreotextBox.Text;
            proveedores.CiudadId = (int)CiudadcomboBox.SelectedValue;
            proveedores.Celular = CelulartextBox.Text;
            return retorno;
        }

        private void Guardarbutton_Click(object sender, EventArgs e)
        {
            if (ProveedorId <= 0)
            {
                if (LlenarDatos())
                {
                    if (proveedores.Insertar())
                    {
                        MessageBox.Show("Guardado Correctamente", "Confirmar", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Limpiar();
                    }
                    else
                    {
                        MessageBox.Show("Error Al Guardar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Faltan Datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                if (LlenarDatos())
                {
                    proveedores.ProveedorId = ProveedorId;
                    if (proveedores.Editar())
                    {
                        MessageBox.Show("Guardado Correctamente", "Confirmar", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Limpiar();
                    }
                    else
                    {
                        MessageBox.Show("Error Al Guardar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Faltan Datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void Cancelarbutton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
