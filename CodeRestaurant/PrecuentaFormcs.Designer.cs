﻿namespace CodeRestaurant
{
    partial class PrecuentaFormcs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrecuentaFormcs));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Precuentapanel = new System.Windows.Forms.Panel();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.ProductosdataGridView = new System.Windows.Forms.DataGridView();
            this.ProductoId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Articulo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Precio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SubTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.MeserocomboBox = new System.Windows.Forms.ComboBox();
            this.MontoTotallabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonAtras = new System.Windows.Forms.Button();
            this.Dividirbutton = new System.Windows.Forms.Button();
            this.Notabutton = new System.Windows.Forms.Button();
            this.Eliminarbutton = new System.Windows.Forms.Button();
            this.Duplicarbutton = new System.Windows.Forms.Button();
            this.Unirbutton = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.ContenedorPrecuentapanel = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.ComidasflowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.EnsaladasflowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.BebidasflowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.HeladosflowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.TragosflowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.Precuentapanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ProductosdataGridView)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.ContenedorPrecuentapanel.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.SuspendLayout();
            // 
            // Precuentapanel
            // 
            this.Precuentapanel.BackColor = System.Drawing.Color.Black;
            this.Precuentapanel.Controls.Add(this.button7);
            this.Precuentapanel.Controls.Add(this.button6);
            this.Precuentapanel.Controls.Add(this.ProductosdataGridView);
            this.Precuentapanel.Controls.Add(this.panel1);
            this.Precuentapanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.Precuentapanel.Location = new System.Drawing.Point(0, 0);
            this.Precuentapanel.Name = "Precuentapanel";
            this.Precuentapanel.Size = new System.Drawing.Size(404, 688);
            this.Precuentapanel.TabIndex = 0;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.Red;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.ForeColor = System.Drawing.Color.White;
            this.button7.Image = ((System.Drawing.Image)(resources.GetObject("button7.Image")));
            this.button7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button7.Location = new System.Drawing.Point(3, 599);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(398, 69);
            this.button7.TabIndex = 10;
            this.button7.Text = "Finalizar";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.Green;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.Color.White;
            this.button6.Image = ((System.Drawing.Image)(resources.GetObject("button6.Image")));
            this.button6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.Location = new System.Drawing.Point(3, 524);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(398, 69);
            this.button6.TabIndex = 9;
            this.button6.Text = "Precuenta";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // ProductosdataGridView
            // 
            this.ProductosdataGridView.AllowUserToAddRows = false;
            this.ProductosdataGridView.AllowUserToDeleteRows = false;
            this.ProductosdataGridView.AllowUserToResizeColumns = false;
            this.ProductosdataGridView.AllowUserToResizeRows = false;
            this.ProductosdataGridView.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ProductosdataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.ProductosdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ProductosdataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProductoId,
            this.Articulo,
            this.Precio,
            this.Cantidad,
            this.SubTotal});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ProductosdataGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.ProductosdataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.ProductosdataGridView.Location = new System.Drawing.Point(3, 81);
            this.ProductosdataGridView.MultiSelect = false;
            this.ProductosdataGridView.Name = "ProductosdataGridView";
            this.ProductosdataGridView.ReadOnly = true;
            this.ProductosdataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ProductosdataGridView.Size = new System.Drawing.Size(401, 440);
            this.ProductosdataGridView.TabIndex = 1;
            // 
            // ProductoId
            // 
            this.ProductoId.HeaderText = "ProductoId";
            this.ProductoId.Name = "ProductoId";
            this.ProductoId.ReadOnly = true;
            this.ProductoId.Visible = false;
            // 
            // Articulo
            // 
            this.Articulo.HeaderText = "Articulo";
            this.Articulo.Name = "Articulo";
            this.Articulo.ReadOnly = true;
            this.Articulo.Width = 170;
            // 
            // Precio
            // 
            this.Precio.HeaderText = "Precio";
            this.Precio.Name = "Precio";
            this.Precio.ReadOnly = true;
            this.Precio.Width = 60;
            // 
            // Cantidad
            // 
            this.Cantidad.HeaderText = "Cantidad";
            this.Cantidad.Name = "Cantidad";
            this.Cantidad.ReadOnly = true;
            this.Cantidad.Width = 85;
            // 
            // SubTotal
            // 
            this.SubTotal.HeaderText = "SubTotal";
            this.SubTotal.Name = "SubTotal";
            this.SubTotal.ReadOnly = true;
            this.SubTotal.Width = 80;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.MeserocomboBox);
            this.panel1.Controls.Add(this.MontoTotallabel);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(404, 81);
            this.panel1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(10, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 24);
            this.label2.TabIndex = 9;
            this.label2.Text = "Mesero";
            // 
            // MeserocomboBox
            // 
            this.MeserocomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MeserocomboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeserocomboBox.FormattingEnabled = true;
            this.MeserocomboBox.Location = new System.Drawing.Point(90, 43);
            this.MeserocomboBox.Name = "MeserocomboBox";
            this.MeserocomboBox.Size = new System.Drawing.Size(311, 32);
            this.MeserocomboBox.TabIndex = 8;
            // 
            // MontoTotallabel
            // 
            this.MontoTotallabel.AutoSize = true;
            this.MontoTotallabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MontoTotallabel.ForeColor = System.Drawing.Color.White;
            this.MontoTotallabel.Location = new System.Drawing.Point(99, 9);
            this.MontoTotallabel.Name = "MontoTotallabel";
            this.MontoTotallabel.Size = new System.Drawing.Size(67, 31);
            this.MontoTotallabel.TabIndex = 1;
            this.MontoTotallabel.Text = "0.00";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "TOTAL";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.buttonAtras);
            this.panel2.Controls.Add(this.Dividirbutton);
            this.panel2.Controls.Add(this.Notabutton);
            this.panel2.Controls.Add(this.Eliminarbutton);
            this.panel2.Controls.Add(this.Duplicarbutton);
            this.panel2.Controls.Add(this.Unirbutton);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(404, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(92, 688);
            this.panel2.TabIndex = 1;
            // 
            // buttonAtras
            // 
            this.buttonAtras.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(224)))));
            this.buttonAtras.FlatAppearance.BorderSize = 0;
            this.buttonAtras.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAtras.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAtras.ForeColor = System.Drawing.Color.White;
            this.buttonAtras.Image = ((System.Drawing.Image)(resources.GetObject("buttonAtras.Image")));
            this.buttonAtras.Location = new System.Drawing.Point(7, 3);
            this.buttonAtras.Name = "buttonAtras";
            this.buttonAtras.Size = new System.Drawing.Size(81, 88);
            this.buttonAtras.TabIndex = 11;
            this.buttonAtras.Text = "Atras";
            this.buttonAtras.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonAtras.UseVisualStyleBackColor = false;
            this.buttonAtras.Click += new System.EventHandler(this.buttonAtras_Click);
            // 
            // Dividirbutton
            // 
            this.Dividirbutton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(224)))));
            this.Dividirbutton.FlatAppearance.BorderSize = 0;
            this.Dividirbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Dividirbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dividirbutton.ForeColor = System.Drawing.Color.White;
            this.Dividirbutton.Image = ((System.Drawing.Image)(resources.GetObject("Dividirbutton.Image")));
            this.Dividirbutton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Dividirbutton.Location = new System.Drawing.Point(7, 197);
            this.Dividirbutton.Name = "Dividirbutton";
            this.Dividirbutton.Size = new System.Drawing.Size(81, 88);
            this.Dividirbutton.TabIndex = 10;
            this.Dividirbutton.Text = "Dividir Cuenta";
            this.Dividirbutton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Dividirbutton.UseVisualStyleBackColor = false;
            this.Dividirbutton.Click += new System.EventHandler(this.Dividirbutton_Click_1);
            // 
            // Notabutton
            // 
            this.Notabutton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(224)))));
            this.Notabutton.FlatAppearance.BorderSize = 0;
            this.Notabutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Notabutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Notabutton.ForeColor = System.Drawing.Color.White;
            this.Notabutton.Image = ((System.Drawing.Image)(resources.GetObject("Notabutton.Image")));
            this.Notabutton.Location = new System.Drawing.Point(7, 100);
            this.Notabutton.Name = "Notabutton";
            this.Notabutton.Size = new System.Drawing.Size(81, 88);
            this.Notabutton.TabIndex = 9;
            this.Notabutton.Text = "Nota";
            this.Notabutton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Notabutton.UseVisualStyleBackColor = false;
            this.Notabutton.Click += new System.EventHandler(this.Notabutton_Click);
            // 
            // Eliminarbutton
            // 
            this.Eliminarbutton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(224)))));
            this.Eliminarbutton.FlatAppearance.BorderSize = 0;
            this.Eliminarbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Eliminarbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Eliminarbutton.ForeColor = System.Drawing.Color.White;
            this.Eliminarbutton.Image = ((System.Drawing.Image)(resources.GetObject("Eliminarbutton.Image")));
            this.Eliminarbutton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Eliminarbutton.Location = new System.Drawing.Point(7, 488);
            this.Eliminarbutton.Name = "Eliminarbutton";
            this.Eliminarbutton.Size = new System.Drawing.Size(81, 88);
            this.Eliminarbutton.TabIndex = 8;
            this.Eliminarbutton.Text = "Eliminar Articulo";
            this.Eliminarbutton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Eliminarbutton.UseVisualStyleBackColor = false;
            this.Eliminarbutton.Click += new System.EventHandler(this.Eliminarbutton_Click);
            // 
            // Duplicarbutton
            // 
            this.Duplicarbutton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(224)))));
            this.Duplicarbutton.FlatAppearance.BorderSize = 0;
            this.Duplicarbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Duplicarbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Duplicarbutton.ForeColor = System.Drawing.Color.White;
            this.Duplicarbutton.Image = ((System.Drawing.Image)(resources.GetObject("Duplicarbutton.Image")));
            this.Duplicarbutton.Location = new System.Drawing.Point(7, 391);
            this.Duplicarbutton.Name = "Duplicarbutton";
            this.Duplicarbutton.Size = new System.Drawing.Size(81, 88);
            this.Duplicarbutton.TabIndex = 7;
            this.Duplicarbutton.Text = "Duplicar";
            this.Duplicarbutton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Duplicarbutton.UseVisualStyleBackColor = false;
            this.Duplicarbutton.Click += new System.EventHandler(this.Duplicarbutton_Click);
            // 
            // Unirbutton
            // 
            this.Unirbutton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(224)))));
            this.Unirbutton.FlatAppearance.BorderSize = 0;
            this.Unirbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Unirbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Unirbutton.ForeColor = System.Drawing.Color.White;
            this.Unirbutton.Image = ((System.Drawing.Image)(resources.GetObject("Unirbutton.Image")));
            this.Unirbutton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Unirbutton.Location = new System.Drawing.Point(7, 294);
            this.Unirbutton.Name = "Unirbutton";
            this.Unirbutton.Size = new System.Drawing.Size(81, 88);
            this.Unirbutton.TabIndex = 6;
            this.Unirbutton.Text = "Unir Cuenta";
            this.Unirbutton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Unirbutton.UseVisualStyleBackColor = false;
            this.Unirbutton.Click += new System.EventHandler(this.Unirbutton_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.ContenedorPrecuentapanel);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1207, 688);
            this.panel4.TabIndex = 2;
            // 
            // ContenedorPrecuentapanel
            // 
            this.ContenedorPrecuentapanel.Controls.Add(this.tabControl1);
            this.ContenedorPrecuentapanel.Controls.Add(this.panel2);
            this.ContenedorPrecuentapanel.Controls.Add(this.Precuentapanel);
            this.ContenedorPrecuentapanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ContenedorPrecuentapanel.Location = new System.Drawing.Point(0, 0);
            this.ContenedorPrecuentapanel.Name = "ContenedorPrecuentapanel";
            this.ContenedorPrecuentapanel.Size = new System.Drawing.Size(1207, 688);
            this.ContenedorPrecuentapanel.TabIndex = 1;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.ImageList = this.imageList1;
            this.tabControl1.Location = new System.Drawing.Point(496, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(711, 688);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.ComidasflowLayoutPanel);
            this.tabPage1.ImageIndex = 0;
            this.tabPage1.Location = new System.Drawing.Point(4, 34);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(703, 650);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Comidas ";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // ComidasflowLayoutPanel
            // 
            this.ComidasflowLayoutPanel.AutoScroll = true;
            this.ComidasflowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ComidasflowLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.ComidasflowLayoutPanel.Name = "ComidasflowLayoutPanel";
            this.ComidasflowLayoutPanel.Size = new System.Drawing.Size(697, 644);
            this.ComidasflowLayoutPanel.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.EnsaladasflowLayoutPanel);
            this.tabPage2.ImageIndex = 1;
            this.tabPage2.Location = new System.Drawing.Point(4, 34);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(703, 650);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Ensaladas";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // EnsaladasflowLayoutPanel
            // 
            this.EnsaladasflowLayoutPanel.AutoScroll = true;
            this.EnsaladasflowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EnsaladasflowLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.EnsaladasflowLayoutPanel.Name = "EnsaladasflowLayoutPanel";
            this.EnsaladasflowLayoutPanel.Size = new System.Drawing.Size(697, 644);
            this.EnsaladasflowLayoutPanel.TabIndex = 1;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.BebidasflowLayoutPanel);
            this.tabPage3.ImageIndex = 2;
            this.tabPage3.Location = new System.Drawing.Point(4, 34);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(703, 650);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Bebidas";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // BebidasflowLayoutPanel
            // 
            this.BebidasflowLayoutPanel.AutoScroll = true;
            this.BebidasflowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BebidasflowLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.BebidasflowLayoutPanel.Name = "BebidasflowLayoutPanel";
            this.BebidasflowLayoutPanel.Size = new System.Drawing.Size(697, 644);
            this.BebidasflowLayoutPanel.TabIndex = 1;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.HeladosflowLayoutPanel);
            this.tabPage4.ImageIndex = 3;
            this.tabPage4.Location = new System.Drawing.Point(4, 34);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(703, 650);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Helados";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // HeladosflowLayoutPanel
            // 
            this.HeladosflowLayoutPanel.AutoScroll = true;
            this.HeladosflowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HeladosflowLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.HeladosflowLayoutPanel.Name = "HeladosflowLayoutPanel";
            this.HeladosflowLayoutPanel.Size = new System.Drawing.Size(697, 644);
            this.HeladosflowLayoutPanel.TabIndex = 1;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.TragosflowLayoutPanel);
            this.tabPage5.ImageIndex = 4;
            this.tabPage5.Location = new System.Drawing.Point(4, 34);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(703, 650);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Tragos";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // TragosflowLayoutPanel
            // 
            this.TragosflowLayoutPanel.AutoScroll = true;
            this.TragosflowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TragosflowLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.TragosflowLayoutPanel.Name = "TragosflowLayoutPanel";
            this.TragosflowLayoutPanel.Size = new System.Drawing.Size(697, 644);
            this.TragosflowLayoutPanel.TabIndex = 1;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "comida2.png");
            this.imageList1.Images.SetKeyName(1, "ensalada.png");
            this.imageList1.Images.SetKeyName(2, "bebidas.png");
            this.imageList1.Images.SetKeyName(3, "IceCream2.png");
            this.imageList1.Images.SetKeyName(4, "tragos2.png");
            // 
            // PrecuentaFormcs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1207, 688);
            this.Controls.Add(this.panel4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "PrecuentaFormcs";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PrecuentaFormcs";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Precuentapanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ProductosdataGridView)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.ContenedorPrecuentapanel.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Precuentapanel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button Eliminarbutton;
        private System.Windows.Forms.Button Duplicarbutton;
        private System.Windows.Forms.Button Unirbutton;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label MontoTotallabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.FlowLayoutPanel ComidasflowLayoutPanel;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button Dividirbutton;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductoId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Articulo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Precio;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn SubTotal;
        private System.Windows.Forms.Button buttonAtras;
        private System.Windows.Forms.Panel ContenedorPrecuentapanel;
        private System.Windows.Forms.Button button7;
        public System.Windows.Forms.DataGridView ProductosdataGridView;
        public System.Windows.Forms.Button Notabutton;
        private System.Windows.Forms.FlowLayoutPanel EnsaladasflowLayoutPanel;
        private System.Windows.Forms.FlowLayoutPanel BebidasflowLayoutPanel;
        private System.Windows.Forms.FlowLayoutPanel HeladosflowLayoutPanel;
        private System.Windows.Forms.FlowLayoutPanel TragosflowLayoutPanel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox MeserocomboBox;
    }
}