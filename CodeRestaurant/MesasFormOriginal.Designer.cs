﻿namespace CodeRestaurant
{
    partial class MesasFormOriginal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MesasFormOriginal));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.label16 = new System.Windows.Forms.Label();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.Mesa1button = new System.Windows.Forms.Button();
            this.Mesa4button = new System.Windows.Forms.Button();
            this.Mesa6button = new System.Windows.Forms.Button();
            this.Mesa2button = new System.Windows.Forms.Button();
            this.Mesa7button = new System.Windows.Forms.Button();
            this.Mesa5button = new System.Windows.Forms.Button();
            this.Mesa8button = new System.Windows.Forms.Button();
            this.Mesa14button = new System.Windows.Forms.Button();
            this.Mesa15button = new System.Windows.Forms.Button();
            this.Mesa13button = new System.Windows.Forms.Button();
            this.Mesa16button = new System.Windows.Forms.Button();
            this.Mesa12button = new System.Windows.Forms.Button();
            this.Mesa11button = new System.Windows.Forms.Button();
            this.Mesa9button = new System.Windows.Forms.Button();
            this.Mesa22button = new System.Windows.Forms.Button();
            this.Mesa21button = new System.Windows.Forms.Button();
            this.Mesa20button = new System.Windows.Forms.Button();
            this.Mesa19button = new System.Windows.Forms.Button();
            this.Mesa3button = new System.Windows.Forms.Button();
            this.Mesa18button = new System.Windows.Forms.Button();
            this.Mesa17button = new System.Windows.Forms.Button();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.Mesa10button = new System.Windows.Forms.Button();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.label15 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 13;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.Controls.Add(this.pictureBox21, 7, 7);
            this.tableLayoutPanel1.Controls.Add(this.panel5, 6, 7);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox9, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.Mesa1button, 12, 6);
            this.tableLayoutPanel1.Controls.Add(this.Mesa4button, 11, 3);
            this.tableLayoutPanel1.Controls.Add(this.Mesa6button, 12, 0);
            this.tableLayoutPanel1.Controls.Add(this.Mesa2button, 11, 7);
            this.tableLayoutPanel1.Controls.Add(this.Mesa7button, 10, 0);
            this.tableLayoutPanel1.Controls.Add(this.Mesa5button, 10, 2);
            this.tableLayoutPanel1.Controls.Add(this.Mesa8button, 8, 0);
            this.tableLayoutPanel1.Controls.Add(this.Mesa14button, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.Mesa15button, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.Mesa13button, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.Mesa16button, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.Mesa12button, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.Mesa11button, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.Mesa9button, 6, 2);
            this.tableLayoutPanel1.Controls.Add(this.Mesa22button, 5, 8);
            this.tableLayoutPanel1.Controls.Add(this.Mesa21button, 5, 7);
            this.tableLayoutPanel1.Controls.Add(this.Mesa20button, 5, 6);
            this.tableLayoutPanel1.Controls.Add(this.Mesa19button, 5, 5);
            this.tableLayoutPanel1.Controls.Add(this.Mesa3button, 9, 6);
            this.tableLayoutPanel1.Controls.Add(this.Mesa18button, 6, 5);
            this.tableLayoutPanel1.Controls.Add(this.Mesa17button, 7, 5);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox5, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox1, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox3, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox2, 3, 6);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox6, 3, 8);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox7, 9, 8);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox4, 10, 8);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox8, 11, 8);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox10, 12, 8);
            this.tableLayoutPanel1.Controls.Add(this.Mesa10button, 6, 3);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox11, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox12, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox13, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox14, 8, 2);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox15, 12, 1);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox16, 10, 4);
            this.tableLayoutPanel1.Controls.Add(this.label1, 4, 8);
            this.tableLayoutPanel1.Controls.Add(this.label2, 4, 7);
            this.tableLayoutPanel1.Controls.Add(this.label3, 4, 6);
            this.tableLayoutPanel1.Controls.Add(this.label4, 4, 5);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 5, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 6, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 7, 4);
            this.tableLayoutPanel1.Controls.Add(this.label7, 8, 5);
            this.tableLayoutPanel1.Controls.Add(this.label12, 8, 6);
            this.tableLayoutPanel1.Controls.Add(this.label13, 8, 7);
            this.tableLayoutPanel1.Controls.Add(this.label14, 8, 8);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 6, 8);
            this.tableLayoutPanel1.Controls.Add(this.panel6, 7, 6);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 9;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(971, 524);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // pictureBox21
            // 
            this.pictureBox21.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox21.Image")));
            this.pictureBox21.Location = new System.Drawing.Point(521, 409);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(44, 31);
            this.pictureBox21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox21.TabIndex = 95;
            this.pictureBox21.TabStop = false;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.pictureBox19);
            this.panel5.Controls.Add(this.label16);
            this.panel5.Location = new System.Drawing.Point(447, 409);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(68, 52);
            this.panel5.TabIndex = 93;
            // 
            // pictureBox19
            // 
            this.pictureBox19.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox19.Image")));
            this.pictureBox19.Location = new System.Drawing.Point(20, 8);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(29, 37);
            this.pictureBox19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox19.TabIndex = 81;
            this.pictureBox19.TabStop = false;
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(49, -61);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(16, 180);
            this.label16.TabIndex = 49;
            this.label16.Text = "|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n" +
    "|\r\n|\r\n|";
            // 
            // pictureBox9
            // 
            this.pictureBox9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox9.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox9.Image")));
            this.pictureBox9.Location = new System.Drawing.Point(3, 467);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(68, 54);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox9.TabIndex = 72;
            this.pictureBox9.TabStop = false;
            // 
            // Mesa1button
            // 
            this.Mesa1button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Mesa1button.BackgroundImage")));
            this.Mesa1button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Mesa1button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Mesa1button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Mesa1button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Mesa1button.ForeColor = System.Drawing.Color.White;
            this.Mesa1button.Location = new System.Drawing.Point(891, 351);
            this.Mesa1button.Name = "Mesa1button";
            this.Mesa1button.Size = new System.Drawing.Size(77, 52);
            this.Mesa1button.TabIndex = 33;
            this.Mesa1button.Text = " 1";
            this.Mesa1button.UseVisualStyleBackColor = true;
            this.Mesa1button.Click += new System.EventHandler(this.Mesa1button_Click);
            // 
            // Mesa4button
            // 
            this.Mesa4button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Mesa4button.BackgroundImage")));
            this.Mesa4button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Mesa4button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Mesa4button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Mesa4button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Mesa4button.ForeColor = System.Drawing.Color.White;
            this.Mesa4button.Location = new System.Drawing.Point(817, 177);
            this.Mesa4button.Name = "Mesa4button";
            this.Mesa4button.Size = new System.Drawing.Size(68, 52);
            this.Mesa4button.TabIndex = 35;
            this.Mesa4button.Text = " 4";
            this.Mesa4button.UseVisualStyleBackColor = true;
            this.Mesa4button.Click += new System.EventHandler(this.Mesa4button_Click);
            // 
            // Mesa6button
            // 
            this.Mesa6button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Mesa6button.BackgroundImage")));
            this.Mesa6button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Mesa6button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Mesa6button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Mesa6button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Mesa6button.ForeColor = System.Drawing.Color.White;
            this.Mesa6button.Location = new System.Drawing.Point(891, 3);
            this.Mesa6button.Name = "Mesa6button";
            this.Mesa6button.Size = new System.Drawing.Size(77, 52);
            this.Mesa6button.TabIndex = 36;
            this.Mesa6button.Text = " 6";
            this.Mesa6button.UseVisualStyleBackColor = true;
            this.Mesa6button.Click += new System.EventHandler(this.Mesa6button_Click);
            // 
            // Mesa2button
            // 
            this.Mesa2button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Mesa2button.BackgroundImage")));
            this.Mesa2button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Mesa2button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Mesa2button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Mesa2button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Mesa2button.ForeColor = System.Drawing.Color.White;
            this.Mesa2button.Location = new System.Drawing.Point(817, 409);
            this.Mesa2button.Name = "Mesa2button";
            this.Mesa2button.Size = new System.Drawing.Size(68, 52);
            this.Mesa2button.TabIndex = 37;
            this.Mesa2button.Text = " 2";
            this.Mesa2button.UseVisualStyleBackColor = true;
            this.Mesa2button.Click += new System.EventHandler(this.Mesa2button_Click);
            // 
            // Mesa7button
            // 
            this.Mesa7button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Mesa7button.BackgroundImage")));
            this.Mesa7button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Mesa7button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Mesa7button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Mesa7button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Mesa7button.ForeColor = System.Drawing.Color.White;
            this.Mesa7button.Location = new System.Drawing.Point(743, 3);
            this.Mesa7button.Name = "Mesa7button";
            this.Mesa7button.Size = new System.Drawing.Size(68, 52);
            this.Mesa7button.TabIndex = 38;
            this.Mesa7button.Text = " 7";
            this.Mesa7button.UseVisualStyleBackColor = true;
            this.Mesa7button.Click += new System.EventHandler(this.Mesa7button_Click);
            // 
            // Mesa5button
            // 
            this.Mesa5button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Mesa5button.BackgroundImage")));
            this.Mesa5button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Mesa5button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Mesa5button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Mesa5button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Mesa5button.ForeColor = System.Drawing.Color.White;
            this.Mesa5button.Location = new System.Drawing.Point(743, 119);
            this.Mesa5button.Name = "Mesa5button";
            this.Mesa5button.Size = new System.Drawing.Size(68, 52);
            this.Mesa5button.TabIndex = 39;
            this.Mesa5button.Text = " 5";
            this.Mesa5button.UseVisualStyleBackColor = true;
            this.Mesa5button.Click += new System.EventHandler(this.Mesa5button_Click);
            // 
            // Mesa8button
            // 
            this.Mesa8button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Mesa8button.BackgroundImage")));
            this.Mesa8button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Mesa8button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Mesa8button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Mesa8button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Mesa8button.ForeColor = System.Drawing.Color.White;
            this.Mesa8button.Location = new System.Drawing.Point(595, 3);
            this.Mesa8button.Name = "Mesa8button";
            this.Mesa8button.Size = new System.Drawing.Size(68, 52);
            this.Mesa8button.TabIndex = 41;
            this.Mesa8button.Text = " 8";
            this.Mesa8button.UseVisualStyleBackColor = true;
            this.Mesa8button.Click += new System.EventHandler(this.Mesa8button_Click);
            // 
            // Mesa14button
            // 
            this.Mesa14button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Mesa14button.BackgroundImage")));
            this.Mesa14button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Mesa14button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Mesa14button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Mesa14button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Mesa14button.ForeColor = System.Drawing.Color.White;
            this.Mesa14button.Location = new System.Drawing.Point(3, 119);
            this.Mesa14button.Name = "Mesa14button";
            this.Mesa14button.Size = new System.Drawing.Size(68, 52);
            this.Mesa14button.TabIndex = 42;
            this.Mesa14button.Text = " 14";
            this.Mesa14button.UseVisualStyleBackColor = true;
            this.Mesa14button.Click += new System.EventHandler(this.Mesa14button_Click);
            // 
            // Mesa15button
            // 
            this.Mesa15button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Mesa15button.BackgroundImage")));
            this.Mesa15button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Mesa15button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Mesa15button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Mesa15button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Mesa15button.ForeColor = System.Drawing.Color.White;
            this.Mesa15button.Location = new System.Drawing.Point(3, 293);
            this.Mesa15button.Name = "Mesa15button";
            this.Mesa15button.Size = new System.Drawing.Size(68, 52);
            this.Mesa15button.TabIndex = 43;
            this.Mesa15button.Text = " 15";
            this.Mesa15button.UseVisualStyleBackColor = true;
            this.Mesa15button.Click += new System.EventHandler(this.Mesa15button_Click);
            // 
            // Mesa13button
            // 
            this.Mesa13button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Mesa13button.BackgroundImage")));
            this.Mesa13button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Mesa13button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Mesa13button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Mesa13button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Mesa13button.ForeColor = System.Drawing.Color.White;
            this.Mesa13button.Location = new System.Drawing.Point(225, 119);
            this.Mesa13button.Name = "Mesa13button";
            this.Mesa13button.Size = new System.Drawing.Size(68, 52);
            this.Mesa13button.TabIndex = 44;
            this.Mesa13button.Text = " 13";
            this.Mesa13button.UseVisualStyleBackColor = true;
            this.Mesa13button.Click += new System.EventHandler(this.Mesa13button_Click);
            // 
            // Mesa16button
            // 
            this.Mesa16button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Mesa16button.BackgroundImage")));
            this.Mesa16button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Mesa16button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Mesa16button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Mesa16button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Mesa16button.ForeColor = System.Drawing.Color.White;
            this.Mesa16button.Location = new System.Drawing.Point(225, 293);
            this.Mesa16button.Name = "Mesa16button";
            this.Mesa16button.Size = new System.Drawing.Size(68, 52);
            this.Mesa16button.TabIndex = 45;
            this.Mesa16button.Text = " 16";
            this.Mesa16button.UseVisualStyleBackColor = true;
            this.Mesa16button.Click += new System.EventHandler(this.Mesa16button_Click);
            // 
            // Mesa12button
            // 
            this.Mesa12button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Mesa12button.BackgroundImage")));
            this.Mesa12button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Mesa12button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Mesa12button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Mesa12button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Mesa12button.ForeColor = System.Drawing.Color.White;
            this.Mesa12button.Location = new System.Drawing.Point(299, 3);
            this.Mesa12button.Name = "Mesa12button";
            this.Mesa12button.Size = new System.Drawing.Size(68, 52);
            this.Mesa12button.TabIndex = 46;
            this.Mesa12button.Text = " 12";
            this.Mesa12button.UseVisualStyleBackColor = true;
            this.Mesa12button.Click += new System.EventHandler(this.Mesa12button_Click);
            // 
            // Mesa11button
            // 
            this.Mesa11button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Mesa11button.BackgroundImage")));
            this.Mesa11button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Mesa11button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Mesa11button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Mesa11button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Mesa11button.ForeColor = System.Drawing.Color.White;
            this.Mesa11button.Location = new System.Drawing.Point(299, 177);
            this.Mesa11button.Name = "Mesa11button";
            this.Mesa11button.Size = new System.Drawing.Size(68, 52);
            this.Mesa11button.TabIndex = 47;
            this.Mesa11button.Text = " 11";
            this.Mesa11button.UseVisualStyleBackColor = true;
            this.Mesa11button.Click += new System.EventHandler(this.Mesa11button_Click);
            // 
            // Mesa9button
            // 
            this.Mesa9button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Mesa9button.BackgroundImage")));
            this.Mesa9button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Mesa9button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Mesa9button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Mesa9button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Mesa9button.ForeColor = System.Drawing.Color.White;
            this.Mesa9button.Location = new System.Drawing.Point(447, 119);
            this.Mesa9button.Name = "Mesa9button";
            this.Mesa9button.Size = new System.Drawing.Size(68, 52);
            this.Mesa9button.TabIndex = 48;
            this.Mesa9button.Text = " 9";
            this.Mesa9button.UseVisualStyleBackColor = true;
            this.Mesa9button.Click += new System.EventHandler(this.Mesa9button_Click);
            // 
            // Mesa22button
            // 
            this.Mesa22button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Mesa22button.BackgroundImage")));
            this.Mesa22button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Mesa22button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Mesa22button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Mesa22button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Mesa22button.ForeColor = System.Drawing.Color.White;
            this.Mesa22button.Location = new System.Drawing.Point(373, 467);
            this.Mesa22button.Name = "Mesa22button";
            this.Mesa22button.Size = new System.Drawing.Size(68, 54);
            this.Mesa22button.TabIndex = 49;
            this.Mesa22button.Text = " 22";
            this.Mesa22button.UseVisualStyleBackColor = true;
            this.Mesa22button.Click += new System.EventHandler(this.Mesa22button_Click);
            // 
            // Mesa21button
            // 
            this.Mesa21button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Mesa21button.BackgroundImage")));
            this.Mesa21button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Mesa21button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Mesa21button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Mesa21button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Mesa21button.ForeColor = System.Drawing.Color.White;
            this.Mesa21button.Location = new System.Drawing.Point(373, 409);
            this.Mesa21button.Name = "Mesa21button";
            this.Mesa21button.Size = new System.Drawing.Size(68, 52);
            this.Mesa21button.TabIndex = 50;
            this.Mesa21button.Text = " 21";
            this.Mesa21button.UseVisualStyleBackColor = true;
            this.Mesa21button.Click += new System.EventHandler(this.Mesa21button_Click);
            // 
            // Mesa20button
            // 
            this.Mesa20button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Mesa20button.BackgroundImage")));
            this.Mesa20button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Mesa20button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Mesa20button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Mesa20button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Mesa20button.ForeColor = System.Drawing.Color.White;
            this.Mesa20button.Location = new System.Drawing.Point(373, 351);
            this.Mesa20button.Name = "Mesa20button";
            this.Mesa20button.Size = new System.Drawing.Size(68, 52);
            this.Mesa20button.TabIndex = 51;
            this.Mesa20button.Text = " 20";
            this.Mesa20button.UseVisualStyleBackColor = true;
            this.Mesa20button.Click += new System.EventHandler(this.Mesa20button_Click);
            // 
            // Mesa19button
            // 
            this.Mesa19button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Mesa19button.BackgroundImage")));
            this.Mesa19button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Mesa19button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Mesa19button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Mesa19button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Mesa19button.ForeColor = System.Drawing.Color.White;
            this.Mesa19button.Location = new System.Drawing.Point(373, 293);
            this.Mesa19button.Name = "Mesa19button";
            this.Mesa19button.Size = new System.Drawing.Size(68, 52);
            this.Mesa19button.TabIndex = 52;
            this.Mesa19button.Text = " 19";
            this.Mesa19button.UseVisualStyleBackColor = true;
            this.Mesa19button.Click += new System.EventHandler(this.Mesa19button_Click);
            // 
            // Mesa3button
            // 
            this.Mesa3button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Mesa3button.BackgroundImage")));
            this.Mesa3button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Mesa3button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Mesa3button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Mesa3button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Mesa3button.ForeColor = System.Drawing.Color.White;
            this.Mesa3button.Location = new System.Drawing.Point(669, 351);
            this.Mesa3button.Name = "Mesa3button";
            this.Mesa3button.Size = new System.Drawing.Size(68, 52);
            this.Mesa3button.TabIndex = 40;
            this.Mesa3button.Text = " 3";
            this.Mesa3button.UseVisualStyleBackColor = true;
            this.Mesa3button.Click += new System.EventHandler(this.Mesa3button_Click);
            // 
            // Mesa18button
            // 
            this.Mesa18button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Mesa18button.BackgroundImage")));
            this.Mesa18button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Mesa18button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Mesa18button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Mesa18button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Mesa18button.ForeColor = System.Drawing.Color.White;
            this.Mesa18button.Location = new System.Drawing.Point(447, 293);
            this.Mesa18button.Name = "Mesa18button";
            this.Mesa18button.Size = new System.Drawing.Size(68, 52);
            this.Mesa18button.TabIndex = 53;
            this.Mesa18button.Text = " 18";
            this.Mesa18button.UseVisualStyleBackColor = true;
            this.Mesa18button.Click += new System.EventHandler(this.Mesa18button_Click);
            // 
            // Mesa17button
            // 
            this.Mesa17button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Mesa17button.BackgroundImage")));
            this.Mesa17button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Mesa17button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Mesa17button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Mesa17button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Mesa17button.ForeColor = System.Drawing.Color.White;
            this.Mesa17button.Location = new System.Drawing.Point(521, 293);
            this.Mesa17button.Name = "Mesa17button";
            this.Mesa17button.Size = new System.Drawing.Size(68, 52);
            this.Mesa17button.TabIndex = 54;
            this.Mesa17button.Text = " 17";
            this.Mesa17button.UseVisualStyleBackColor = true;
            this.Mesa17button.Click += new System.EventHandler(this.Mesa17button_Click);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(77, 409);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(68, 52);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 64;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(151, 409);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(68, 52);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 65;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(3, 351);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(68, 52);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 67;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(225, 351);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(68, 52);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 66;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(225, 467);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(68, 54);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 69;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(669, 467);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(68, 54);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox7.TabIndex = 70;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(743, 467);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(68, 54);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 68;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(817, 467);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(68, 54);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox8.TabIndex = 71;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox10.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox10.Image")));
            this.pictureBox10.Location = new System.Drawing.Point(891, 467);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(77, 54);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox10.TabIndex = 73;
            this.pictureBox10.TabStop = false;
            // 
            // Mesa10button
            // 
            this.Mesa10button.BackgroundImage = global::CodeRestaurant.Properties.Resources.Mesa10;
            this.Mesa10button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Mesa10button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Mesa10button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Mesa10button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Mesa10button.ForeColor = System.Drawing.Color.White;
            this.Mesa10button.Location = new System.Drawing.Point(447, 177);
            this.Mesa10button.Name = "Mesa10button";
            this.Mesa10button.Size = new System.Drawing.Size(68, 52);
            this.Mesa10button.TabIndex = 55;
            this.Mesa10button.Text = " 10";
            this.Mesa10button.UseVisualStyleBackColor = true;
            this.Mesa10button.Click += new System.EventHandler(this.Mesa10button_Click);
            // 
            // pictureBox11
            // 
            this.pictureBox11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox11.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox11.Image")));
            this.pictureBox11.Location = new System.Drawing.Point(3, 3);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(68, 52);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox11.TabIndex = 74;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox12.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox12.Image")));
            this.pictureBox12.Location = new System.Drawing.Point(151, 61);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(68, 52);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox12.TabIndex = 75;
            this.pictureBox12.TabStop = false;
            // 
            // pictureBox13
            // 
            this.pictureBox13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox13.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox13.Image")));
            this.pictureBox13.Location = new System.Drawing.Point(151, 235);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(68, 52);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox13.TabIndex = 76;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox14.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox14.Image")));
            this.pictureBox14.Location = new System.Drawing.Point(595, 119);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(68, 52);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox14.TabIndex = 77;
            this.pictureBox14.TabStop = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox15.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox15.Image")));
            this.pictureBox15.Location = new System.Drawing.Point(891, 61);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(77, 52);
            this.pictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox15.TabIndex = 78;
            this.pictureBox15.TabStop = false;
            // 
            // pictureBox16
            // 
            this.pictureBox16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox16.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox16.Image")));
            this.pictureBox16.Location = new System.Drawing.Point(743, 235);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(68, 52);
            this.pictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox16.TabIndex = 79;
            this.pictureBox16.TabStop = false;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Location = new System.Drawing.Point(354, 464);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 60);
            this.label1.TabIndex = 80;
            this.label1.Text = "|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n" +
    "|\r\n|\r\n|";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.Location = new System.Drawing.Point(354, 406);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 58);
            this.label2.TabIndex = 81;
            this.label2.Text = "|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n" +
    "|\r\n|\r\n|";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Location = new System.Drawing.Point(354, 348);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 58);
            this.label3.TabIndex = 82;
            this.label3.Text = "|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n" +
    "|\r\n|\r\n|";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.Location = new System.Drawing.Point(354, 290);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 58);
            this.label4.TabIndex = 83;
            this.label4.Text = "|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n" +
    "|\r\n|\r\n|";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Location = new System.Drawing.Point(373, 235);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(68, 52);
            this.panel1.TabIndex = 85;
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(55, 37);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(10, 15);
            this.label19.TabIndex = 78;
            this.label19.Text = "|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n" +
    "|\r\n|\r\n|";
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(-16, 37);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 15);
            this.label6.TabIndex = 46;
            this.label6.Text = "________________________________________________________________________";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Location = new System.Drawing.Point(447, 235);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(68, 52);
            this.panel3.TabIndex = 87;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(3, 37);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(10, 15);
            this.label9.TabIndex = 79;
            this.label9.Text = "|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n" +
    "|\r\n|\r\n|";
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(55, 37);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(10, 15);
            this.label10.TabIndex = 78;
            this.label10.Text = "|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n" +
    "|\r\n|\r\n|";
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Location = new System.Drawing.Point(12, 37);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 15);
            this.label11.TabIndex = 46;
            this.label11.Text = "________________________________________________________________________";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Location = new System.Drawing.Point(521, 235);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(68, 52);
            this.panel2.TabIndex = 86;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(3, 37);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(10, 15);
            this.label5.TabIndex = 79;
            this.label5.Text = "|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n" +
    "|\r\n|\r\n|";
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Location = new System.Drawing.Point(12, 37);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 15);
            this.label8.TabIndex = 46;
            this.label8.Text = "________________________________________________________________________";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(595, 290);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 58);
            this.label7.TabIndex = 88;
            this.label7.Text = "|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n" +
    "|\r\n|\r\n|";
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(595, 348);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(13, 58);
            this.label12.TabIndex = 89;
            this.label12.Text = "|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n" +
    "|\r\n|\r\n|";
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(595, 406);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(13, 58);
            this.label13.TabIndex = 90;
            this.label13.Text = "|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n" +
    "|\r\n|\r\n|";
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(595, 464);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(13, 58);
            this.label14.TabIndex = 91;
            this.label14.Text = "|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n" +
    "|\r\n|\r\n|";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.pictureBox20);
            this.panel4.Controls.Add(this.label15);
            this.panel4.Location = new System.Drawing.Point(447, 467);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(68, 54);
            this.panel4.TabIndex = 92;
            // 
            // pictureBox20
            // 
            this.pictureBox20.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox20.Image")));
            this.pictureBox20.Location = new System.Drawing.Point(20, 9);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(29, 37);
            this.pictureBox20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox20.TabIndex = 81;
            this.pictureBox20.TabStop = false;
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(49, -61);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(16, 180);
            this.label15.TabIndex = 49;
            this.label15.Text = "|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n" +
    "|\r\n|\r\n|";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.pictureBox17);
            this.panel6.Controls.Add(this.pictureBox18);
            this.panel6.Controls.Add(this.label17);
            this.panel6.Location = new System.Drawing.Point(521, 351);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(68, 52);
            this.panel6.TabIndex = 94;
            // 
            // pictureBox17
            // 
            this.pictureBox17.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox17.Image")));
            this.pictureBox17.Location = new System.Drawing.Point(36, 12);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(29, 37);
            this.pictureBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox17.TabIndex = 83;
            this.pictureBox17.TabStop = false;
            // 
            // pictureBox18
            // 
            this.pictureBox18.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox18.Image")));
            this.pictureBox18.Location = new System.Drawing.Point(3, 12);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(29, 37);
            this.pictureBox18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox18.TabIndex = 82;
            this.pictureBox18.TabStop = false;
            // 
            // label17
            // 
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(-71, 38);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(207, 14);
            this.label17.TabIndex = 50;
            this.label17.Text = "________________________________________________________________________";
            // 
            // MesasFormOriginal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(971, 524);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MesasFormOriginal";
            this.Text = "MesasFormOriginal";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button Mesa1button;
        private System.Windows.Forms.Button Mesa4button;
        private System.Windows.Forms.Button Mesa6button;
        private System.Windows.Forms.Button Mesa2button;
        private System.Windows.Forms.Button Mesa7button;
        private System.Windows.Forms.Button Mesa5button;
        private System.Windows.Forms.Button Mesa3button;
        private System.Windows.Forms.Button Mesa8button;
        private System.Windows.Forms.Button Mesa14button;
        private System.Windows.Forms.Button Mesa15button;
        private System.Windows.Forms.Button Mesa13button;
        private System.Windows.Forms.Button Mesa16button;
        private System.Windows.Forms.Button Mesa12button;
        private System.Windows.Forms.Button Mesa11button;
        private System.Windows.Forms.Button Mesa9button;
        private System.Windows.Forms.Button Mesa22button;
        private System.Windows.Forms.Button Mesa21button;
        private System.Windows.Forms.Button Mesa20button;
        private System.Windows.Forms.Button Mesa19button;
        private System.Windows.Forms.Button Mesa18button;
        private System.Windows.Forms.Button Mesa17button;
        private System.Windows.Forms.Button Mesa10button;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.PictureBox pictureBox21;
    }
}