﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BLL;

namespace CodeRestaurant
{
    public partial class RegistroInventario : Form
    {
        private Productos productos;
        private Inventarios inventario;

        public RegistroInventario()
        {
            InitializeComponent();                       
        }

        private void CargarProductos()
        {
            ProductosdataGridView.DataSource = productos.Listado("p.ProductoId,p.Nombre, tp.Nombre as TipoProducto, p.Precio, p.Costo "," 1 = 1 ","");
        }

        private void CargarInventario()
        {
            InventariodataGridView.DataSource = inventario.Listado("i.ProductoId, p.Nombre, i.Cantidad, i.Fecha ", " i.ProductoId = p.ProductoId","");
        }

        private void RegistroInventario_Load(object sender, EventArgs e)
        {
            productos = new Productos();
            inventario = new Inventarios();
            CargarProductos();
            CargarInventario();
        }

        private void Agregarbutton_Click(object sender, EventArgs e)
        {
            CantidadInventario.ProductoId = Seguridad.ValidarIdEntero(ProductosdataGridView.Rows[ProductosdataGridView.CurrentRow.Index].Cells[0].Value.ToString());
            CantidadInventario.Condicion = true;
            CantidadInventario cantidad = new CantidadInventario();
            cantidad.NombretextBox.Text = ProductosdataGridView.Rows[ProductosdataGridView.CurrentRow.Index].Cells[1].Value.ToString();
            cantidad.ShowDialog();
            CargarInventario();
        }

        private void Restarbutton_Click(object sender, EventArgs e)
        {
            CantidadInventario.ProductoId = Seguridad.ValidarIdEntero(ProductosdataGridView.Rows[ProductosdataGridView.CurrentRow.Index].Cells[0].Value.ToString());
            CantidadInventario.Condicion = false;
            CantidadInventario cantidad = new CantidadInventario();
            cantidad.NombretextBox.Text = ProductosdataGridView.Rows[ProductosdataGridView.CurrentRow.Index].Cells[1].Value.ToString();
            cantidad.ShowDialog();
            CargarInventario();
        }
    }
}
