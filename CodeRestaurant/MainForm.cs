﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BLL;

namespace CodeRestaurant
{
    public partial class MainForm : Form
    {
        ListaForm lf;
        public MainForm()
        {
            InitializeComponent();
            lf = new ListaForm();
        }

        protected override void OnClosed(EventArgs e)
        {
            Application.Exit();
        }

        public static void AbrirFormPanel(object FormHijo, Panel panel)
        {
            if (panel.Controls.Count > 0)
                panel.Controls.RemoveAt(0);
            Form fh = FormHijo as Form;
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill;
            panel.Controls.Add(fh);
            panel.Tag = fh;
            fh.Show();
        }

        public void MostrarVentanaActiva(Panel panel)
        {
            if(panel == VentanaActiva1panel)
                VentanaActiva1panel.Visible = true;
            else
                VentanaActiva1panel.Visible = false;
            if (panel == VentanaActiva2panel)
                VentanaActiva2panel.Visible = true;
            else
                VentanaActiva2panel.Visible = false;
            if (panel == VentanaActiva3panel)
                VentanaActiva3panel.Visible = true;
            else
                VentanaActiva3panel.Visible = false;
            if (panel == VentanaActiva4panel)
                VentanaActiva4panel.Visible = true;
            else
                VentanaActiva4panel.Visible = false;
            if (panel == VentanaActiva5panel)
                VentanaActiva5panel.Visible = true;
            else
                VentanaActiva5panel.Visible = false;
            if (panel == VentanaActiva6panel)
                VentanaActiva6panel.Visible = true;
            else
                VentanaActiva6panel.Visible = false;
        }

        private void Mesasbutton_Click(object sender, EventArgs e)
        {
            var form = Application.OpenForms.OfType<MesasFormOriginal>().FirstOrDefault();
            MesasFormOriginal mf = form ?? new MesasFormOriginal();
            AbrirFormPanel(mf, this.Contenderpanel);

            MostrarVentanaActiva(VentanaActiva1panel);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PrecuentaFormcs.MesaId = 25;
            PrecuentaFormcs pf = new PrecuentaFormcs();
            pf.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            MostrarVentanaActiva(VentanaActiva5panel);
            
            if(MessageBox.Show("Esta seguro que desea salir?", "Alerta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Application.Exit();
            } 
        }

        private void MenupictureBox_Click(object sender, EventArgs e)
        {
            if(MenuVerticalpanel.Width == 200)
            {
                MenuVerticalpanel.Width = 16;
            }
            else
            {
                MenuVerticalpanel.Width = 200;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MostrarVentanaActiva(VentanaActiva3panel);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MostrarVentanaActiva(VentanaActiva4panel);

            VentanaCuadre vc = new VentanaCuadre();
            vc.ShowDialog();
        }

        private void CargarEventoCliente()
        {
            lf.Nuevobutton.Click += new EventHandler(handlerClientes_Click);
            lf.ItemsdataGridView.CellClick += new DataGridViewCellEventHandler(ClientesItemsdataGridView_CellClick);
        }

        private void QuitarEventoCliente()
        {
            lf.Nuevobutton.Click -= new EventHandler(handlerClientes_Click);
            lf.ItemsdataGridView.CellClick -= new DataGridViewCellEventHandler(ClientesItemsdataGridView_CellClick);
        }

        private void CargarClientes()
        {
            Clientes clientes = new Clientes();
            ListaForm.filtrar = 0;
            lf.ItemsdataGridView.DataSource = clientes.Listado("ClienteId, Nombre, Direccion, Telefono", "1=1", "1");
            lf.ItemsdataGridView.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            AbrirFormPanel(lf, this.Contenderpanel);
        }

        private void clientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            QuitarEventoMesero();
            QuitarEventoProducto();
            QuitarEventoProveedore();
            QuitarEventoUsuario();
            CargarEventoCliente();
            CargarClientes();
        }

        private void handlerClientes_Click(object sender, EventArgs e)
        {
            RegistroClientes rClientes = new RegistroClientes();
            rClientes.ShowDialog();
            CargarClientes();
        }

        private void ClientesItemsdataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            RegistroClientes.ClienteId = Seguridad.ValidarIdEntero(lf.ItemsdataGridView.Rows[lf.ItemsdataGridView.CurrentRow.Index].Cells[0].Value.ToString());
            RegistroClientes rClientes = new RegistroClientes();
            rClientes.ShowDialog();
            RegistroClientes.ClienteId = 0;
            CargarClientes();
        }

        private void CargarEventoMesero()
        {
            lf.Nuevobutton.Click += new EventHandler(handlerMeseros_Click);
            lf.ItemsdataGridView.CellClick += new DataGridViewCellEventHandler(MeserosItemsdataGridView_CellClick);
        }

        private void QuitarEventoMesero()
        {
            lf.Nuevobutton.Click -= new EventHandler(handlerMeseros_Click);
            lf.ItemsdataGridView.CellClick -= new DataGridViewCellEventHandler(MeserosItemsdataGridView_CellClick);
        }

        private void CargarMeseros()
        {
            Meseros meseros = new Meseros();
            lf.ItemsdataGridView.DataSource = meseros.Listado("MeseroId, Nombre", "1=1", "");
            lf.ItemsdataGridView.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            AbrirFormPanel(lf, this.Contenderpanel);
        }

        private void handlerMeseros_Click(object sender, EventArgs e)
        {
            RegistroMeseros rMesero = new RegistroMeseros();
            rMesero.ShowDialog();
            CargarMeseros();
        }

        private void MeserosItemsdataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            RegistroMeseros.MeseroId = Seguridad.ValidarIdEntero(lf.ItemsdataGridView.Rows[lf.ItemsdataGridView.CurrentRow.Index].Cells[0].Value.ToString());
            RegistroMeseros rMeseros = new RegistroMeseros();            
            rMeseros.ShowDialog();
            RegistroMeseros.MeseroId = 0;
            CargarMeseros();
        }

        private void meserosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            QuitarEventoCliente();
            QuitarEventoProducto();
            QuitarEventoProveedore();
            QuitarEventoUsuario();
            CargarEventoMesero();
            CargarMeseros();
        }

        private void CargarEventoProducto()
        {
            lf.Nuevobutton.Click += new EventHandler(handlerProductos_Click);
            lf.ItemsdataGridView.CellClick += new DataGridViewCellEventHandler(ProductoItemsdataGridView_CellClick);
        }

        private void QuitarEventoProducto()
        {
            lf.Nuevobutton.Click -= new EventHandler(handlerProductos_Click);
            lf.ItemsdataGridView.CellClick -= new DataGridViewCellEventHandler(ProductoItemsdataGridView_CellClick);
        }
        

        private void CargarProductos()
        {            
            Productos productos = new Productos();
            ListaForm.filtrar = 2;
            lf.ItemsdataGridView.DataSource = productos.Listado("p.ProductoId, p.Nombre, tp.Nombre as Tipo, p.Precio, p.Costo", " 1=1 ", "");
            lf.ItemsdataGridView.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            lf.ItemsdataGridView.RowHeadersWidth = 13;            
            AbrirFormPanel(lf, this.Contenderpanel);
        }

        private void handlerProductos_Click(object sender, EventArgs e)
        {
            RegistroProductos rProductos = new RegistroProductos();
            rProductos.ShowDialog();
            CargarProductos();
        }

        private void ProductoItemsdataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            RegistroProductos.ProductoId = Seguridad.ValidarIdEntero(lf.ItemsdataGridView.Rows[lf.ItemsdataGridView.CurrentRow.Index].Cells[0].Value.ToString());
            RegistroProductos rProductos = new RegistroProductos();            
            rProductos.ShowDialog();
            RegistroProductos.ProductoId = 0;
            CargarProductos();
            //MessageBox.Show(lf.ItemsdataGridView.Rows[lf.ItemsdataGridView.CurrentRow.Index].Cells[0].Value.ToString());
        }

        private void productosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            QuitarEventoMesero();
            QuitarEventoCliente();
            QuitarEventoUsuario();
            CargarEventoProducto();
            QuitarEventoProveedore();
            CargarProductos();
        }

        private void CargarEventoProveedore()
        {
            lf.Nuevobutton.Click += new EventHandler(handlerProveedores_Click);
            lf.ItemsdataGridView.CellClick += new DataGridViewCellEventHandler(ProveedoreItemsdataGridView_CellClick);
        }

        private void QuitarEventoProveedore()
        {
            lf.Nuevobutton.Click -= new EventHandler(handlerProveedores_Click);
            lf.ItemsdataGridView.CellClick -= new DataGridViewCellEventHandler(ProveedoreItemsdataGridView_CellClick);
        }

        private void CargarProveedores()
        {
            Proveedores proveedores = new Proveedores();
            lf.ItemsdataGridView.DataSource = proveedores.Listado("*", "1=1", "1");
            lf.ItemsdataGridView.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            AbrirFormPanel(lf, this.Contenderpanel);
        }

        private void ProveedoreItemsdataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            RegistroProveedores.ProveedorId = Seguridad.ValidarIdEntero(lf.ItemsdataGridView.Rows[lf.ItemsdataGridView.CurrentRow.Index].Cells[0].Value.ToString());
            RegistroProveedores rProveedore = new RegistroProveedores();
            rProveedore.ShowDialog();
            RegistroProveedores.ProveedorId = 0;
            CargarProveedores();
        }

        private void handlerProveedores_Click(object sender, EventArgs e)
        {
            RegistroProveedores rProveedores = new RegistroProveedores();
            rProveedores.ShowDialog();
            CargarProveedores();
        }

        private void proveedoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            QuitarEventoCliente();
            QuitarEventoMesero();
            QuitarEventoProducto();
            QuitarEventoUsuario();
            CargarEventoProveedore();
            CargarProveedores();
        }

        private void inventarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RegistroInventario inventario = new RegistroInventario();
            AbrirFormPanel(inventario,this.Contenderpanel);
        }

        private void archivoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            QuitarEventoCliente();
            QuitarEventoMesero();
            QuitarEventoProducto();
            QuitarEventoProveedore();
            CargarEventoUsuario();
            CargarUsuario();
        }

        private void QuitarEventoUsuario()
        {
            lf.Nuevobutton.Click -= new EventHandler(handlerUsuarios_Click);
            lf.ItemsdataGridView.CellClick -= new DataGridViewCellEventHandler(UsuariosItemsdataGridView_CellClick);
        }

        private void CargarEventoUsuario()
        {
            lf.Nuevobutton.Click += new EventHandler(handlerUsuarios_Click);
            lf.ItemsdataGridView.CellClick += new DataGridViewCellEventHandler(UsuariosItemsdataGridView_CellClick);
        }

        private void CargarUsuario()
        {
            Usuarios usuarios = new Usuarios();
            lf.ItemsdataGridView.DataSource = usuarios.Listado("*", "1=1", "1");           
            AbrirFormPanel(lf, this.Contenderpanel);
        }

        private void handlerUsuarios_Click(object sender, EventArgs e)
        {
            RegistroUsuarios rUsuarios = new RegistroUsuarios();
            rUsuarios.ShowDialog();
            CargarUsuario();
        }

        private void UsuariosItemsdataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            RegistroUsuarios.UsuarioId = Seguridad.ValidarIdEntero(lf.ItemsdataGridView.Rows[lf.ItemsdataGridView.CurrentRow.Index].Cells[0].Value.ToString());
            RegistroUsuarios rUsuario = new RegistroUsuarios();
            rUsuario.ShowDialog();
            RegistroUsuarios.UsuarioId = 0;
            CargarUsuario();
        }

        private void acercaDeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AcercaDeForm acerca = new AcercaDeForm();
            acerca.ShowDialog();
        }

        private void configuracionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConfiguracionesForm conf = new ConfiguracionesForm();
            conf.ShowDialog();
        }

        private void Iniciobutton_Click(object sender, EventArgs e)
        {
            MostrarVentanaActiva(VentanaActiva6panel);
            Contenderpanel.Controls.Clear();
            Contenderpanel.Controls.Add(pictureBox1);
        }

        private void backupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BackupForm backupForm = new BackupForm();
            backupForm.Show();

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            VentanaBloqueo vb = new VentanaBloqueo();
            vb.ShowDialog();
        }
    }
}
