﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BLL;

namespace CodeRestaurant
{

    public partial class ClaveAdm : Form
    {

        public ClaveAdm()
        {
            InitializeComponent();
        }

        private void OKbutton_Click(object sender, EventArgs e)
        {
            Usuarios user = new Usuarios();
            DataTable useradm = user.Listado(" * ", " TipoUsuario = 0 and Contrasena =  '"+ Seguridad.Encriptar(ContrasenatextBox.Text) +"'", "");
            if(useradm.Rows.Count > 0)
            {
                PrecuentaFormcs.EliminarArticulo = true;
                RegistroVentas.ValidacionAdm = true;
                this.Close();
            }
            else
            {
                MessageBox.Show("Contraseña digitada no es de usuario Administrador!");
            }
        }
    }
}
