﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BLL;

namespace CodeRestaurant
{
    public partial class RegistroClientes : Form
    {
        private Clientes clientes;
        public static int ClienteId;

        public RegistroClientes()
        {
            clientes = new Clientes();
            InitializeComponent();
            BuscarCliente();
        }

        private void BuscarCliente()
        {
            if (clientes.Buscar(ClienteId) && ClienteId > 0)
            {
                NombretextBox.Text = clientes.Nombre;
                TelefonotextBox.Text = clientes.Telefono;
                DirecciontextBox.Text = clientes.Direccion;
            }
        }

        private void Limpiar()
        {
            NombretextBox.Clear();
            TelefonotextBox.Clear();
            DirecciontextBox.Clear();
        }

        private bool LlenarDatos()
        {
            bool retorno = true;
            clientes.Nombre = NombretextBox.Text;
            clientes.Telefono = TelefonotextBox.Text;
            clientes.Direccion = DirecciontextBox.Text;
            return retorno;
        }

        private void Guardarbutton_Click(object sender, EventArgs e)
        {
            if(NombretextBox.Text != "" || TelefonotextBox.Text != "")
            {
                if (ClienteId <= 0)
                {
                    if (LlenarDatos())
                    {
                        if (clientes.Insertar())
                        {
                            MessageBox.Show("Guardado Correctamente", "Confirmar", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            Limpiar();
                        }
                        else
                        {
                            MessageBox.Show("Error Al Guardar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Faltan Datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    if (LlenarDatos())
                    {
                        clientes.ClienteId = ClienteId;
                        if (clientes.Editar())
                        {
                            MessageBox.Show("Modificado Correctamente", "Confirmar", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            ClienteId = 0;
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("Error Al Modificar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Faltan Datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            
        }

        private void Cancelarbutton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TelefonotextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso 
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan 
                e.Handled = true;
            }
        }
    }
}
