﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BLL;

namespace CodeRestaurant
{
    public partial class MesasFormOriginal : Form
    {
        private Preventa preventa;
        private Mesas mesas;

        public MesasFormOriginal()
        {
            InitializeComponent();
            preventa = new Preventa();
            mesas = new Mesas();
            PonerMesasOcupadas();
        }

        public void AbrirPrecuenta(int id)
        {
            PrecuentaFormcs.MesaId = id;
            PrecuentaFormcs pf = new PrecuentaFormcs();
            pf.ShowDialog();
            PonerMesasOcupadas();
        }

        public void PonerMesasOcupadas()
        {
            if (!mesas.Buscar(1))
            {
                Mesa1button.BackgroundImage = Properties.Resources.MesaOcupada;
            }
            else
            {
                Mesa1button.BackgroundImage = Properties.Resources.MesaDesocupada;
            }
            if (!mesas.Buscar(2))
            {
                Mesa2button.BackgroundImage = Properties.Resources.MesaOcupada;
            }
            else
            {
                Mesa2button.BackgroundImage = Properties.Resources.MesaDesocupada;
            }
            if (!mesas.Buscar(3))
            {
                Mesa3button.BackgroundImage = Properties.Resources.MesaOcupada;
            }
            else
            {
                Mesa3button.BackgroundImage = Properties.Resources.MesaDesocupada;
            }
            if (!mesas.Buscar(4))
            {
                Mesa4button.BackgroundImage = Properties.Resources.MesaOcupada;
            }
            else
            {
                Mesa4button.BackgroundImage = Properties.Resources.MesaDesocupada;
            }
            if (!mesas.Buscar(5))
            {
                Mesa5button.BackgroundImage = Properties.Resources.MesaOcupada;
            }
            else
            {
                Mesa5button.BackgroundImage = Properties.Resources.MesaDesocupada;
            }
            if (!mesas.Buscar(6))
            {
                Mesa6button.BackgroundImage = Properties.Resources.MesaOcupada;
            }
            else
            {
                Mesa6button.BackgroundImage = Properties.Resources.MesaDesocupada;
            }
            if (!mesas.Buscar(7))
            {
                Mesa7button.BackgroundImage = Properties.Resources.MesaOcupada;
            }
            else
            {
                Mesa7button.BackgroundImage = Properties.Resources.MesaDesocupada;
            }
            if (!mesas.Buscar(8))
            {
                Mesa8button.BackgroundImage = Properties.Resources.MesaOcupada;
            }
            else
            {
                Mesa8button.BackgroundImage = Properties.Resources.MesaDesocupada;
            }
            if (!mesas.Buscar(9))
            {
                Mesa9button.BackgroundImage = Properties.Resources.MesaOcupada;
            }
            else
            {
                Mesa9button.BackgroundImage = Properties.Resources.MesaDesocupada;
            }
            if (!mesas.Buscar(10))
            {
                Mesa10button.BackgroundImage = Properties.Resources.MesaOcupada;
            }
            else
            {
                Mesa10button.BackgroundImage = Properties.Resources.MesaDesocupada;
            }
            if (!mesas.Buscar(11))
            {
                Mesa11button.BackgroundImage = Properties.Resources.MesaOcupada;
            }
            else
            {
                Mesa11button.BackgroundImage = Properties.Resources.MesaDesocupada;
            }
            if (!mesas.Buscar(12))
            {
                Mesa12button.BackgroundImage = Properties.Resources.MesaOcupada;
            }
            else
            {
                Mesa12button.BackgroundImage = Properties.Resources.MesaDesocupada;
            }
            if (!mesas.Buscar(13))
            {
                Mesa13button.BackgroundImage = Properties.Resources.MesaOcupada;
            }
            else
            {
                Mesa13button.BackgroundImage = Properties.Resources.MesaDesocupada;
            }
            if (!mesas.Buscar(14))
            {
                Mesa14button.BackgroundImage = Properties.Resources.MesaOcupada;
            }
            else
            {
                Mesa14button.BackgroundImage = Properties.Resources.MesaDesocupada;
            }
            if (!mesas.Buscar(15))
            {
                Mesa15button.BackgroundImage = Properties.Resources.MesaOcupada;
            }
            else
            {
                Mesa15button.BackgroundImage = Properties.Resources.MesaDesocupada;
            }
            if (!mesas.Buscar(16))
            {
                Mesa16button.BackgroundImage = Properties.Resources.MesaOcupada;
            }
            else
            {
                Mesa16button.BackgroundImage = Properties.Resources.MesaDesocupada;
            }
            if (!mesas.Buscar(17))
            {
                Mesa17button.BackgroundImage = Properties.Resources.MesaOcupada;
            }
            else
            {
                Mesa17button.BackgroundImage = Properties.Resources.MesaDesocupada;
            }
            if (!mesas.Buscar(18))
            {
                Mesa18button.BackgroundImage = Properties.Resources.MesaOcupada;
            }
            else
            {
                Mesa18button.BackgroundImage = Properties.Resources.MesaDesocupada;
            }
            if (!mesas.Buscar(19))
            {
                Mesa19button.BackgroundImage = Properties.Resources.MesaOcupada;
            }
            else
            {
                Mesa19button.BackgroundImage = Properties.Resources.MesaDesocupada;
            }
            if (!mesas.Buscar(20))
            {
                Mesa20button.BackgroundImage = Properties.Resources.MesaOcupada;
            }
            else
            {
                Mesa20button.BackgroundImage = Properties.Resources.MesaDesocupada;
            }
            if (!mesas.Buscar(21))
            {
                Mesa21button.BackgroundImage = Properties.Resources.MesaOcupada;
            }
            else
            {
                Mesa21button.BackgroundImage = Properties.Resources.MesaDesocupada;
            }
            if (!mesas.Buscar(22))
            {
                Mesa22button.BackgroundImage = Properties.Resources.MesaOcupada;
            }
            else
            {
                Mesa22button.BackgroundImage = Properties.Resources.MesaDesocupada;
            }
        }

        private void Mesa1button_Click(object sender, EventArgs e)
        {
            AbrirPrecuenta(1);
        }

        private void Mesa2button_Click(object sender, EventArgs e)
        {
            AbrirPrecuenta(2);
        }

        private void Mesa3button_Click(object sender, EventArgs e)
        {
            AbrirPrecuenta(3);
        }

        private void Mesa4button_Click(object sender, EventArgs e)
        {
            AbrirPrecuenta(4);
        }

        private void Mesa5button_Click(object sender, EventArgs e)
        {
            AbrirPrecuenta(5);
        }

        private void Mesa6button_Click(object sender, EventArgs e)
        {
            AbrirPrecuenta(6);
        }

        private void Mesa7button_Click(object sender, EventArgs e)
        {
            AbrirPrecuenta(7);
        }

        private void Mesa8button_Click(object sender, EventArgs e)
        {
            AbrirPrecuenta(8);
        }

        private void Mesa9button_Click(object sender, EventArgs e)
        {
            AbrirPrecuenta(9);
        }

        private void Mesa10button_Click(object sender, EventArgs e)
        {
            AbrirPrecuenta(10);
        }

        private void Mesa11button_Click(object sender, EventArgs e)
        {
            AbrirPrecuenta(11);
        }

        private void Mesa12button_Click(object sender, EventArgs e)
        {
            AbrirPrecuenta(12);
        }

        private void Mesa13button_Click(object sender, EventArgs e)
        {
            AbrirPrecuenta(13);
        }

        private void Mesa14button_Click(object sender, EventArgs e)
        {
            AbrirPrecuenta(14);
        }

        private void Mesa15button_Click(object sender, EventArgs e)
        {
            AbrirPrecuenta(15);
        }

        private void Mesa16button_Click(object sender, EventArgs e)
        {
            AbrirPrecuenta(16);
        }

        private void Mesa17button_Click(object sender, EventArgs e)
        {
            AbrirPrecuenta(17);
        }

        private void Mesa18button_Click(object sender, EventArgs e)
        {
            AbrirPrecuenta(18);
        }

        private void Mesa19button_Click(object sender, EventArgs e)
        {
            AbrirPrecuenta(19);
        }

        private void Mesa20button_Click(object sender, EventArgs e)
        {
            AbrirPrecuenta(20);
        }

        private void Mesa21button_Click(object sender, EventArgs e)
        {
            AbrirPrecuenta(21);
        }

        private void Mesa22button_Click(object sender, EventArgs e)
        {
            AbrirPrecuenta(22);
        }
    }
}
