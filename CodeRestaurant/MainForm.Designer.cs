﻿namespace CodeRestaurant
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.MenuVerticalpanel = new System.Windows.Forms.Panel();
            this.VentanaActiva6panel = new System.Windows.Forms.Panel();
            this.VentanaActiva5panel = new System.Windows.Forms.Panel();
            this.VentanaActiva4panel = new System.Windows.Forms.Panel();
            this.VentanaActiva3panel = new System.Windows.Forms.Panel();
            this.VentanaActiva2panel = new System.Windows.Forms.Panel();
            this.VentanaActiva1panel = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.Contenderpanel = new System.Windows.Forms.Panel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.UsuarioarchivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.edicionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.meserosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.proveedoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inventarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configuracionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.acercaDeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MenupictureBox = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Iniciobutton = new System.Windows.Forms.Button();
            this.Salirbutton = new System.Windows.Forms.Button();
            this.Cuadrebutton = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.VentaDirectabutton = new System.Windows.Forms.Button();
            this.Mesasbutton = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.MenuVerticalpanel.SuspendLayout();
            this.panel2.SuspendLayout();
            this.Contenderpanel.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MenupictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // MenuVerticalpanel
            // 
            this.MenuVerticalpanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.MenuVerticalpanel.Controls.Add(this.pictureBox2);
            this.MenuVerticalpanel.Controls.Add(this.VentanaActiva6panel);
            this.MenuVerticalpanel.Controls.Add(this.Iniciobutton);
            this.MenuVerticalpanel.Controls.Add(this.VentanaActiva5panel);
            this.MenuVerticalpanel.Controls.Add(this.VentanaActiva4panel);
            this.MenuVerticalpanel.Controls.Add(this.VentanaActiva3panel);
            this.MenuVerticalpanel.Controls.Add(this.VentanaActiva2panel);
            this.MenuVerticalpanel.Controls.Add(this.Salirbutton);
            this.MenuVerticalpanel.Controls.Add(this.Cuadrebutton);
            this.MenuVerticalpanel.Controls.Add(this.button2);
            this.MenuVerticalpanel.Controls.Add(this.VentaDirectabutton);
            this.MenuVerticalpanel.Controls.Add(this.VentanaActiva1panel);
            this.MenuVerticalpanel.Controls.Add(this.Mesasbutton);
            this.MenuVerticalpanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.MenuVerticalpanel.Location = new System.Drawing.Point(0, 24);
            this.MenuVerticalpanel.Name = "MenuVerticalpanel";
            this.MenuVerticalpanel.Size = new System.Drawing.Size(200, 675);
            this.MenuVerticalpanel.TabIndex = 0;
            // 
            // VentanaActiva6panel
            // 
            this.VentanaActiva6panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(41)))), ((int)(((byte)(134)))));
            this.VentanaActiva6panel.Location = new System.Drawing.Point(3, 3);
            this.VentanaActiva6panel.Name = "VentanaActiva6panel";
            this.VentanaActiva6panel.Size = new System.Drawing.Size(10, 62);
            this.VentanaActiva6panel.TabIndex = 12;
            // 
            // VentanaActiva5panel
            // 
            this.VentanaActiva5panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(41)))), ((int)(((byte)(134)))));
            this.VentanaActiva5panel.Location = new System.Drawing.Point(3, 338);
            this.VentanaActiva5panel.Name = "VentanaActiva5panel";
            this.VentanaActiva5panel.Size = new System.Drawing.Size(10, 62);
            this.VentanaActiva5panel.TabIndex = 10;
            this.VentanaActiva5panel.Visible = false;
            // 
            // VentanaActiva4panel
            // 
            this.VentanaActiva4panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(41)))), ((int)(((byte)(134)))));
            this.VentanaActiva4panel.Location = new System.Drawing.Point(3, 271);
            this.VentanaActiva4panel.Name = "VentanaActiva4panel";
            this.VentanaActiva4panel.Size = new System.Drawing.Size(10, 62);
            this.VentanaActiva4panel.TabIndex = 9;
            this.VentanaActiva4panel.Visible = false;
            // 
            // VentanaActiva3panel
            // 
            this.VentanaActiva3panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(41)))), ((int)(((byte)(134)))));
            this.VentanaActiva3panel.Location = new System.Drawing.Point(3, 204);
            this.VentanaActiva3panel.Name = "VentanaActiva3panel";
            this.VentanaActiva3panel.Size = new System.Drawing.Size(10, 62);
            this.VentanaActiva3panel.TabIndex = 8;
            this.VentanaActiva3panel.Visible = false;
            // 
            // VentanaActiva2panel
            // 
            this.VentanaActiva2panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(41)))), ((int)(((byte)(134)))));
            this.VentanaActiva2panel.Location = new System.Drawing.Point(3, 137);
            this.VentanaActiva2panel.Name = "VentanaActiva2panel";
            this.VentanaActiva2panel.Size = new System.Drawing.Size(10, 62);
            this.VentanaActiva2panel.TabIndex = 7;
            this.VentanaActiva2panel.Visible = false;
            // 
            // VentanaActiva1panel
            // 
            this.VentanaActiva1panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(41)))), ((int)(((byte)(134)))));
            this.VentanaActiva1panel.Location = new System.Drawing.Point(3, 70);
            this.VentanaActiva1panel.Name = "VentanaActiva1panel";
            this.VentanaActiva1panel.Size = new System.Drawing.Size(10, 62);
            this.VentanaActiva1panel.TabIndex = 2;
            this.VentanaActiva1panel.Visible = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(41)))), ((int)(((byte)(134)))));
            this.panel2.Controls.Add(this.MenupictureBox);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(200, 24);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1057, 27);
            this.panel2.TabIndex = 1;
            // 
            // Contenderpanel
            // 
            this.Contenderpanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Contenderpanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Contenderpanel.Controls.Add(this.pictureBox1);
            this.Contenderpanel.Location = new System.Drawing.Point(200, 50);
            this.Contenderpanel.Name = "Contenderpanel";
            this.Contenderpanel.Size = new System.Drawing.Size(1045, 649);
            this.Contenderpanel.TabIndex = 2;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.UsuarioarchivoToolStripMenuItem,
            this.edicionToolStripMenuItem,
            this.inventarioToolStripMenuItem,
            this.configuracionesToolStripMenuItem,
            this.backupToolStripMenuItem,
            this.acercaDeToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1257, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // UsuarioarchivoToolStripMenuItem
            // 
            this.UsuarioarchivoToolStripMenuItem.Name = "UsuarioarchivoToolStripMenuItem";
            this.UsuarioarchivoToolStripMenuItem.Size = new System.Drawing.Size(64, 20);
            this.UsuarioarchivoToolStripMenuItem.Text = "Usuarios";
            this.UsuarioarchivoToolStripMenuItem.Click += new System.EventHandler(this.archivoToolStripMenuItem_Click);
            // 
            // edicionToolStripMenuItem
            // 
            this.edicionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clientesToolStripMenuItem,
            this.meserosToolStripMenuItem,
            this.productosToolStripMenuItem,
            this.proveedoresToolStripMenuItem});
            this.edicionToolStripMenuItem.Name = "edicionToolStripMenuItem";
            this.edicionToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.edicionToolStripMenuItem.Text = "Edicion";
            // 
            // clientesToolStripMenuItem
            // 
            this.clientesToolStripMenuItem.Name = "clientesToolStripMenuItem";
            this.clientesToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.clientesToolStripMenuItem.Text = "Clientes";
            this.clientesToolStripMenuItem.Click += new System.EventHandler(this.clientesToolStripMenuItem_Click);
            // 
            // meserosToolStripMenuItem
            // 
            this.meserosToolStripMenuItem.Name = "meserosToolStripMenuItem";
            this.meserosToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.meserosToolStripMenuItem.Text = "Meseros";
            this.meserosToolStripMenuItem.Click += new System.EventHandler(this.meserosToolStripMenuItem_Click);
            // 
            // productosToolStripMenuItem
            // 
            this.productosToolStripMenuItem.Name = "productosToolStripMenuItem";
            this.productosToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.productosToolStripMenuItem.Text = "Productos";
            this.productosToolStripMenuItem.Click += new System.EventHandler(this.productosToolStripMenuItem_Click);
            // 
            // proveedoresToolStripMenuItem
            // 
            this.proveedoresToolStripMenuItem.Name = "proveedoresToolStripMenuItem";
            this.proveedoresToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.proveedoresToolStripMenuItem.Text = "Proveedores";
            this.proveedoresToolStripMenuItem.Click += new System.EventHandler(this.proveedoresToolStripMenuItem_Click);
            // 
            // inventarioToolStripMenuItem
            // 
            this.inventarioToolStripMenuItem.Name = "inventarioToolStripMenuItem";
            this.inventarioToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.inventarioToolStripMenuItem.Text = "Inventario";
            this.inventarioToolStripMenuItem.Click += new System.EventHandler(this.inventarioToolStripMenuItem_Click);
            // 
            // configuracionesToolStripMenuItem
            // 
            this.configuracionesToolStripMenuItem.Name = "configuracionesToolStripMenuItem";
            this.configuracionesToolStripMenuItem.Size = new System.Drawing.Size(106, 20);
            this.configuracionesToolStripMenuItem.Text = "Configuraciones";
            this.configuracionesToolStripMenuItem.Click += new System.EventHandler(this.configuracionesToolStripMenuItem_Click);
            // 
            // backupToolStripMenuItem
            // 
            this.backupToolStripMenuItem.Name = "backupToolStripMenuItem";
            this.backupToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.backupToolStripMenuItem.Text = "BackUp";
            this.backupToolStripMenuItem.Click += new System.EventHandler(this.backupToolStripMenuItem_Click);
            // 
            // acercaDeToolStripMenuItem
            // 
            this.acercaDeToolStripMenuItem.Name = "acercaDeToolStripMenuItem";
            this.acercaDeToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.acercaDeToolStripMenuItem.Text = "Acerca De";
            this.acercaDeToolStripMenuItem.Click += new System.EventHandler(this.acercaDeToolStripMenuItem_Click);
            // 
            // MenupictureBox
            // 
            this.MenupictureBox.Image = ((System.Drawing.Image)(resources.GetObject("MenupictureBox.Image")));
            this.MenupictureBox.Location = new System.Drawing.Point(3, 0);
            this.MenupictureBox.Name = "MenupictureBox";
            this.MenupictureBox.Size = new System.Drawing.Size(35, 27);
            this.MenupictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.MenupictureBox.TabIndex = 0;
            this.MenupictureBox.TabStop = false;
            this.MenupictureBox.Click += new System.EventHandler(this.MenupictureBox_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(6, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1036, 643);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // Iniciobutton
            // 
            this.Iniciobutton.FlatAppearance.BorderSize = 0;
            this.Iniciobutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Iniciobutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Iniciobutton.ForeColor = System.Drawing.Color.White;
            this.Iniciobutton.Image = ((System.Drawing.Image)(resources.GetObject("Iniciobutton.Image")));
            this.Iniciobutton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Iniciobutton.Location = new System.Drawing.Point(12, 3);
            this.Iniciobutton.Name = "Iniciobutton";
            this.Iniciobutton.Size = new System.Drawing.Size(186, 62);
            this.Iniciobutton.TabIndex = 11;
            this.Iniciobutton.Text = "Inicio";
            this.Iniciobutton.UseVisualStyleBackColor = true;
            this.Iniciobutton.Click += new System.EventHandler(this.Iniciobutton_Click);
            // 
            // Salirbutton
            // 
            this.Salirbutton.FlatAppearance.BorderSize = 0;
            this.Salirbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Salirbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Salirbutton.ForeColor = System.Drawing.Color.White;
            this.Salirbutton.Image = ((System.Drawing.Image)(resources.GetObject("Salirbutton.Image")));
            this.Salirbutton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Salirbutton.Location = new System.Drawing.Point(12, 338);
            this.Salirbutton.Name = "Salirbutton";
            this.Salirbutton.Size = new System.Drawing.Size(186, 62);
            this.Salirbutton.TabIndex = 6;
            this.Salirbutton.Text = "Salir";
            this.Salirbutton.UseVisualStyleBackColor = true;
            this.Salirbutton.Click += new System.EventHandler(this.button4_Click);
            // 
            // Cuadrebutton
            // 
            this.Cuadrebutton.FlatAppearance.BorderSize = 0;
            this.Cuadrebutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cuadrebutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cuadrebutton.ForeColor = System.Drawing.Color.White;
            this.Cuadrebutton.Image = ((System.Drawing.Image)(resources.GetObject("Cuadrebutton.Image")));
            this.Cuadrebutton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Cuadrebutton.Location = new System.Drawing.Point(11, 271);
            this.Cuadrebutton.Name = "Cuadrebutton";
            this.Cuadrebutton.Size = new System.Drawing.Size(186, 62);
            this.Cuadrebutton.TabIndex = 5;
            this.Cuadrebutton.Text = "Cuadre";
            this.Cuadrebutton.UseVisualStyleBackColor = true;
            this.Cuadrebutton.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(11, 204);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(186, 62);
            this.button2.TabIndex = 4;
            this.button2.Text = "Delivery";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // VentaDirectabutton
            // 
            this.VentaDirectabutton.FlatAppearance.BorderSize = 0;
            this.VentaDirectabutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.VentaDirectabutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VentaDirectabutton.ForeColor = System.Drawing.Color.White;
            this.VentaDirectabutton.Image = ((System.Drawing.Image)(resources.GetObject("VentaDirectabutton.Image")));
            this.VentaDirectabutton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.VentaDirectabutton.Location = new System.Drawing.Point(11, 137);
            this.VentaDirectabutton.Name = "VentaDirectabutton";
            this.VentaDirectabutton.Size = new System.Drawing.Size(186, 62);
            this.VentaDirectabutton.TabIndex = 3;
            this.VentaDirectabutton.Text = "Venta ";
            this.VentaDirectabutton.UseVisualStyleBackColor = true;
            this.VentaDirectabutton.Click += new System.EventHandler(this.button1_Click);
            // 
            // Mesasbutton
            // 
            this.Mesasbutton.FlatAppearance.BorderSize = 0;
            this.Mesasbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Mesasbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Mesasbutton.ForeColor = System.Drawing.Color.White;
            this.Mesasbutton.Image = ((System.Drawing.Image)(resources.GetObject("Mesasbutton.Image")));
            this.Mesasbutton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Mesasbutton.Location = new System.Drawing.Point(12, 70);
            this.Mesasbutton.Name = "Mesasbutton";
            this.Mesasbutton.Size = new System.Drawing.Size(186, 62);
            this.Mesasbutton.TabIndex = 2;
            this.Mesasbutton.Text = "Mesas";
            this.Mesasbutton.UseVisualStyleBackColor = true;
            this.Mesasbutton.Click += new System.EventHandler(this.Mesasbutton_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(12, 626);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(39, 37);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 27;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1257, 699);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.Contenderpanel);
            this.Controls.Add(this.MenuVerticalpanel);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.MenuVerticalpanel.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.Contenderpanel.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MenupictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel MenuVerticalpanel;
        private System.Windows.Forms.Button Mesasbutton;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button Salirbutton;
        private System.Windows.Forms.Button Cuadrebutton;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button VentaDirectabutton;
        private System.Windows.Forms.Panel VentanaActiva1panel;
        public System.Windows.Forms.Panel Contenderpanel;
        private System.Windows.Forms.PictureBox MenupictureBox;
        private System.Windows.Forms.Panel VentanaActiva5panel;
        private System.Windows.Forms.Panel VentanaActiva4panel;
        private System.Windows.Forms.Panel VentanaActiva3panel;
        private System.Windows.Forms.Panel VentanaActiva2panel;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem edicionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem acercaDeToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem UsuarioarchivoToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem configuracionesToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem inventarioToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem backupToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel VentanaActiva6panel;
        private System.Windows.Forms.Button Iniciobutton;
        public System.Windows.Forms.ToolStripMenuItem clientesToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem meserosToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem productosToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem proveedoresToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}