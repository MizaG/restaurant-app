﻿namespace CodeRestaurant
{
    partial class RegistroInventario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ProductosdataGridView = new System.Windows.Forms.DataGridView();
            this.Listalabel = new System.Windows.Forms.Label();
            this.Agregarbutton = new System.Windows.Forms.Button();
            this.InventariodataGridView = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.Restarbutton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.ProductosdataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InventariodataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // ProductosdataGridView
            // 
            this.ProductosdataGridView.AllowUserToAddRows = false;
            this.ProductosdataGridView.AllowUserToDeleteRows = false;
            this.ProductosdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ProductosdataGridView.Location = new System.Drawing.Point(12, 96);
            this.ProductosdataGridView.Name = "ProductosdataGridView";
            this.ProductosdataGridView.ReadOnly = true;
            this.ProductosdataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ProductosdataGridView.Size = new System.Drawing.Size(506, 492);
            this.ProductosdataGridView.TabIndex = 0;
            // 
            // Listalabel
            // 
            this.Listalabel.AutoSize = true;
            this.Listalabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Listalabel.Location = new System.Drawing.Point(12, 63);
            this.Listalabel.Name = "Listalabel";
            this.Listalabel.Size = new System.Drawing.Size(212, 29);
            this.Listalabel.TabIndex = 37;
            this.Listalabel.Text = "Lista de Productos";
            // 
            // Agregarbutton
            // 
            this.Agregarbutton.BackColor = System.Drawing.Color.Green;
            this.Agregarbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Agregarbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Agregarbutton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Agregarbutton.Location = new System.Drawing.Point(356, 37);
            this.Agregarbutton.Name = "Agregarbutton";
            this.Agregarbutton.Size = new System.Drawing.Size(78, 58);
            this.Agregarbutton.TabIndex = 38;
            this.Agregarbutton.Text = "+\r\n";
            this.Agregarbutton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Agregarbutton.UseVisualStyleBackColor = false;
            this.Agregarbutton.Click += new System.EventHandler(this.Agregarbutton_Click);
            // 
            // InventariodataGridView
            // 
            this.InventariodataGridView.AllowUserToAddRows = false;
            this.InventariodataGridView.AllowUserToDeleteRows = false;
            this.InventariodataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.InventariodataGridView.Location = new System.Drawing.Point(535, 96);
            this.InventariodataGridView.Name = "InventariodataGridView";
            this.InventariodataGridView.ReadOnly = true;
            this.InventariodataGridView.Size = new System.Drawing.Size(478, 492);
            this.InventariodataGridView.TabIndex = 40;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(695, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 29);
            this.label1.TabIndex = 41;
            this.label1.Text = "Inventario";
            // 
            // Restarbutton
            // 
            this.Restarbutton.BackColor = System.Drawing.Color.Red;
            this.Restarbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Restarbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Restarbutton.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.Restarbutton.Location = new System.Drawing.Point(440, 37);
            this.Restarbutton.Name = "Restarbutton";
            this.Restarbutton.Size = new System.Drawing.Size(78, 58);
            this.Restarbutton.TabIndex = 42;
            this.Restarbutton.Text = "-";
            this.Restarbutton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Restarbutton.UseVisualStyleBackColor = false;
            this.Restarbutton.Click += new System.EventHandler(this.Restarbutton_Click);
            // 
            // RegistroInventario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1026, 600);
            this.Controls.Add(this.Restarbutton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.InventariodataGridView);
            this.Controls.Add(this.Agregarbutton);
            this.Controls.Add(this.Listalabel);
            this.Controls.Add(this.ProductosdataGridView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "RegistroInventario";
            this.Text = "RegistroInventario";
            this.Load += new System.EventHandler(this.RegistroInventario_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ProductosdataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InventariodataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView ProductosdataGridView;
        private System.Windows.Forms.Label Listalabel;
        private System.Windows.Forms.Button Agregarbutton;
        private System.Windows.Forms.DataGridView InventariodataGridView;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Restarbutton;
    }
}