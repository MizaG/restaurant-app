﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DAL;

namespace BLL
{
    public class Cajas : ClaseMaestra
    {
        public int CajaId { get; set; }
        public int UsuarioId { get; set; }
        public string Fecha { get; set; }
        public double Balance { get; set; }
        public int Estado { get; set; }

        public bool CerrarCaja()
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {// 0 = cerrada y 1 = Abierta
                retorno = conexion.Ejecutar(String.Format("update Cajas set Estado = 0 where CajaId = {0}", this.CajaId));

            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public bool ActualizarBalance()
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update Cajas set Balance = {0} where CajaId = {1}", this.Balance, this.CajaId));

            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Buscar(int IdBuscado)
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                DataTable dtCiudad = new DataTable();
                dtCiudad = conexion.ObtenerDatos(string.Format("select * from Cajas where CajaId = (select MAX(CajaId) from Cajas);", IdBuscado));
                if (dtCiudad.Rows.Count > 0)
                {
                    this.CajaId = Seguridad.ValidarIdEntero(dtCiudad.Rows[0]["CajaId"].ToString());
                    this.UsuarioId = Seguridad.ValidarIdEntero(dtCiudad.Rows[0]["UsuarioId"].ToString());
                    this.Fecha = dtCiudad.Rows[0]["Fecha"].ToString();
                    this.Balance = Seguridad.ValidarDouble(dtCiudad.Rows[0]["Balance"].ToString());
                    this.Estado = Seguridad.ValidarIdEntero(dtCiudad.Rows[0]["Estado"].ToString());
                    retorno = true;
                }
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Editar()
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update Cajas set UsuarioId = {0}, Fecha = getdate(), Balance = {1} where CajaId = {2}", this.UsuarioId, this.Balance,this.CajaId));

            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Eliminar()
        {
            throw new NotImplementedException();
        }

        public override bool Insertar()
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("insert into Cajas(UsuarioId,Fecha,Balance,Estado) values({0},getdate(),{1},1)", UsuarioId, Balance));
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override DataTable Listado(string Campos, string Condicion, string Orden)
        {
            ConexionDB con = new ConexionDB();
            string ordenFinal = "";

            if (!Orden.Equals(""))
                ordenFinal = " Order by " + Orden;
            return con.ObtenerDatos("select " + Campos + " from Cajas where " + Condicion + " " + ordenFinal);
        }
    }
}
