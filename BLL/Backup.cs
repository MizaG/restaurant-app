﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL
{
    public class Backup
    {
        public string Direccion { get; set; }
        public string Nombre { get; set; }
        public string Fecha { get; set; }
       

        public bool Crear()
        {
            ConexionDB conexion = new ConexionDB();
            try
            {
                //retur conexion.Ejecutar(@"BACKUP DATABASE [facturacionmf_db] TO  DISK = N'"+Direccion+"'\'"+Fecha+"-"+Nombre+ "' WITH NOFORMAT, NOINIT, NAME = N'facturacionmf_db-Completa Base de datos Copia de seguridad', SKIP, NOREWIND, NOUNLOAD,  STATS = 10");
                return conexion.Ejecutar(@"BACKUP DATABASE [coderestaurant_db] TO  DISK = N'" + this.Direccion + this.Fecha + this.Nombre+ "' WITH NOFORMAT, NOINIT, NAME = N'coderestaurant_db-Completa Base de datos Copia de seguridad', SKIP, NOREWIND, NOUNLOAD,  STATS = 10");
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
