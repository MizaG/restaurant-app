﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DAL;

namespace BLL
{
    public class Usuarios : ClaseMaestra
    {
        public int IdUsuario { get; set; }
        public string Nombre { get; set; }
        public string Contrasena { get; set; }
        public string FechaInicio { get; set; }
        public int TipoUsuario { get; set; }
        public int Activo { get; set; }
        ConexionDB conexion = new ConexionDB();

        public bool UpdateFecha()
        {
            bool retorno = false;

            try
            {
                retorno = conexion.Ejecutar(String.Format("update Usuarios set FechaInicio = GETDATE() where UsuarioId = {0}", this.IdUsuario));
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public bool InicioSesion()
        {
            DataTable dt = new DataTable();
            bool retorno = false;

            try
            {
                dt = conexion.ObtenerDatos(String.Format("SELECT UsuarioId,TipoUsuario from Usuarios where Nombre = '{0}' And Contrasena = '{1}'", this.Nombre, this.Contrasena));
                if (dt.Rows.Count > 0)
                {
                    this.IdUsuario = (int)dt.Rows[0]["UsuarioId"];
                    this.TipoUsuario = (int)dt.Rows[0]["TipoUsuario"];
                    retorno = true;
                }
                else
                {
                    retorno = false;
                }

            }catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Buscar(int IdBuscado)
        {
            DataTable bt = new DataTable();
            bool retorno = false;

            try
            {
                bt = conexion.ObtenerDatos(String.Format("SELECT * from Usuarios where UsuarioId = {0} AND Activo = 0", IdBuscado));
                this.Nombre = bt.Rows[0]["Nombre"].ToString();
                this.FechaInicio = bt.Rows[0]["FechaInicio"].ToString();
                this.Contrasena = bt.Rows[0]["Contrasena"].ToString();
                this.TipoUsuario = Seguridad.ValidarIdEntero(bt.Rows[0]["TipoUsuario"].ToString());
                retorno = true;
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Editar()
        {
            bool retorno = false;

            try
            {
                retorno = conexion.Ejecutar(String.Format("update Usuarios set Nombre = '{0}' , Contrasena='{1}', TipoUsuario = {2} where UsuarioId = {3}", this.Nombre, this.Contrasena, this.TipoUsuario, this.IdUsuario));
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Eliminar()
        {
            bool retorno = false;

            try
            {
                retorno = conexion.Ejecutar(String.Format("update Usuarios set Activo = 0  where UsuarioId = {0}", this.IdUsuario));
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Insertar()
        {
            bool retorno = false;

            try
            {
                retorno = conexion.Ejecutar(String.Format("Insert into Usuarios(Nombre,Contrasena,FechaInicio,TipoUsuario,Activo)" +
                    " Values('{0}','{1}',GETDATE(),{2},0)", this.Nombre, this.Contrasena, this.TipoUsuario));

            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override DataTable Listado(string Campos, string Condicion, string Orden)
        {
            ConexionDB conexion = new ConexionDB();
            string ordenFinal = "";

            if (!Orden.Equals(""))
                ordenFinal = " Order by " + Orden;
            return conexion.ObtenerDatos("select " + Campos + " from Usuarios  where " + Condicion + " and Activo = 0 " + ordenFinal);
        }
    }
}
