﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DAL;

namespace BLL
{
    public class Preventa : ClaseMaestra
    {
        public int PreventaId { get; set; }
        public int UsuarioId { get; set; }
        public int MesaId { get; set; }
        public int MeseroId { get; set; }
        public string NombreCliente { get; set; }
        public string NombreUsuario { get; set; }
        public string Fecha { get; set; }
        public int Activo { get; set; }
        public float Total { get; set; }
        public int ProductoId { get; set; }
        public int Cantidad { get; set; }
        public float ITBS { get; set; }
        public float Importe { get; set; }
        public List<Productos> producto { get; set; }
        public string Nota { get; set; }

        public Preventa()
        {
            producto = new List<Productos>();
        }

        public void AgregarProductos(int ProductoId, int Cantidad,double ITBIS, double Importe)
        {
            this.producto.Add(new Productos(ProductoId, Cantidad, Importe));
        }

        public void AgregarProductos(int ProductoId, string Nombre, double Precio, int Cantidad, double Importe)
        {
            this.producto.Add(new Productos(ProductoId, Nombre, Precio, Cantidad, Importe));
        }

        public void LimpiarList()
        {
            this.producto.Clear();
        }

        public int MaximoId()
        {
            ConexionDB conexion = new ConexionDB();
            return (int)conexion.ObtenerDatos(String.Format("select MAX(PreventaId) as PreventaId from Preventa")).Rows[0]["PreventaId"];
        }

        public override bool Buscar(int IdBuscado)
        {
            DataTable dtVentas = new DataTable();
            DataTable dtVentaProductos = new DataTable();
            ConexionDB conexion = new ConexionDB();
            bool retorno = false;

            try
            {
                dtVentas = conexion.ObtenerDatos(string.Format("select  pv.Preventaid,u.UsuarioId as UsuarioId, u.Nombre as NombreUsuario, pv.Fecha as Fecha, pv.Total, pv.Nota, pv.MeseroId from Preventa pv inner join Usuarios u on u.UsuarioId = pv.UsuarioId where pv.MesaId = {0} and pv.Activo = 0", IdBuscado));
                if (dtVentas.Rows.Count > 0)
                {
                    this.PreventaId = (int)dtVentas.Rows[0]["Preventaid"];
                    this.UsuarioId = (int)dtVentas.Rows[0]["UsuarioId"];
                    this.MeseroId = (int)dtVentas.Rows[0]["MeseroId"];
                    this.NombreUsuario = dtVentas.Rows[0]["NombreUsuario"].ToString();
                    this.Fecha = dtVentas.Rows[0]["Fecha"].ToString();
                    this.Total = Seguridad.ValidarFloat(dtVentas.Rows[0]["Total"].ToString());
                    this.Nota = dtVentas.Rows[0]["Nota"].ToString();

                    dtVentaProductos = conexion.ObtenerDatos(String.Format("select p.ProductoId, p.Nombre,p.Precio, pvp.Cantidad, pvp.Importe from PreventaProductos pvp inner join Productos p on pvp.ProductoId = p.ProductoId where pvp.PreventaId = {0}", this.PreventaId));

                    LimpiarList();
                    foreach (DataRow row in dtVentaProductos.Rows)
                    {
                        AgregarProductos((int)row["ProductoId"], (string)row["Nombre"], (double)row["Precio"], (int)row["Cantidad"], (double)row["Importe"]);
                    }
                    retorno = true;
                }
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Editar()
        {
            bool retorno = false;
            StringBuilder comando = new StringBuilder();
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update Preventa set NombreCliente = '{0}', Fecha = getdate(), Total = {2}, Nota = '{3}' where PreventaId = {4}", this.NombreCliente, this.Fecha,this.Total,this.Nota, this.PreventaId));
                if (retorno)
                {
                    retorno = conexion.Ejecutar(String.Format("delete from PreventaProductos where PreventaId = {0}", this.PreventaId));
                    foreach (var pro in producto)
                    {
                        comando.AppendLine(String.Format("insert into PreventaProductos(ProductoId,PreventaId,Cantidad,Importe) values({0},{1},{2},{3})", pro.ProductoId, this.PreventaId, pro.Cantidad, pro.Importe));
                    }

                    retorno = conexion.Ejecutar(comando.ToString());
                }
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Eliminar()
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar("update Preventa set Activo = 1 where PreventaId = " + this.PreventaId + ";");
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Insertar()
        {
            bool retorno = false;
            StringBuilder comando = new StringBuilder();
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("Insert into Preventa(UsuarioId, MesaId,MeseroId,NombreCliente, Fecha,Activo,Total,Nota) Values ({0},{1},{2},'{3}',getdate(),0,{4}, '{5}') ;",
                                                                                                                            this.UsuarioId, this.MesaId, this.MeseroId, this.NombreCliente, this.Total, this.Nota));
                if (retorno)
                {
                    this.PreventaId = (int)conexion.ObtenerDatos(String.Format("select MAX(PreventaId) as PreventaId from Preventa")).Rows[0]["PreventaId"];
                    foreach (var pro in producto)
                    {
                        comando.AppendLine(String.Format("insert into PreventaProductos(ProductoId,PreventaId,Cantidad,Importe) values({0},{1},{2},{3})", pro.ProductoId, this.PreventaId, pro.Cantidad,pro.Importe));
                    }
                }

                retorno = conexion.Ejecutar(comando.ToString());
            }
            catch (Exception e)
            {
                throw e;
                //retorno = false;
            }

            return retorno;
        }

        public override DataTable Listado(string Campos, string Condicion, string Orden)
        {
            ConexionDB con = new ConexionDB();
            string ordenFinal = "";

            if (!Orden.Equals(""))
                ordenFinal = " Order by " + Orden;
            return con.ObtenerDatos("select " + Campos + " from Preventa where Activo = 0 " + Condicion);
        }

        public bool UnirPreventa(int IdMesaPrincipal, int IdPreventaUnida)
        {
            bool retorno = false;
            StringBuilder comando = new StringBuilder();
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update PreventaProductos set PreventaId=(select PreventaId from Preventa where Activo = 0 and MesaId = {0}) where PreventaId = {1}", IdMesaPrincipal, IdPreventaUnida));
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public bool BuscarMesaId(int IdPreventa)
        {
            DataTable dtVentas = new DataTable();
            ConexionDB conexion = new ConexionDB();
            bool retorno = false;

            try
            {
                dtVentas = conexion.ObtenerDatos(string.Format("select MesaId from Preventa where PreventaId = {0} and Activo = 0", IdPreventa));
                if (dtVentas.Rows.Count > 0)
                {
                    this.MesaId = (int)dtVentas.Rows[0]["MesaId"];

                    retorno = true;
                }

            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }
    }
}
