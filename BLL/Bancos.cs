﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace BLL
{
    public enum Bancos
    {
        [Description("BancoPopular")]
        BancoPopular,
        [Description("Banreservas")]
        Banreservas,
        [Description("Banco BHD León")]
        BancoBHDLeón,
        [Description("Scotiabank")]
        Scotiabank,
        [Description("Banco Progreso")]
        BancoProgreso,
        [Description("Banco Central")]
        BancoCentral,
        [Description("Otros")]
        Otros       
    }
}
