﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Productos : ClaseMaestra
    {
        public int ProductoId { get; set; }
        public int TipoProductosId { get; set; }
        public int ProveedorId { get; set; }
        public int ProductoSecundarioId { get; set; }
        public string Imagen { get; set; }
        public string Categoria { get; set; }
        public string Nombre { get; set; }
        public string NombreIngrediente { get; set; }
        public double Precio { get; set; }
        public double Costo { get; set; }
        public int Activo { get; set; }
        public int Cantidad { get; set; }
        public double ITBIS { get; set; }
        public double Importe { get; set; }
        public bool Ingrediente { get; set; }
        public List<Productos> productos { get; set; }
        public int ClaseProductoId { get; set; }

        public Productos()
        {
            this.ProductoId = 0;
            this.TipoProductosId = 0;
            this.Nombre = "";
            this.Precio = 0.00;
            this.Costo = 0.00;
            this.Activo = 0;
            productos = new List<Productos>();
        }

        //llenar detalle ProductoIngrediente
        public Productos(int ProductoSecundarioId, int Cantidad)
        {
            this.ProductoSecundarioId = ProductoSecundarioId;
            this.Cantidad = Cantidad;
        }

        public Productos(int ProductoSecundarioId,string Nombre, int Cantidad)
        {
            this.ProductoSecundarioId = ProductoSecundarioId;
            this.NombreIngrediente = Nombre;
            this.Cantidad = Cantidad;
        }

        public void AgregarProductos(int ProductoSecundarioId, int Cantidad)
        {
            productos.Add(new Productos(ProductoSecundarioId, Cantidad));
        }
        //para buscar
        public void AgregarProductos(int ProductoSecundarioId,string Nombre, int Cantidad)
        {
            productos.Add(new Productos(ProductoSecundarioId,Nombre, Cantidad));
        }

        //llenar form de ventas...
        public Productos(int ProductoId, string Nombre, double Precio, int Cantidad, double Importe)
        {
            this.ProductoId = ProductoId;
            this.Nombre = Nombre;
            this.Precio = Precio;
            this.Cantidad = Cantidad;
            this.Importe = Importe;
        }
        //Ventas y Compras para insertar
        public Productos(int ProductoId, int Cantidad, double Importe)
        {
            this.ProductoId = ProductoId;
            this.Cantidad = Cantidad;
            this.Importe = Importe;
        }

        public void LimpiarList()
        {
            this.productos.Clear();
        }

        public override bool Buscar(int IdBuscado)
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            DataTable dtProductos = new DataTable();
            DataTable dtIngredientes = new DataTable();
            try
            {               
                dtProductos = conexion.ObtenerDatos(string.Format("select * from Productos where ProductoId = {0} and Activo = 0", IdBuscado));
                if (dtProductos.Rows.Count > 0)
                {
                    this.ProductoId = Seguridad.ValidarIdEntero(dtProductos.Rows[0]["ProductoId"].ToString());
                    this.Imagen = dtProductos.Rows[0]["Imagen"].ToString();
                    this.Nombre = dtProductos.Rows[0]["Nombre"].ToString();
                    this.Categoria = dtProductos.Rows[0]["Categoria"].ToString();
                    this.TipoProductosId = Seguridad.ValidarIdEntero(dtProductos.Rows[0]["TipoProductosId"].ToString());
                    this.Precio = Seguridad.ValidarDouble(dtProductos.Rows[0]["Precio"].ToString());
                    this.Costo = Seguridad.ValidarDouble(dtProductos.Rows[0]["Costo"].ToString());
                    this.ClaseProductoId = Seguridad.ValidarIdEntero(dtProductos.Rows[0]["ClaseProductoId"].ToString());

                    dtIngredientes = conexion.ObtenerDatos(String.Format("select proing.ProductoSecundarioId,p.Nombre,proing.Cantidad from Productos p,ProductoIngredientes proing where p.ProductoId = proing.ProductoId and p.ProductoId = {0}", IdBuscado));

                    LimpiarList();
                    foreach (DataRow row in dtIngredientes.Rows)
                    {
                        AgregarProductos((int)row["ProductoSecundarioId"], (string)row["Nombre"], (int)row["Cantidad"]);
                    }

                    retorno = true;
                }
            }
            catch (Exception e)
            {
                string error = e.Message;
                retorno = false;
            }

            return retorno;
        }

        public override bool Editar()
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update Productos set Nombre = '{0}', TipoProductosId = {1}, Precio = {2}, Costo = {3}, Imagen = '{4}', Categoria = '{5}' where ProductoId = {6}", this.Nombre, this.TipoProductosId, this.Precio, this.Costo,Imagen, Categoria, this.ProductoId));

            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Eliminar()
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update Productos set Activo = 1 where ProductoId = {0};", this.ProductoId));
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Insertar()
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            StringBuilder comando = new StringBuilder();
            try
            {
                retorno = conexion.Ejecutar(String.Format("insert into Productos(TipoProductosId,ProveedorId,Imagen,Nombre,Precio,Costo,Activo,Categoria,ClaseProductoId) values({0},{1},'{2}','{3}',{4},{5},0,'{6}',{7})",
                    this.TipoProductosId, this.ProveedorId, this.Imagen, this.Nombre, Precio, this.Costo,this.Categoria,this.ClaseProductoId));

                if (retorno && Ingrediente)
                {
                    this.ProductoId = (int)conexion.ObtenerDatos(String.Format("select MAX(ProductoId) as ProductoId from Productos")).Rows[0]["ProductoId"];
                    foreach (var pro in productos)
                    {
                        comando.AppendLine(String.Format("insert into ProductoIngredientes(ProductoId,ProductoSecundarioId,Cantidad) values({0},{1},{2})",this.ProductoId, pro.ProductoSecundarioId, pro.Cantidad));
                    }
                    retorno = conexion.Ejecutar(comando.ToString());
                }
            }
            catch (Exception e)
            {
                throw e;
                //retorno = false;
            }

            return retorno;
        }

        public override DataTable Listado(string Campos, string Condicion, string Orden)
        {
            ConexionDB conexion = new ConexionDB();
            string ordenFinal = "";

            if (!Orden.Equals(""))
                ordenFinal = " Order by " + Orden;
            return conexion.ObtenerDatos("select " + Campos + " from Productos p, TiposProductos tp  where p.TipoProductosId = tp.TipoProductosId and p.Activo = 0 and " + Condicion +" "+ ordenFinal);
        }

        public DataTable ListarClaseProd(string Campos, string Condicion)
        {
            ConexionDB conexion = new ConexionDB();
            
            return conexion.ObtenerDatos("select " + Campos + " from ClaseProductos where " + Condicion);
        }

        public DataTable BuscarIngredientes(string Campos, string Condicion)
        {
            ConexionDB conexion = new ConexionDB();

            return conexion.ObtenerDatos("select " + Campos + " from ProductoIngredientes where " + Condicion);
        }

    }
}
