﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DAL;

namespace BLL
{
    public class Tarjetas : ClaseMaestra
    {
        public int TarjetaId { get; set; }
        public int Banco { get; set; }
        public int Operador { get; set; }
        public string NoTarjeta { get; set; }
        public string Autorizacion { get; set; }

        public int MaximoId()
        {
            ConexionDB conexion = new ConexionDB();
            return (int)conexion.ObtenerDatos(String.Format("select MAX(TarjetaId) as TarjetaId from Tarjetas")).Rows[0]["TarjetaId"];
        }

        public override bool Buscar(int IdBuscado)
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                DataTable dtTarjetas = new DataTable();
                dtTarjetas = conexion.ObtenerDatos(string.Format("select * from Tarjetas where TarjetaId = {0};", IdBuscado));
                if (dtTarjetas.Rows.Count > 0)
                {
                    this.Banco = Seguridad.ValidarIdEntero(dtTarjetas.Rows[0]["Banco"].ToString());
                    this.Operador = Seguridad.ValidarIdEntero(dtTarjetas.Rows[0]["Operador"].ToString());
                    this.NoTarjeta = dtTarjetas.Rows[0]["NoTarjeta"].ToString();
                    this.Autorizacion = dtTarjetas.Rows[0]["Autorizacion"].ToString();
                    retorno = true;
                }
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Editar()
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update Tarjetas set Banco = {0},Operador = {1}, NoTarjeta = '{2}',Autorizacion = '{3}' where TarjetaId = {4}", this.Banco, this.Operador, this.NoTarjeta, this.Autorizacion, this.TarjetaId));

            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Eliminar()
        {
            throw new NotImplementedException();
        }

        public override bool Insertar()
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("insert into Tarjetas(Banco,Operador,NoTarjeta,Autorizacion) values({0},{1},'{2}','{3}')", this.Banco, this.Operador,this.NoTarjeta,this.Autorizacion));
                this.TarjetaId = (int)conexion.ObtenerDatos(String.Format("select MAX(TarjetaId) as TarjetaId from Tarjetas")).Rows[0]["TarjetaId"];
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override DataTable Listado(string Campos, string Condicion, string Orden)
        {
            throw new NotImplementedException();
        }
    }
}
