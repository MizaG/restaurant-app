﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Ventas : ClaseMaestra
    {
        public int VentaId { get; set; }
        public int UsuarioId { get; set; }
        public int MeseroId { get; set; }
        public int MesaId { get; set; }
        public int TarjetaId { get; set; }
        public string NombreUsuario { get; set; }
        public string NombreCliente { get; set; }
        public int ProductoId { get; set; }
        public int Cantidad { get; set; }
        public double Precio { get; set; }
        public string Fecha { get; set; }
        public string NCF { get; set; }
        public double TotalVenta { get; set; }
        public int Activo { get; set; }
        public int TipoVenta { get; set; }
        public int ValorRecibido { get; set; }
        public int Cambio { get; set; }
        public string Comentario { get; set; }
        public List<Productos> producto { get; set; }

        ConexionDB conexion = new ConexionDB();

        public Ventas()
        {
            this.VentaId = 0;
            this.UsuarioId = 0;
            this.TarjetaId = 0;
            this.Fecha = "";
            this.NCF = "";
            this.TotalVenta = 0.0;
            this.ProductoId = 0;
            this.Cantidad = 0;
            this.Precio = 0.0;
            this.Activo = 0;
            this.TipoVenta = 0;
            this.ValorRecibido = 0;
            this.Cambio = 0;
            this.Comentario = "";
            this.producto = new List<Productos>();
        }

        public Ventas(int Ventaid, int Usuarioid, string Fecha, string Nfc, double Totalventa, int ProductoId, int Cantidad, double Precio)
        {
            this.VentaId = Ventaid;
            this.UsuarioId = Usuarioid;
            this.Fecha = Fecha;
            this.NCF = Nfc;
            this.TotalVenta = Totalventa;
            this.ProductoId = ProductoId;
            this.Cantidad = Cantidad;
            this.Precio = Precio;
        }

        public void AgregarProductos(int ProductoId, int Cantidad, double Importe)
        {
            this.producto.Add(new Productos(ProductoId, Cantidad, Importe));
        }

        public void AgregarProductos(int ProductoId, string Nombre, double Precio, int Cantidad, double Importe)
        {
            this.producto.Add(new Productos(ProductoId, Nombre, Precio, Cantidad, Importe));
        }

        public void LimpiarList()
        {
            this.producto.Clear();
        }

        public DataTable TotalCredito(string Fecha, int UsuarioId)
        {
            ConexionDB conexion = new ConexionDB();
            return conexion.ObtenerDatos("select SUM(TotalVenta) as Total from Ventas where TipoVenta = 0 and Activo = 0 and Fecha >= '" + Fecha + "T00:00:00.000' and Fecha <='" + Fecha + "T23:59:59.999' and UsuarioId = " + UsuarioId + ";");
        }

        //public DataTable TotalTarjeta(string Fecha, int UsuarioId)
        //{
        //    ConexionDB conexion = new ConexionDB();
        //    return conexion.ObtenerDatos("select SUM(TotalVenta) as Total from Ventas where TipoVenta = 2 and Activo = 0 and Fecha >= '" + Fecha + "T00:00:00.000' and Fecha <='" + Fecha + "T23:59:59.999'and UsuarioId = " + UsuarioId + ";");
        //}

        public DataTable TotalDebito(string Fecha, int UsuarioId)
        {
            ConexionDB conexion = new ConexionDB();
            return conexion.ObtenerDatos("select SUM(TotalVenta) as Total from Ventas where TipoVenta = 1 and Activo = 0 and Fecha >= '" + Fecha + "T00:00:00.000' and Fecha <='" + Fecha + "T23:59:59.999'and UsuarioId = " + UsuarioId + ";");
        }

        public DataTable TotalVentas(string Fecha, int UsuarioId)
        {
            ConexionDB conexion = new ConexionDB();
            return conexion.ObtenerDatos("select SUM(TotalVenta) as Total from Ventas where Activo = 0 and Fecha >= '" + Fecha + "T00:00:00.000' and Fecha <='" + Fecha + "T23:59:59.999' and UsuarioId = " + UsuarioId + ";");
        }

        public int MaximoId()
        {
            ConexionDB conexion = new ConexionDB();
            return (int)conexion.ObtenerDatos(String.Format("select MAX(VentaId) as VentaId from Ventas")).Rows[0]["VentaId"];
        }

        public override bool Buscar(int IdBuscado)
        {
            DataTable dtVentas = new DataTable();
            DataTable dtVentaProductos = new DataTable();
            bool retorno = false;

            try
            {
                dtVentas = conexion.ObtenerDatos(string.Format("select u.UsuarioId as UsuarioId, u.Nombre as NombreUsuario, v.NombreCliente, v.NCF as NCF, v.Fecha as Fecha, v.TotalVenta as TotalVenta, v.TipoVenta as TipoVenta from Ventas v inner join Usuarios u " +
                    "on u.UsuarioId = v.UsuarioId where v.VentaId = {0} and v.Activo = 0", IdBuscado));
                if (dtVentas.Rows.Count > 0)
                {
                    this.UsuarioId = (int)dtVentas.Rows[0]["UsuarioId"];
                    this.NombreUsuario = dtVentas.Rows[0]["NombreUsuario"].ToString();
                    this.NombreCliente = dtVentas.Rows[0]["NombreCliente"].ToString();
                    this.Fecha = dtVentas.Rows[0]["Fecha"].ToString();
                    this.NCF = dtVentas.Rows[0]["NCF"].ToString();
                    this.TotalVenta = (double)dtVentas.Rows[0]["TotalVenta"];
                    this.TipoVenta = Seguridad.ValidarIdEntero(dtVentas.Rows[0]["TipoVenta"].ToString());

                    dtVentaProductos = conexion.ObtenerDatos(String.Format("select vd.ProductoId as ProductoId, p.Nombre as Nombre, p.Precio as Precio, vd.Cantidad as Cantidad, vd.Importe as Importe from VentasProductos vd inner join " +
                        "Ventas v on vd.VentaId = v.VentaId inner join Productos p on vd.ProductoId = p.ProductoId where v.VentaId =  {0}", IdBuscado));

                    LimpiarList();
                    foreach (DataRow row in dtVentaProductos.Rows)
                    {
                        AgregarProductos((int)row["ProductoId"], (string)row["Nombre"], (double)row["Precio"], (int)row["Cantidad"], (double)row["Importe"]);
                    }
                    retorno = true;
                }
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Editar()
        {
            bool retorno = false;
            StringBuilder comando = new StringBuilder();

            try
            {
                retorno = conexion.Ejecutar(String.Format("update Ventas set NombreCliente = '{0}', Fecha = '{1}', NCF = '{2}', TotalVenta = {3} where VentaId = {4}", this.NombreCliente, this.Fecha, this.NCF, this.TotalVenta, this.VentaId));
                if (retorno)
                {
                    retorno = conexion.Ejecutar(String.Format("delete from VentasProductos where VentaId = {0}", this.VentaId));
                    foreach (var pro in producto)
                    {
                        comando.AppendLine(String.Format("insert into VentasProductos(ProductoId,VentaId,Cantidad,Importe) values({0},{1},{2},{3})", pro.ProductoId, this.VentaId, pro.Cantidad, pro.Importe));
                    }

                    retorno = conexion.Ejecutar(comando.ToString());
                }
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Eliminar()
        {
            bool retorno = false;

            try
            {
                retorno = conexion.Ejecutar("update Ventas set Activo = 1 where VentaId = " + this.VentaId + ";");
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Insertar()
        {
            bool retorno = false;
            StringBuilder comando = new StringBuilder();
            try
            {
                retorno = conexion.Ejecutar(String.Format("Insert into Ventas (UsuarioId,MeseroId,MesaId,TarjetaId,NombreCliente, Fecha, NCF,Activo,TipoVenta,ValorRecibido,Cambio,Comentario,TotalVenta)"+
                    "Values ({0},{1},{2},{3},'{4}',GETDATE(),'{6}',0,{7},{8},{9},'{10}',{11});",
                                            this.UsuarioId,this.MeseroId,this.MesaId,this.TarjetaId, this.NombreCliente, this.Fecha, this.NCF, this.TipoVenta,this.ValorRecibido,this.Cambio,this.Comentario,this.TotalVenta));
                if (retorno)
                {
                    this.VentaId = (int)conexion.ObtenerDatos(String.Format("select MAX(VentaId) as VentaId from Ventas")).Rows[0]["VentaId"];
                    foreach (var pro in producto)
                    {
                        comando.AppendLine(String.Format("insert into VentasProductos(ProductoId,VentaId,Cantidad,Importe) values({0},{1},{2},{3})", pro.ProductoId, this.VentaId, pro.Cantidad, pro.Importe));
                    }
                }

                retorno = conexion.Ejecutar(comando.ToString());
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public DataTable Listado(string Condicion)
        {
            ConexionDB conexion = new ConexionDB();
            return conexion.ObtenerDatos("select v.VentaId, v.NombreCliente, u.Nombre as Usuario, v.Fecha, v.TotalVenta as Total from Ventas v inner join Usuarios u on u.UsuarioId = v.UsuarioId where " + Condicion + " and v.Activo = 0;");
        }

        public DataSet ListadoDataSet(string Campos, string Condicion, string Orden)
        {
            ConexionDB con = new ConexionDB();
            string ordenFinal = "";

            if (!Orden.Equals(""))
                ordenFinal = " Order by " + Orden;
            return con.ObtenerDatosDataSet("select NombreCliente, TotalVenta, CASE WHEN TipoVenta = 0 THEN 'Credito' ELSE 'Debito' END as TipoVenta from Ventas where Activo = 0 " + Condicion);
        }

        public override DataTable Listado(string Campos, string Condicion, string Orden)
        {
            ConexionDB con = new ConexionDB();
            string ordenFinal = "";

            if (!Orden.Equals(""))
                ordenFinal = " Order by " + Orden;
            return con.ObtenerDatos("select NombreCliente, TotalVenta, CASE WHEN TipoVenta = 0 THEN 'Credito' ELSE 'Debito' END as TipoVenta from Ventas where Activo = 0 " + Condicion);
        }
    }
}
