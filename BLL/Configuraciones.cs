﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL
{
    public class Configuraciones : ClaseMaestra
    {
        public int ConfiguracionId { get; set; }
        public string NCF { get; set; }
        public string RNC { get; set; }
        public double ITBIS { get; set; }
        public string RutaBackup { get; set; }
        public string RutaFoto { get; set; }

        public Configuraciones()
        {
            NCF = "";
            RNC = "";
            ITBIS = 0.0;
            RutaBackup = "";
            RutaFoto = "";
        }

        public override bool Buscar(int IdBuscado)
        {

            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                DataTable dtConfiguracion = new DataTable();
                dtConfiguracion = conexion.ObtenerDatos(string.Format("select * from Configuraciones where ConfiguracionId = {0}", IdBuscado));
                if (dtConfiguracion.Rows.Count > 0)
                {
                    this.NCF = dtConfiguracion.Rows[0]["NCF"].ToString();
                    this.RNC = dtConfiguracion.Rows[0]["RNC"].ToString();
                    this.ITBIS = Seguridad.ValidarDouble(dtConfiguracion.Rows[0]["ITBIS"].ToString());
                    this.RutaBackup = dtConfiguracion.Rows[0]["RutaBackup"].ToString();
                    this.RutaFoto = dtConfiguracion.Rows[0]["RutaFoto"].ToString();
                    retorno = true;
                }
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Editar()
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update Configuraciones set NCF = '{0}', RutaBackup = '{1}', RutaFoto = '{2}', RNC = '{3}', ITBIS = {4} where ConfiguracionId = {5}", this.NCF, this.RutaBackup, this.RutaFoto,this.RNC,this.ITBIS, this.ConfiguracionId));

            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Eliminar()
        {
            throw new NotImplementedException();
        }

        public override bool Insertar()
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("insert into Configuraciones(NCF,RNC,ITBIS,RutaBackup,RutaFoto) values('{0}','{1}',{2},'{3}','{4}')", this.NCF,this.RNC,this.ITBIS, this.RutaBackup, this.RutaFoto));
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override DataTable Listado(string Campos, string Condicion, string Orden)
        {
            throw new NotImplementedException();
        }
    }
}
