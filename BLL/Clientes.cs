﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Clientes : ClaseMaestra
    {
        public int ClienteId { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public int Activo { get; set; }

        public int MaximoId()
        {
            ConexionDB conexion = new ConexionDB();
            return Seguridad.ValidarIdEntero(conexion.ObtenerDatos(String.Format("select MAX(ClienteId) as ClienteId from Clientes")).Rows[0]["ClienteId"].ToString());
        }

        public override bool Buscar(int IdBuscado)
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();

            try
            {
                DataTable dtClientes = new DataTable();
                dtClientes = conexion.ObtenerDatos(string.Format("select * from Clientes where ClienteId = {0} and Activo = 0", IdBuscado));
                if (dtClientes.Rows.Count > 0)
                {
                    this.Nombre = dtClientes.Rows[0]["Nombre"].ToString();
                    this.Direccion = dtClientes.Rows[0]["Direccion"].ToString();
                    this.Telefono = dtClientes.Rows[0]["Telefono"].ToString();
                    retorno = true;
                }
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Editar()
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update Clientes set Nombre = '{0}', Direccion = '{1}', Telefono = '{2}' where ClienteId = {3}", this.Nombre, this.Direccion, this.Telefono, this.ClienteId));
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Eliminar()
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update Clientes set Activo = 1 where ClienteId = {0};", this.ClienteId));
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Insertar()
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("insert into Clientes(Nombre,Direccion,Telefono,activo) values('{0}','{1}','{2}',0)", this.Nombre, this.Direccion, this.Telefono));
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return retorno;
        }

        public override DataTable Listado(string Campos, string Condicion, string Orden)
        {
            ConexionDB con = new ConexionDB();
            string ordenFinal = "";

            if (!Orden.Equals(""))
                ordenFinal = " Order by " + Orden;
            return con.ObtenerDatos("select " + Campos + " from Clientes where " + Condicion + " " + ordenFinal);
        }
    }
}
