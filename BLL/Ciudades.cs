﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BLL
{
    public class Ciudades : ClaseMaestra
    {
        public int CiudadId { get; set; }
        public string Nombre { get; set; }
        public int Activo { get; set; }

        public Ciudades()
        {
            this.CiudadId = 0;
            this.Nombre = "";
            this.Activo = 0;
        }

        public override bool Buscar(int IdBuscado)
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                DataTable dtCiudad = new DataTable();
                dtCiudad = conexion.ObtenerDatos(string.Format("select * from Ciudades where CiudadId = {0} and activo = 0;", IdBuscado));
                if (dtCiudad.Rows.Count > 0)
                {
                    this.Nombre = dtCiudad.Rows[0]["Nombre"].ToString();
                    retorno = true;
                }
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Editar()
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update Ciudades set Nombre = '{0}' where CiudadId = {1}", this.Nombre, this.CiudadId));

            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Eliminar()
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update Ciudades set Activo = 1 where CiudadId = {0};", this.CiudadId));
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Insertar()
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("insert into Ciudades(Nombre,Activo) values('{0}',0)", this.Nombre));
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override DataTable Listado(string Campos, string Condicion, string Orden)
        {
            ConexionDB con = new ConexionDB();
            string ordenFinal = "";

            if (!Orden.Equals(""))
                ordenFinal = " Order by " + Orden;
            return con.ObtenerDatos("select " + Campos + " from Ciudades where " + Condicion + " " + ordenFinal);
        }
    }
}