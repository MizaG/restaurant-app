﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DAL;

namespace BLL
{
    public class Meseros : ClaseMaestra
    {
        public int MeseroId { get; set; }
        public string Nombre { get; set; }
        public int Activo { get; set; }

        public Meseros()
        {
            this.MeseroId = 0;
            this.Nombre = "";
            this.Activo = 0;
        }

        public override bool Buscar(int IdBuscado)
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                DataTable dtMeseros = new DataTable();
                dtMeseros = conexion.ObtenerDatos(string.Format("select * from Meseros where MeseroId = {0} and Activo = 0;", IdBuscado));
                if (dtMeseros.Rows.Count > 0)
                {
                    this.Nombre = dtMeseros.Rows[0]["Nombre"].ToString();
                    retorno = true;
                }
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Editar()
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update Meseros set Nombre = '{0}' where MeseroId = {1}", this.Nombre, this.MeseroId));

            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Eliminar()
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update Meseros set Activo = 1 where MeseroId = {0};", this.MeseroId));
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Insertar()
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("insert into Meseros(Nombre,Activo) values('{0}',0)", this.Nombre));
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override DataTable Listado(string Campos, string Condicion, string Orden)
        {
            ConexionDB con = new ConexionDB();
            string ordenFinal = "";

            if (!Orden.Equals(""))
                ordenFinal = " Order by " + Orden;
            return con.ObtenerDatos("select " + Campos + " from Meseros where " + Condicion + " " + ordenFinal);
        }
    }
}
