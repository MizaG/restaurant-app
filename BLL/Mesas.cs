﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DAL;

namespace BLL
{
    public class Mesas : ClaseMaestra
    {
        public int MesaId { get; set; }
        public int Numero { get; set; }
        public int Activo { get; set; }

        public Mesas()
        {
            this.MesaId = 0;
            this.Numero = 0;
            this.Activo = 0;
        }

        public bool Liberar()
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update Mesas set Activo = 0 where MesaId = {0};", this.MesaId));
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Buscar(int IdBuscado)
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                DataTable dtCiudad = new DataTable();
                dtCiudad = conexion.ObtenerDatos(string.Format("select * from Mesas where MesaId = {0} and Activo = 0;", IdBuscado));
                if (dtCiudad.Rows.Count > 0)
                {
                    this.Numero = Seguridad.ValidarIdEntero(dtCiudad.Rows[0]["Numero"].ToString());
                    retorno = true;
                }
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Editar()
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update Mesas set Numero = {0} where MesaId = {1}", this.Numero, this.MesaId));

            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Eliminar()
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update Mesas set Activo = 1 where MesaId = {0};", this.MesaId));
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Insertar()
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("insert into Mesas(Numero,Activo) values({0},0)", this.Numero));
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override DataTable Listado(string Campos, string Condicion, string Orden)
        {
            ConexionDB con = new ConexionDB();
            string ordenFinal = "";

            if (!Orden.Equals(""))
                ordenFinal = " Order by " + Orden;
            return con.ObtenerDatos("select " + Campos + " from Mesas where " + Condicion + " " + ordenFinal);
        }
    }
}
