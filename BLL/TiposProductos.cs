﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class TiposProductos : ClaseMaestra
    {
        public int TipoProductosId { get; set; }
        public string Nombre { get; set; }

        public TiposProductos()
        {
            this.TipoProductosId = 0;
            this.Nombre = "";
        }

        public override bool Buscar(int IdBuscado)
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                DataTable dtTipoProductos = new DataTable();
                dtTipoProductos = conexion.ObtenerDatos(string.Format("select * from TiposProductos where TipoProductosId = {0}", IdBuscado));
                if (dtTipoProductos.Rows.Count > 0)
                {
                    this.Nombre = dtTipoProductos.Rows[0]["Nombre"].ToString();
                    retorno = true;
                }
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Editar()
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update TiposProductos set Nombre = '{0}' where TipoProductosId = {1}", this.Nombre, this.TipoProductosId));

            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Eliminar()
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("delete from TiposProductos where TipoProductosId = {0};", this.TipoProductosId));
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Insertar()
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("insert into TiposProductos(Nombre) values('{0}')", this.Nombre));
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override DataTable Listado(string Campos, string Condicion, string Orden)
        {
            ConexionDB con = new ConexionDB();
            string ordenFinal = "";

            if (!Orden.Equals(""))
                ordenFinal = " Order by " + Orden;
            return con.ObtenerDatos("select " + Campos + " from TiposProductos where " + Condicion + " " + ordenFinal);
        }
    }
}
