﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DAL;

namespace BLL
{
    public class ClientePedidos : ClaseMaestra
    {
        public int ClientePedidoId { get; set; }
        public int ClienteId { get; set; }
        public int UsuarioId { get; set; }
        public string Fecha { get; set; }
        public double TotalVenta { get; set; }
        public int Activo { get; set; }
        public string NombreCliente { get; set; }
        public string NombreUsuario { get; set; }
        public int ProductoId { get; set; }
        public int Cantidad { get; set; }
        public double ITBS { get; set; }
        public double Importe { get; set; }
        public List<Productos> producto { get; set; }

        public void AgregarProductos(int ProductoId, int Cantidad, double Importe)
        {
            this.producto.Add(new Productos(ProductoId, Cantidad, Importe));
        }

        public void AgregarProductos(int ProductoId, string Nombre, double Precio, int Cantidad, double Importe)
        {
            this.producto.Add(new Productos(ProductoId, Nombre, Precio, Cantidad, Importe));
        }

        public void LimpiarList()
        {
            this.producto.Clear();
        }

        public int MaximoId()
        {
            ConexionDB conexion = new ConexionDB();
            return (int)conexion.ObtenerDatos(String.Format("select MAX(ClientePedidoId) as ClientePedidoId from ClientePedidos")).Rows[0]["ClientePedidoId"];
        }

        public override bool Buscar(int IdBuscado)
        {
            DataTable dtPedidos = new DataTable();
            DataTable dtPedidosProductos = new DataTable();
            ConexionDB conexion = new ConexionDB();
            bool retorno = false;

            try
            {
                dtPedidos = conexion.ObtenerDatos(string.Format("", IdBuscado));
                if (dtPedidos.Rows.Count > 0)
                {
                    this.ClienteId =  Seguridad.ValidarIdEntero(dtPedidos.Rows[0]["ClienteId"].ToString());
                    this.Fecha = dtPedidos.Rows[0]["Fecha"].ToString();
                    this.NombreUsuario = dtPedidos.Rows[0]["NombreUsuario"].ToString();
                    this.ClienteId = (int)dtPedidos.Rows[0]["ClienteId"];
                    this.NombreCliente = dtPedidos.Rows[0]["NombreCliente"].ToString();                    
                    this.Activo = Seguridad.ValidarIdEntero(dtPedidos.Rows[0]["Activo"].ToString());
                    this.TotalVenta = Seguridad.ValidarDouble(dtPedidos.Rows[0]["TotalVenta"].ToString());

                    dtPedidosProductos = conexion.ObtenerDatos(String.Format("", IdBuscado));

                    LimpiarList();
                    foreach (DataRow row in dtPedidosProductos.Rows)
                    {
                        AgregarProductos((int)row["ProductoId"], (string)row["Nombre"], (double)row["Precio"], (int)row["Cantidad"], (double)row["Importe"]);
                    }
                    retorno = true;
                }
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Editar()
        {
            bool retorno = false;
            StringBuilder comando = new StringBuilder();
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update ClientePedidos set ClienteId = {0}, TotalVenta = {1} where ClientePedidoId = {2}", this.ClienteId, this.TotalVenta, this.ClientePedidoId));
                if (retorno)
                {
                    retorno = conexion.Ejecutar(String.Format("delete from ClientePedidosProductos where ClientePedidoId = {0}", this.ClientePedidoId));
                    foreach (var pro in producto)
                    {
                        comando.AppendLine(String.Format("insert into ClientePedidosProductos(ClientePedidoId,ProductoId,Cantidad,ITBIS,Importe) values({0},{1},{2},{3},{4})",this.ClientePedidoId, pro.ProductoId, pro.Cantidad,pro.ITBIS, pro.Importe));
                    }

                    retorno = conexion.Ejecutar(comando.ToString());
                }
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Eliminar()
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar("update ClientePedidos set Activo = 1 where ClientePedidoId = " + this.ClientePedidoId + ";");
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Insertar()
        {
            bool retorno = false;
            StringBuilder comando = new StringBuilder();
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("Insert into ClientePedidos(ClienteId,UsuarioId, Fecha, TotalVenta, Activo) Values ({0},{1},getdate(),{2},0);",
                                            this.ClienteId, this.UsuarioId, this.Fecha, this.TotalVenta, this.Activo));
                if (retorno)
                {
                    this.ClientePedidoId = (int)conexion.ObtenerDatos(String.Format("select MAX(ClientePedidoId) as ClientePedidoId from ClientePedidos")).Rows[0]["ClientePedidoId"];
                    foreach (var pro in producto)
                    {
                        comando.AppendLine(String.Format("insert into ClientePedidosProductos(ClientePedidoId,ProductoId,Cantidad,ITBIS,Importe) values({0},{1},{2},{3},{4})", this.ClientePedidoId, pro.ProductoId, pro.Cantidad, pro.ITBIS, pro.Importe));
                    }
                }

                retorno = conexion.Ejecutar(comando.ToString());
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override DataTable Listado(string Campos, string Condicion, string Orden)
        {
            ConexionDB con = new ConexionDB();
            string ordenFinal = "";

            if (!Orden.Equals(""))
                ordenFinal = " Order by " + Orden;
            return con.ObtenerDatos("" + Condicion);
        }
    }
}
