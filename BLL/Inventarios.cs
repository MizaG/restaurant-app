﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL
{
    public class Inventarios : ClaseMaestra
    {
        public int InventarioId { get; set; }
        public int ProductoId { get; set; }
        public int Cantidad { get; set; }
        public string Fecha { get; set; }
        public string Nombre { get; set; }


        public bool RestarCantidad(int Cantidad, int ProductoId)
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update Inventario set Cantidad = {0} where ProductoId = {1}", Cantidad, ProductoId));

            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Buscar(int IdBuscado)
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                DataTable dtInventario = new DataTable();
                dtInventario = conexion.ObtenerDatos(string.Format("select p.Nombre, i.Cantidad, i.Fecha from Inventario i, Productos p where i.ProductoId = p.ProductoId and i.ProductoId = {0}", IdBuscado));
                if (dtInventario.Rows.Count > 0)
                {
                    this.Nombre = dtInventario.Rows[0]["Nombre"].ToString();
                    this.Cantidad = Seguridad.ValidarIdEntero(dtInventario.Rows[0]["Cantidad"].ToString());
                    this.Fecha = dtInventario.Rows[0]["Fecha"].ToString();
                    retorno = true;
                }
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Editar()
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update Inventario set Cantidad = {0}, Fecha = getdate() where ProductoId = {2}", this.Cantidad, this.Fecha, this.ProductoId));

            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Eliminar()
        {
            throw new NotImplementedException();
        }

        public override bool Insertar()
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("insert into Inventario(ProductoId,Cantidad,Fecha) values({0},{1},getdate())", this.ProductoId, this.Cantidad, this.Fecha));
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override DataTable Listado(string Campos, string Condicion, string Orden)
        {
            ConexionDB con = new ConexionDB();
            string ordenFinal = "";

            if (!Orden.Equals(""))
                ordenFinal = " Order by " + Orden;
            return con.ObtenerDatos("select " + Campos + " from Inventario i, Productos p where " + Condicion + " " + ordenFinal);
        }
    }
}
