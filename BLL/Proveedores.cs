﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using BLL;

namespace BLL
{
    public class Proveedores : ClaseMaestra
    {
        ConexionDB conexion = new ConexionDB();

        public int ProveedorId { get; set; }
        public int CiudadId { get; set; }
        public string Ciudad { get; set; }
        public string NombreRepresentante { get; set; }
        public string NombreEmpresa { get; set; }
        public string RNC { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string Celular { get; set; }
        public string Correo { get; set; }

        public Proveedores()
        {
            this.ProveedorId = 0;
            this.CiudadId = 0;
            this.NombreEmpresa = "";
            this.NombreRepresentante = "";
            this.RNC = "";
            this.Direccion = "";
            this.Telefono = "";
            this.Celular = "";
            this.Correo = "";
        }

        public override bool Buscar(int IdBuscado)
        {
            DataTable dtProveedor = new DataTable();
            bool retorno = false;

            try
            {
                dtProveedor = conexion.ObtenerDatos(String.Format("select * from Proveedores p, Ciudades c where c.CiudadId = p.CiudadId AND p.ProveedorId = {0}",IdBuscado));
                this.Ciudad = dtProveedor.Rows[0]["Nombre"].ToString();
                this.NombreEmpresa = dtProveedor.Rows[0]["NombreEmpresa"].ToString();
                this.NombreRepresentante = dtProveedor.Rows[0]["NombreRepresentante"].ToString();
                this.RNC = dtProveedor.Rows[0]["RNC"].ToString();
                this.Direccion = dtProveedor.Rows[0]["Direccion"].ToString();
                this.Telefono = dtProveedor.Rows[0]["Telefono"].ToString();
                this.Celular = dtProveedor.Rows[0]["Celular"].ToString();
                this.Correo = dtProveedor.Rows[0]["Correo"].ToString();

                retorno = true;
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Editar()
        {
            bool retorno = false;
            try
            {
                retorno = conexion.Ejecutar(String.Format("update Proveedores set CiudadId={0}, NombreEmpresa='{1}' ,NombreRepresentante='{2}', RNC='{3}', Direccion='{4}', Telefono='{5}', Celular='{6}' ,Correo='{7}' where ProveedorId={8}",
                                                this.CiudadId,this.NombreEmpresa,this.NombreRepresentante,this.RNC,this.Direccion,this.Telefono,this.Celular,this.Correo, this.ProveedorId));
                
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Eliminar()
        {
            bool retorno = false;

            try
            {
                retorno = conexion.Ejecutar(String.Format("UPDATE Proveedores set Activo = 1 where ProveedorId ={0};",this.ProveedorId));
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override bool Insertar()
        {
            bool retorno = false;

            try
            {
                retorno = conexion.Ejecutar(String.Format("insert into Proveedores(CiudadId,NombreEmpresa,NombreRepresentante,RNC,Direccion,Telefono,Celular,Correo,Activo) values({0},'{1}','{2}','{3}','{4}','{5}','{6}','{7}',0)",
                                                this.CiudadId, this.NombreEmpresa, this.NombreRepresentante, this.RNC, this.Direccion, this.Telefono, this.Celular, this.Correo));
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public override DataTable Listado(string Campos, string Condicion, string Orden)
        {
            ConexionDB con = new ConexionDB();
            string ordenFinal = "";

            if (!Orden.Equals(""))
                ordenFinal = " Order by " + Orden;
            return con.ObtenerDatos("select " + Campos + " from Proveedores where " + Condicion + " " + ordenFinal);
        }


    }
}
