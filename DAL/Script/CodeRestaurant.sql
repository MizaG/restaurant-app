﻿create database coderestaurant_db;
go
use coderestaurant_db;
go
create table Usuarios(
UsuarioId int identity(1,1) primary key,
Nombre varchar(40),
Contrasena Varchar(200),
TipoUsuario int,
FechaInicio datetime,
Activo int
);
go
create table Cajas(
CajaId int identity(1,1) primary key,
UsuarioId int References Usuarios(UsuarioId),
Fecha datetime,
Balance float,
Estado int
);
go
create table Ciudades(
CiudadId int primary key identity(1,1),
Nombre varchar(100),
Activo int
);
go
create table Meseros(
MeseroId int primary key identity(1,1),
Nombre varchar(100),
Activo int
);
go
create table Clientes(
ClienteId int primary key identity(1,1),
Nombre varchar(50),
Direccion varchar(100),
Telefono varchar(14),
Activo int
);
go
create table TiposProductos(
TipoProductosId int primary key identity(1,1),
Nombre varchar(100)
);
go
create table Proveedores(
ProveedorId int primary key identity(1,1),
CiudadId int References Ciudades(CiudadId),
NombreEmpresa varchar(50),
NombreRepresentante varchar(50),
RNC varchar(15),
Direccion varchar(100),
Telefono varchar(14),
Celular varchar(14),
Correo varchar(100),
Activo int
);
go
create table Productos(
ProductoId int primary key identity(1,1),
TipoProductosId int References TiposProductos(TipoProductosId),
ProveedorId int References Proveedores(ProveedorId),
Imagen varchar(250),
Nombre varchar(50),
Categoria varchar(40),
Precio float,
Costo float,
Activo int
);
go
create table ClientePedidos(
ClientePedidoId int primary key identity(1,1),
ClienteId int References Clientes(ClienteId),
UsuarioId int References Usuarios(UsuarioId),
Fecha datetime,
TotalVenta float,
Activo int
);
go
create table ClientePedidosProductos(
ClientePedidosProductoId int primary key identity(1,1),
ClientePedidoId int References ClientePedidos(ClientePedidoId),
ProductoId int References Productos(ProductoId),
Cantidad int,
ITBIS float,
Importe float
);
go
create table Mesas(
MesaId int primary key identity(1,1),
Numero int,
Activo int
);
go
create table Preventa(
PreventaId int primary key identity(1,1),
UsuarioId int References Usuarios(UsuarioId),
MesaId int References Mesas(MesaId),
MeseroId int References Meseros(MeseroId),
NombreCliente varchar(20),
Fecha datetime,
Activo int,
Total float,
Nota varchar(150)
);
go-- quite el itbis
create table PreventaProductos(
PreventaProductoId int primary key identity(1,1),
ProductoId int References Productos(ProductoId),
PreventaId int References Preventa(PreventaId),
Cantidad int,
Importe float
);
go--<<actualizadas
create table Tarjetas(
TarjetaId int primary key identity(1,1),
Banco int,
Operador int,
NoTarjeta varchar(20),
Autorizacion varchar(6)
);
go
create table Ventas(
VentaId int primary key identity(1,1),
UsuarioId int References Usuarios(UsuarioId),
MeseroId int References Meseros(MeseroId),
MesaId int,
TarjetaId int,
NombreCliente varchar(20),
Fecha datetime,
NCF varchar(50),
Activo int,
TipoVenta int,
ValorRecibido int,
Cambio int,
Comentario varchar(150),
TotalVenta float
);
go-->>
create table VentasProductos(
VentaDetalleId int primary key identity(1,1),
ProductoId int References Productos(ProductoId),
VentaId int References Ventas(VentaId),
Cantidad int,
Importe float
);
go
create table Compras(
CompraId int primary key identity(1,1),
ProveedorId int References Proveedores(ProveedorId),
UsuarioId int References Usuarios(UsuarioId),
ITBIS float,
NCF varchar(50),
Fecha datetime,
Total float
);
go
create table ComprasProductos(
CompraDetalleId int primary key identity(1,1),
CompraId int References Compras(CompraId),
ProductoId int References Productos(ProductoId),
Cantidad int,
SubTotal float,
);
go
create table Configuraciones(
ConfiguracionId int primary key identity(1,1),
NCF varchar(20),
RNC varchar(20),
ITBIS float,
RutaBackup varchar(100),
RutaFoto varchar(100)
);
go
create table Inventario(
InventarioId int primary key identity(1,1),
ProductoId int References Productos(ProductoId),
Cantidad int,
Fecha datetime
);
go--Nueva Tabla
create table ProductoIngredientes(
ProductoIngredienteId int primary key identity(1,1),
ProductoId int references Productos(ProductoId),
ProductoSecundarioId int references Productos(ProductoId),
Cantidad int
);

go

create table ClaseProductos(
	ClaseProductosId int primary key identity(1,1),
	DescripcionClase varchar(50)
);

insert into ClaseProductos(DescripcionClase) values('Normal');
insert into ClaseProductos(DescripcionClase) values('Compuesto');
insert into ClaseProductos(DescripcionClase) values('Ingrediente');

alter table Productos add ClaseProductoId int references ClaseProductos(ClaseProductosId) default 1;
alter table Productos add UnidadMedida varchar(40);

Alter Table Cajas add Estado int default 0;

insert into Proveedores(CiudadId,NombreEmpresa,NombreRepresentante,RNC,Direccion,Telefono,Celular,Correo,Activo)
values(1,'La para LC','Pedro Fernandez','33439dsdsds','Moca','809-323-0903','908-098-8790','info@dsds.com',0);
--Buscar Preventa
select pv.PreventaId, u.UsuarioId as UsuarioId, u.Nombre as NombreUsuario, pv.Fecha as Fecha, pv.Total as TotalVenta from Preventa pv inner join Usuarios u on u.UsuarioId = pv.UsuarioId where pv.MesaId = 1 and pv.Activo = 0
select p.ProductoId, p.Nombre,p.Precio, pvp.Cantidad, pvp.ITBIS, pvp.Importe from PreventaProductos pvp inner join Productos p on pvp.ProductoId = p.ProductoId where pvp.PreventaId = 1004
select * from Mesas
update Mesas set Activo = 0 where MesaId = 13;


select * from PreventaProductos where PreventaId = 1003

select * from Tarjetas where MesaId = 10 and Activo = 0;

select * from Proveedores p, Ciudades c where c.CiudadId = p.CiudadId AND p.ProveedorId = 1

select * from Cajas
select * from Tarjetas
select * from Productos
select * from Usuarios
select * from Ventas
select * from VentasProductos

update Cajas set Estado = 0;

select proing.ProductoSecundarioId,p.Nombre,proing.Cantidad from Productos p,ProductoIngredientes proing where p.ProductoId = proing.ProductoId and p.ProductoId = 17
select proing.ProductoSecundarioId,p.Nombre,proing.Cantidad from Productos p,ProductoIngredientes proing where p.ProductoId = proing.ProductoId and p.ProductoId = 18

--Insert que debemos hacer al instalar el sistema
--Usuario																		ZgBjADEAMAAxADAA = fc1010
insert into Usuarios(Nombre,Contrasena,TipoUsuario,FechaInicio,Activo) values('Francis','ZgBjADEAMAAxADAA',0,GETDATE(),0);
--Ciudades
insert into Ciudades(Nombre,Activo) Values ('Tenares',0),('SFM',0);
--Tipo de Productos
insert into TiposProductos(Nombre) values ('Comida'),('Ensalada'),('Bebida'),('Helado'),('Trago');
--Proveedores
insert into Proveedores(CiudadId,NombreEmpresa,NombreRepresentante,RNC,Direccion,Telefono,Celular,Correo,Activo) values
(1,'ContraPlus','Luis Torres','547879685635','Moca, frente a servitech','802-987-8855','789-456-1320','info@contraplus.com.do',0)
--Productos
insert into Productos(TipoProductosId,ProveedorId,Imagen,Nombre,Categoria,Precio,Costo,Activo) values 
 (1,1,'','Pan','Categoria',5,5,0),
 (2,1,'','Tacos','Categoria',50,45,0),
 (3,1,'','Ensalada Mista','Categoria',50,45,0),
 (4,1,'','Black Label','Categoria',500,400,0),
 (5,1,'','Ron con Pasa','Categoria',200,150,0),
 (6,1,'','Trago 1','Categoria',50,45,0);
--Mesas
insert into Mesas(Numero,Activo)values(1,0);
insert into Mesas(Numero,Activo)values(2,0);
insert into Mesas(Numero,Activo)values(3,0);
insert into Mesas(Numero,Activo)values(4,0);
insert into Mesas(Numero,Activo)values(5,0);
insert into Mesas(Numero,Activo)values(6,0);
insert into Mesas(Numero,Activo)values(7,0);
insert into Mesas(Numero,Activo)values(8,0);
insert into Mesas(Numero,Activo)values(9,0);
insert into Mesas(Numero,Activo)values(10,0);
insert into Mesas(Numero,Activo)values(11,0);
insert into Mesas(Numero,Activo)values(12,0);
insert into Mesas(Numero,Activo)values(13,0);
insert into Mesas(Numero,Activo)values(14,0);
insert into Mesas(Numero,Activo)values(15,0);
insert into Mesas(Numero,Activo)values(16,0);
insert into Mesas(Numero,Activo)values(17,0);
insert into Mesas(Numero,Activo)values(18,0);
insert into Mesas(Numero,Activo)values(19,0);
insert into Mesas(Numero,Activo)values(20,0);
insert into Mesas(Numero,Activo)values(21,0);
insert into Mesas(Numero,Activo)values(22,0);
insert into Mesas(Numero,Activo)values(23,0);
insert into Mesas(Numero,Activo)values(24,0);
insert into Mesas(Numero,Activo)values(25,0);
